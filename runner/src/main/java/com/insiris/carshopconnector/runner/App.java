package com.insiris.carshopconnector.runner;

public class App {
    public static void main(String[] args) {
        System.setProperty("java.util.concurrent.ForkJoinPool.common.parallelism", "8");

        if (args != null && args.length == 1) {
            if("connector".equals(args[0])) {
                com.insiris.carshopconnector.connector.App.main(null);
            } else if ("idling".equals(args[0])){
                com.insiris.carshopconnector.idling.App.main(null);
            }
        }
        System.exit(0);
    }
}
