package com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.mysql.KassetSql;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class KassetStatusSql {

    public static KassetStatusSql instance;
    private static Logger logger = LoggerFactory.getLogger(KassetStatusSql.class);


    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = KassetSql.class.getClassLoader().getResourceAsStream("kasset_status_sql.yml");
            instance = mapper.readValue(stream, KassetStatusSql.class);
        } catch (Exception e) {
            logger.error("loading KassetStatusSql has thrown {}", e);
        }
    }

    private String getAll;
    private String aliasKassetId;
    private String aliasKassetStatusId;
    private String aliasKassetStatus;
    private String aliasUpdatedAt;
    private String aliasUserId;

    public String getGetAll() {
        return getAll;
    }

    public void setGetAll(String getAll) {
        this.getAll = getAll;
    }

    public String getAliasKassetId() {
        return aliasKassetId;
    }

    public void setAliasKassetId(String aliasKassetId) {
        this.aliasKassetId = aliasKassetId;
    }

    public String getAliasKassetStatusId() {
        return aliasKassetStatusId;
    }

    public void setAliasKassetStatusId(String aliasKassetStatusId) {
        this.aliasKassetStatusId = aliasKassetStatusId;
    }

    public String getAliasKassetStatus() {
        return aliasKassetStatus;
    }

    public void setAliasKassetStatus(String aliasKassetStatus) {
        this.aliasKassetStatus = aliasKassetStatus;
    }

    public String getAliasUpdatedAt() {
        return aliasUpdatedAt;
    }

    public void setAliasUpdatedAt(String aliasUpdatedAt) {
        this.aliasUpdatedAt = aliasUpdatedAt;
    }

    public String getAliasUserId() {
        return aliasUserId;
    }

    public void setAliasUserId(String aliasUserId) {
        this.aliasUserId = aliasUserId;
    }
}
