package com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses;

import java.sql.Timestamp;

public class KassetStatus {
    private int kassetId;
    private int kassetStatusId;
    private String kassetStatus;
    private Timestamp updatedAt;
    private int userId;

    public int getKassetId() {
        return kassetId;
    }

    public void setKassetId(int kassetId) {
        this.kassetId = kassetId;
    }

    public int getKassetStatusId() {
        return kassetStatusId;
    }

    public void setKassetStatusId(int kassetStatusId) {
        this.kassetStatusId = kassetStatusId;
    }

    public String getKassetStatus() {
        return kassetStatus;
    }

    public void setKassetStatus(String kassetStatus) {
        this.kassetStatus = kassetStatus;
    }

    public Timestamp getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Timestamp updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public KassetStatus() {

    }
}
