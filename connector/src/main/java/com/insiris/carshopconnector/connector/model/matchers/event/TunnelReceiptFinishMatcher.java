package com.insiris.carshopconnector.connector.model.matchers.event;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;

public class TunnelReceiptFinishMatcher extends JobLogEntryMatcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if("2. Tunnel Receipt".equals(((JobLogEntry)toMatch).getName()) && "completed".equals(((JobLogEntry)toMatch).getAction()))
            return generateMutation((JobLogEntry)toMatch, connector, "TunnelReceiptFinish");
        else
            return null;
    }
}
