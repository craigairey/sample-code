package com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.mysql;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.Kasset;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.KassetDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlKassetDao implements KassetDao {

    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlKassetDao(@Named("mysql")DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }

    @Override
    public List<Kasset> getAll(Timestamp start, Timestamp finish) {
        logger.info("getting all kassets start {} finish {}", start, finish);
        PreparedStatement statement = null;
        List<Kasset> results = new ArrayList<>();
        try {
            statement = connection.prepareStatement(KassetSql.instance.getGetAll());
            statement.setTimestamp(1, start);
            statement.setTimestamp(2, finish);
            logger.info("kassets get all {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = KassetMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all kassets start {} finish {} has thrown: {}", start, finish, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }
}
