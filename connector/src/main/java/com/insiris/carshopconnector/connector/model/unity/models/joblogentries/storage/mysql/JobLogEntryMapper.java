package com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.mysql;


import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JobLogEntryMapper {
    public static List<JobLogEntry> mapResults(ResultSet resultSet) throws SQLException {
        List<JobLogEntry> jobLogEntries = new ArrayList<>();
        while(resultSet.next()) {
            JobLogEntry jobLogEntry = new JobLogEntry();

            jobLogEntry.setId(resultSet.getInt(JobLogEntrySql.instance.getAliasId()));
            jobLogEntry.setKassetId(resultSet.getInt(JobLogEntrySql.instance.getAliasKassetId()));
            jobLogEntry.setAction(resultSet.getString(JobLogEntrySql.instance.getAliasAction()));
            jobLogEntry.setName(resultSet.getString(JobLogEntrySql.instance.getAliasName()));
            jobLogEntry.setPersonId(resultSet.getInt(JobLogEntrySql.instance.getAliasPerson()));
            jobLogEntry.setGeneratedAt(resultSet.getTimestamp(JobLogEntrySql.instance.getAliasGen()));
            jobLogEntry.setJobId(resultSet.getInt(JobLogEntrySql.instance.getAliasJobId()));
            jobLogEntry.setJobDescription(resultSet.getString(JobLogEntrySql.instance.getAliasJobDescription()));
            jobLogEntries.add(jobLogEntry);
        }
        return jobLogEntries;
    }
}
