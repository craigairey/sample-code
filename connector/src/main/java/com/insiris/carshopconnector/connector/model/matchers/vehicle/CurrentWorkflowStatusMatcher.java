package com.insiris.carshopconnector.connector.model.matchers.vehicle;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;
import com.insiris.carshopconnector.connector.shared.Converters;

public class CurrentWorkflowStatusMatcher extends Matcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        return currentWorkflowStatus((JobLogEntry)toMatch, connector);
    }

    private Mutation currentWorkflowStatus(JobLogEntry jobLogEntry, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "UPDATE "+connector.getDbName()+".dbo.vehicles SET current_workflow_status = \\'"+jobLogEntry.getName()+"\\', updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+jobLogEntry.getKassetId();
        return new UpdateMutation(sql, connector.getUuid());
    }
}
