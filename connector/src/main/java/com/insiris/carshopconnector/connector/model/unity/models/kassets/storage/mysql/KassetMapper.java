package com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.mysql;

import com.insiris.carshopconnector.connector.model.unity.models.kassets.Kasset;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class KassetMapper {
    public static List<Kasset>mapResults(ResultSet resultSet) throws SQLException{
        List<Kasset> kassets = new ArrayList<>();
        while(resultSet.next()) {
            Kasset kasset = new Kasset();

            kasset.setId(resultSet.getInt(KassetSql.instance.getAliasId()));
            kasset.setName(resultSet.getString(KassetSql.instance.getAliasName()));
            kasset.setAddress(resultSet.getString(KassetSql.instance.getAliasAddress()));
            kasset.setDescription(resultSet.getString(KassetSql.instance.getAliasDescription()));
            kasset.setPostcode_zip(resultSet.getString(KassetSql.instance.getAliasPostcode_zip()));
            kasset.setLatitude(resultSet.getDouble(KassetSql.instance.getAliasLatitude()));
            kasset.setDecimal(resultSet.getDouble(KassetSql.instance.getAliasDec()));
            kasset.setLongitude(resultSet.getDouble(KassetSql.instance.getAliasLongitude()));
            kasset.setKasset_type(resultSet.getString(KassetSql.instance.getAliasKasset_type()));
            kasset.setIs_stock(resultSet.getInt(KassetSql.instance.getAliasIs_stock()));
            kasset.setStock_count(resultSet.getDouble(KassetSql.instance.getAliasStock_count()));
            kasset.setKasset_template_id(resultSet.getInt(KassetSql.instance.getAliasKasset_template_id()));
            kasset.setKasset_group_id(resultSet.getInt(KassetSql.instance.getAliasKasset_group_id()));
            kasset.setCustomer_id(resultSet.getInt(KassetSql.instance.getAliasCustomer_id()));
            kasset.setNfc_tag_identifier(resultSet.getString(KassetSql.instance.getAliasNfc_tag_identifier()));
            kasset.setIdentifier(resultSet.getString(KassetSql.instance.getAliasIdentifier()));
            kasset.setShow_on_mobile(resultSet.getInt(KassetSql.instance.getAliasShow_on_mobile()));
            kasset.setShow_on_mobile_updated(resultSet.getInt(KassetSql.instance.getAliasShow_on_mobile_updated()));
            kasset.setDeleted_at(resultSet.getTimestamp(KassetSql.instance.getAliasDeleted_at()));
            kasset.setGuid(resultSet.getString(KassetSql.instance.getAliasGuid()));
            kasset.setCreated_at(resultSet.getTimestamp(KassetSql.instance.getAliasCreated_at()));
            kasset.setUpdated_at(resultSet.getTimestamp(KassetSql.instance.getAliasUpdated_at()));
            kasset.setRate_plan_id(resultSet.getInt(KassetSql.instance.getAliasRate_plan_id()));
            kasset.setKasset_status_id(resultSet.getInt(KassetSql.instance.getAliasKasset_status_id()));
            kasset.setSend_job_history_kasset_count(resultSet.getInt(KassetSql.instance.getAliasSend_job_history_kasset_count()));
            kasset.setDormant(resultSet.getInt(KassetSql.instance.getAliasDormant()));
            kasset.setLocked_by_user_id(resultSet.getInt(KassetSql.instance.getAliasLocked_by_user_id()));
            kasset.setLocked_until(resultSet.getTimestamp(KassetSql.instance.getAliasLocked_until()));
            kassets.add(kasset);
        }
        return kassets;
    }


}

















