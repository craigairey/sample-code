package com.insiris.carshopconnector.connector.model.unity.models.asset.storage.mysql;

import com.insiris.carshopconnector.connector.model.unity.models.asset.Asset;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AssetMapper {
    public static List<Asset> mapResults(ResultSet resultSet) throws SQLException {
        List<Asset> assets = new ArrayList<>();
        while(resultSet.next()) {
            Asset asset = new Asset();

            asset.setId(resultSet.getInt(AssetSql.instance.getAliasId()));
            asset.setName(resultSet.getString(AssetSql.instance.getAliasName()));
            asset.setCreatedAt(resultSet.getTimestamp(AssetSql.instance.getAliasCreatedAt()));
            asset.setUpdatedAt(resultSet.getTimestamp(AssetSql.instance.getAliasUpdatedAt()));
            asset.setGroupsList(resultSet.getString(AssetSql.instance.getAliasGroupsList()));
            assets.add(asset);
        }
        return assets;
    }
}
