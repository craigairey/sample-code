package com.insiris.carshopconnector.connector.model.unity.models.wiqevents.storage.mysql;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.WiqEvent;
import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.storage.WiqEventDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlWiqEventDao implements WiqEventDao {
    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlWiqEventDao(@Named("mysql")DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }

    @Override
    public List<WiqEvent> getAll(Timestamp start, Timestamp finish) {
        logger.info("getting all Wiq Events start {} finish {}", start, finish);
        PreparedStatement statement = null;
        List<WiqEvent> results = new ArrayList<>();
        try {
            statement = connection.prepareStatement(WiqEventSql.instance.getGetAll());
            statement.setTimestamp(1, start);
            statement.setTimestamp(2, finish);
            logger.info("wiq events get all {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = WiqEventMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all wid events start {} finish {} has thrown: {}", start, finish, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }
}
