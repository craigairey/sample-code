package com.insiris.carshopconnector.connector.model.unity.models.jobs.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class JobSql {
    public static JobSql instance;
    private static Logger logger = LoggerFactory.getLogger(JobSql.class);


    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = JobSql.class.getClassLoader().getResourceAsStream("job_sql.yml");
            instance = mapper.readValue(stream, JobSql.class);
        } catch (Exception e) {
            logger.error("loading JobSql has thrown {}", e);
        }
    }

    private String getAll;
    private String aliasId;
    private String aliasGroup;
    private String aliasVehicleId;
    private String aliasDescription;
    private String aliasCreatedAt;
    private String aliasHour;
    private String aliasMinute;
    private String aliasWorkflowName;
    private String aliasNotes;

    public String getGetAll() {
        return getAll;
    }

    public void setGetAll(String getAll) {
        this.getAll = getAll;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public String getAliasGroup() {
        return aliasGroup;
    }

    public void setAliasGroup(String aliasGroup) {
        this.aliasGroup = aliasGroup;
    }

    public String getAliasVehicleId() {
        return aliasVehicleId;
    }

    public void setAliasVehicleId(String aliasVehicleId) {
        this.aliasVehicleId = aliasVehicleId;
    }

    public String getAliasDescription() {
        return aliasDescription;
    }

    public void setAliasDescription(String aliasDescription) {
        this.aliasDescription = aliasDescription;
    }

    public String getAliasCreatedAt() {
        return aliasCreatedAt;
    }

    public void setAliasCreatedAt(String aliasCreatedAt) {
        this.aliasCreatedAt = aliasCreatedAt;
    }

    public String getAliasHour() {
        return aliasHour;
    }

    public void setAliasHour(String aliasHour) {
        this.aliasHour = aliasHour;
    }

    public String getAliasMinute() {
        return aliasMinute;
    }

    public void setAliasMinute(String aliasMinute) {
        this.aliasMinute = aliasMinute;
    }

    public String getAliasWorkflowName() {
        return aliasWorkflowName;
    }

    public void setAliasWorkflowName(String aliasWorkflowName) {
        this.aliasWorkflowName = aliasWorkflowName;
    }

    public String getAliasNotes() {
        return aliasNotes;
    }

    public void setAliasNotes(String aliasNotes) {
        this.aliasNotes = aliasNotes;
    }
}
