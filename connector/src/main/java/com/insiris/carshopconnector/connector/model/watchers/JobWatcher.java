package com.insiris.carshopconnector.connector.model.watchers;


import com.google.inject.Inject;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.handlers.JobHandler;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.matchers.job.JobCreatedMatcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.jobs.Job;
import com.insiris.carshopconnector.connector.model.unity.models.jobs.storage.JobDao;

import java.util.ArrayList;
import java.util.List;

public class JobWatcher extends Watcher {

    private JobDao dao;
    private ConnectorDao connectorDao;
    private JobHandler handler;

    @Inject
    public JobWatcher(JobDao dao, ConnectorDao connectorDao,  MutationDao mutationDao){
        this.dao = dao;
        this.handler = new JobHandler(
                new ArrayList<Matcher>(){{
                    add(new JobCreatedMatcher());
                }},
                mutationDao
        );
        this.connectorDao = connectorDao;
    }

    @Override
    public void onPoll(Connector connector){
        List<Job> jobs = dao.getAll(connector.getStart(),connector.getEnd());
        boolean handlerCompleted = false;

        if(jobs != null && jobs.size() > 0) {
            handlerCompleted = handler.match(jobs, connector);
            connector.setLastJobTimestamp(jobs.get(jobs.size() - 1).getCreatedAt());
        }
    };
}
