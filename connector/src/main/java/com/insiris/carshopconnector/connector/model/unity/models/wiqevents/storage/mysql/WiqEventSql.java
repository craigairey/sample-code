package com.insiris.carshopconnector.connector.model.unity.models.wiqevents.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class WiqEventSql {

    public static WiqEventSql instance;
    private static Logger logger = LoggerFactory.getLogger(WiqEventSql.class);


    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = WiqEventSql.class.getClassLoader().getResourceAsStream("wiq_event_sql.yml");
            instance = mapper.readValue(stream, WiqEventSql.class);
        } catch (Exception e) {
            logger.error("loading WiqEventSql has thrown {}", e);
        }
    }

    private String getAll;
    private String aliasAssetId;
    private String aliasType;
    private String aliasGeneratedAt;

    public String getGetAll() {
        return getAll;
    }

    public void setGetAll(String getAll) {
        this.getAll = getAll;
    }

    public String getAliasAssetId() {
        return aliasAssetId;
    }

    public void setAliasAssetId(String aliasAssetId) {
        this.aliasAssetId = aliasAssetId;
    }

    public String getAliasType() {
        return aliasType;
    }

    public void setAliasType(String aliasType) {
        this.aliasType = aliasType;
    }

    public String getAliasGeneratedAt() {
        return aliasGeneratedAt;
    }

    public void setAliasGeneratedAt(String aliasGeneratedAt) {
        this.aliasGeneratedAt = aliasGeneratedAt;
    }
}
