package com.insiris.carshopconnector.connector.model.matchers.person;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.AssetGroup;
import com.insiris.carshopconnector.connector.shared.Converters;

import java.util.ArrayList;
import java.util.List;

public class DepartmentMatcher extends Matcher {
    private List<String> departments = new ArrayList<String>(){{
        add("COMPOUND");
        add("INSPECTION");
        add("PRODUCTION");
        add("PARTS");
    }};

    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(departments.contains(((AssetGroup)toMatch).getName().trim()))
            return departmentChangedSQL((AssetGroup)toMatch, connector);
        else
            return null;
    }

    private Mutation departmentChangedSQL(AssetGroup assetGroup, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "UPDATE "+connector.getDbName()+".dbo.people SET department = \\'"+assetGroup.getName()+"\\', updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+assetGroup.getId();
        return new UpdateMutation(sql, connector.getUuid());
    }

}