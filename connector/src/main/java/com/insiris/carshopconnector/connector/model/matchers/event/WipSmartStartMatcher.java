package com.insiris.carshopconnector.connector.model.matchers.event;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;

public class WipSmartStartMatcher extends JobLogEntryMatcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if("Smart".equals(((JobLogEntry)toMatch).getName()) && "in_progress".equals(((JobLogEntry)toMatch).getAction()))
            return generateMutation((JobLogEntry)toMatch, connector, "WipSmartStart");
        else
            return null;
    }
}
