package com.insiris.carshopconnector.connector.model.connector.models.connector.storage.mysql;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.List;

public class MySqlConnectorDao implements ConnectorDao {
    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlConnectorDao(@Named("mysqlConMut") DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }


    @Override
    public Connector get() {
        Statement statement = null;
        Connector toReturn = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(ConnectorSql.instance.getPrevious());
            List<Connector> results = ConnectorMapper.mapResults(resultSet);
            if(results.size() == 1)
                toReturn = results.get(0);
        } catch (SQLException e){
            logger.error("getting latest connector has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return toReturn;
    }

    @Override
    public void save(Connector connector) {
        Statement statement = null;
        try{
            statement = connection.createStatement();
            String sql = String.format(
                    ConnectorSql.instance.getSave(),
                    connector.getStart(),
                    connector.getEnd(),
                    nullOrTimestamp(connector.getLastKassetTimestamp()),
                    nullOrTimestamp(connector.getLastKassetStatusTimestamp()),
                    nullOrTimestamp(connector.getLastKassetFieldTimestamp()),
                    nullOrTimestamp(connector.getLastJobLogEntryTimestamp()),
                    nullOrTimestamp(connector.getLastJobTimestamp()),
                    connector.getUuid()
            );
            statement.execute(sql);
        } catch (SQLException e) {
            logger.error("creating connector has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
    }

    private String nullOrTimestamp(Timestamp timestamp){
        if (timestamp != null)
            return "'"+timestamp+"'";
        else
            return "null";
    }
}
