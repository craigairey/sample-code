package com.insiris.carshopconnector.connector.model.watchers;

import com.insiris.carshopconnector.connector.model.unity.models.casekasset.storage.CaseKassetDao;

public class CaseKassetWatcher extends Watcher {

    private CaseKassetDao dao;

    protected CaseKassetDao getDao() {
        return this.dao;
    }

    protected void setDao(CaseKassetDao dao) {
        this.dao = dao;
    }

}
