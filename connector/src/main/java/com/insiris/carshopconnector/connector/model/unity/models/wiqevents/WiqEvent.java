package com.insiris.carshopconnector.connector.model.unity.models.wiqevents;

import java.sql.Timestamp;

public class WiqEvent {
    private int assetId;
    private String type;
    private Timestamp generatedAt;

    public WiqEvent() {
    }

    public int getAssetId() {
        return assetId;
    }

    public void setAssetId(int assetId) {
        this.assetId = assetId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Timestamp getGeneratedAt() {
        return generatedAt;
    }

    public void setGeneratedAt(Timestamp generatedAt) {
        this.generatedAt = generatedAt;
    }
}
