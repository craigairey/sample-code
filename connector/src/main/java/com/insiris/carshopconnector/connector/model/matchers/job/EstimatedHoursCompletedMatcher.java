package com.insiris.carshopconnector.connector.model.matchers.job;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;
import com.insiris.carshopconnector.connector.shared.Converters;

public class EstimatedHoursCompletedMatcher extends Matcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(0 < ((JobLogEntry)toMatch).getJobId() && "completed".equals(((JobLogEntry)toMatch).getAction()))
            return actualHoursCompletedSql((JobLogEntry)toMatch, connector);
        else
            return null;
    }

    private Mutation actualHoursCompletedSql(JobLogEntry jobLogEntry, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "UPDATE "+connector.getDbName()+".dbo.jobs SET estimated_hours_completed = total_estimated_hours, updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+jobLogEntry.getJobId();
        return new UpdateMutation(sql, connector.getUuid());
    }
}
