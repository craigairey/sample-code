package com.insiris.carshopconnector.connector.model.unity.models.jobs.storage.mysql;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.unity.models.jobs.Job;
import com.insiris.carshopconnector.connector.model.unity.models.jobs.storage.JobDao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlJobDao implements JobDao {
    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlJobDao(@Named("mysql")DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }

    @Override
    public List<Job> getAll(Timestamp start, Timestamp finish) {
        logger.info("getting all Job start {} finish {}", start, finish);
        PreparedStatement statement = null;
        List<Job> results = new ArrayList<>();
        try {
            statement = connection.prepareStatement(JobSql.instance.getGetAll());
            statement.setTimestamp(1, start);
            statement.setTimestamp(2, finish);
            logger.info("Job get all {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = JobMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all Job start {} finish {} has thrown: {}", start, finish, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }
}
