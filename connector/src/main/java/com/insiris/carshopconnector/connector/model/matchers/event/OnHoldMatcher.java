package com.insiris.carshopconnector.connector.model.matchers.event;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.KassetStatus;

public class OnHoldMatcher extends KassetStatusMatcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(24 == ((KassetStatus)toMatch).getKassetStatusId())
            return generateMutation((KassetStatus)toMatch, connector, "OnHold");
        else
            return null;
    }
}
