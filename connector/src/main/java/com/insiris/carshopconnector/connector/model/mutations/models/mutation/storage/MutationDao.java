package com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage;

import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;

import java.util.List;

public interface MutationDao {
    void saveAll(List<Mutation> mutations);
    void markCompleted(List<Mutation> mutations);
    void markFailed(List<Mutation> mutations);
    List<Mutation> getAllCreateMutations();
    List<Mutation> getAllUpdateMutations();
}
