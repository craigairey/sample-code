package com.insiris.carshopconnector.connector.model.matchers.vehicle;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.KassetField;
import com.insiris.carshopconnector.connector.shared.Converters;

public class AqIdChangeMatcher extends Matcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if("Stock Number".equals(((KassetField)toMatch).getName()) && !((KassetField)toMatch).getValue().isEmpty())
            return stockNumberSQL((KassetField)toMatch, connector);
        else
            return null;
    }

    private Mutation stockNumberSQL(KassetField kassetField, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "UPDATE "+connector.getDbName()+".dbo.vehicles SET vehicle_id = \\'"+kassetField.getValue()+"\\', updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+kassetField.getId();
        return new UpdateMutation(sql, connector.getUuid());
    }
}
