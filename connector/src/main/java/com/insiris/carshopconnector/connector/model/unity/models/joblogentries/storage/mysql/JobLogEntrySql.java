package com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class JobLogEntrySql {

    public static JobLogEntrySql instance;
    private static Logger logger = LoggerFactory.getLogger(JobLogEntrySql.class);


    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = JobLogEntrySql.class.getClassLoader().getResourceAsStream("job_log_entries_sql.yml");
            instance = mapper.readValue(stream, JobLogEntrySql.class);
        } catch (Exception e) {
            logger.error("loading JobLogEntrySql has thrown {}", e);
        }
    }

    private String getAll;
    private String getDurationForJob;
    private String aliasId;
    private String aliasKassetId;
    private String aliasAction;
    private String aliasName;
    private String aliasPerson;
    private String aliasGen;
    private String aliasJobId;
    private String aliasJobDescription;

    public String getGetAll() {
        return getAll;
    }

    public void setGetAll(String getAll) {
        this.getAll = getAll;
    }

    public String getGetDurationForJob() {
        return getDurationForJob;
    }

    public void setGetDurationForJob(String getDurationForJob) {
        this.getDurationForJob = getDurationForJob;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public String getAliasKassetId() {
        return aliasKassetId;
    }

    public void setAliasKassetId(String aliasKassetId) {
        this.aliasKassetId = aliasKassetId;
    }

    public String getAliasAction() {
        return aliasAction;
    }

    public void setAliasAction(String aliasAction) {
        this.aliasAction = aliasAction;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasPerson() {
        return aliasPerson;
    }

    public void setAliasPerson(String aliasPerson) {
        this.aliasPerson = aliasPerson;
    }

    public String getAliasGen() {
        return aliasGen;
    }

    public void setAliasGen(String aliasGen) {
        this.aliasGen = aliasGen;
    }

    public String getAliasJobId() {
        return aliasJobId;
    }

    public void setAliasJobId(String aliasJobId) {
        this.aliasJobId = aliasJobId;
    }

    public String getAliasJobDescription() {
        return aliasJobDescription;
    }

    public void setAliasJobDescription(String aliasJobDescription) {
        this.aliasJobDescription = aliasJobDescription;
    }
}
