package com.insiris.carshopconnector.connector.model.connector.models.connector;

import java.sql.Timestamp;
import java.util.UUID;

public class Connector {
    private int id;
    private String dbName;
    private Timestamp start;
    private Timestamp end;
    private Timestamp lastKassetTimestamp;
    private Timestamp lastKassetStatusTimestamp;
    private Timestamp lastKassetFieldTimestamp;
    private Timestamp lastJobLogEntryTimestamp;
    private Timestamp lastJobTimestamp;
    private String uuid;

    public Connector() {
        this.uuid = UUID.randomUUID().toString();
        this.lastKassetTimestamp = null;
        this.lastKassetStatusTimestamp = null;
        this.lastKassetFieldTimestamp = null;
        this.lastJobLogEntryTimestamp = null;
        this.lastJobTimestamp = null;
    }

    public Connector(Connector lastConnector){
        this.uuid = UUID.randomUUID().toString();
        this.lastKassetTimestamp = lastConnector.getLastKassetTimestamp();
        this.lastKassetStatusTimestamp = lastConnector.getLastKassetStatusTimestamp();
        this.lastKassetFieldTimestamp = lastConnector.getLastKassetFieldTimestamp();
        this.lastJobLogEntryTimestamp = lastConnector.getLastJobLogEntryTimestamp();
        this.lastJobTimestamp = lastConnector.getLastJobTimestamp();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    public Timestamp getLastKassetTimestamp() {
        return lastKassetTimestamp;
    }


    public void setLastKassetTimestamp(Timestamp lastKassetTimestamp) {
        this.lastKassetTimestamp = lastKassetTimestamp;
    }

    public Timestamp getLastKassetStatusTimestamp() {
        return lastKassetStatusTimestamp;
    }

    public void setLastKassetStatusTimestamp(Timestamp lastKassetStatusTimestamp) {
        this.lastKassetStatusTimestamp = lastKassetStatusTimestamp;
    }

    public Timestamp getLastKassetFieldTimestamp() {
        return lastKassetFieldTimestamp;
    }

    public void setLastKassetFieldTimestamp(Timestamp lastKassetFieldTimestamp) {
        this.lastKassetFieldTimestamp = lastKassetFieldTimestamp;
    }

    public Timestamp getLastJobLogEntryTimestamp() {
        return lastJobLogEntryTimestamp;
    }

    public void setLastJobLogEntryTimestamp(Timestamp lastJobLogEntryTimestamp) {
        this.lastJobLogEntryTimestamp = lastJobLogEntryTimestamp;
    }

    public Timestamp getLastJobTimestamp() {
        return lastJobTimestamp;
    }

    public void setLastJobTimestamp(Timestamp lastJobTimestamp) {
        this.lastJobTimestamp = lastJobTimestamp;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }
}
