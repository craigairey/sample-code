package com.insiris.carshopconnector.connector.model.unity.models.wiqevents.storage;


import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.WiqEvent;

import java.sql.Timestamp;
import java.util.List;

public interface WiqEventDao {
    List<WiqEvent> getAll(Timestamp start, Timestamp finish);
}
