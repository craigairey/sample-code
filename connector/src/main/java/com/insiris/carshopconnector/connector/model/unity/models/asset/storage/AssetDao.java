package com.insiris.carshopconnector.connector.model.unity.models.asset.storage;


import com.insiris.carshopconnector.connector.model.unity.models.asset.Asset;

import java.sql.Timestamp;
import java.util.List;

public interface AssetDao {
    List<Asset> getAll(Timestamp start, Timestamp finish);
}
