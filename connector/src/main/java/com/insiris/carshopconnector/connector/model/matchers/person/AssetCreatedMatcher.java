package com.insiris.carshopconnector.connector.model.matchers.person;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.CreateMutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.asset.Asset;
import com.insiris.carshopconnector.connector.shared.Converters;

public class AssetCreatedMatcher extends Matcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        // TODO: 23/02/2018 chaneg get start to lastUpdated & last createAt
        if(((Asset)toMatch).getCreatedAt().toInstant().toEpochMilli() >= connector.getStart().toInstant().toEpochMilli() && ((Asset)toMatch).getCreatedAt().toInstant().toEpochMilli() <= connector.getEnd().toInstant().toEpochMilli())
            return assetCreatedSQL((Asset)toMatch, connector);
        else
            return null;
    }

    private Mutation assetCreatedSQL(Asset asset, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "INSERT INTO "+connector.getDbName()+".dbo.people (id, name, created_at, updated_at, deleted_at, connector_log_run_uuid, mutation_uuid) VALUES ("+asset.getId()+", \\'"+asset.getName()+"\\', \\'"+ now+"\\', \\'"+ now+"\\', null, \\'%1$s\\', \\'%2$s\\')";
        return new CreateMutation(sql, connector.getUuid());
    }
}
