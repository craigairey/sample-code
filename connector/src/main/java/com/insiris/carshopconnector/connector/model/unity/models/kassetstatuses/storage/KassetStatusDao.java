package com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage;

import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.KassetStatus;

import java.sql.Timestamp;
import java.util.List;

public interface KassetStatusDao {
    List<KassetStatus> getAll(Timestamp start, Timestamp finish);
}
