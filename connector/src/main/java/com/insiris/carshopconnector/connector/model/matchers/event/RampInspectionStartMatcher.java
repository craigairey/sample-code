package com.insiris.carshopconnector.connector.model.matchers.event;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;

public class RampInspectionStartMatcher extends JobLogEntryMatcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if("4. Ramp Inspection".equals(((JobLogEntry)toMatch).getName()) && "in_progress".equals(((JobLogEntry)toMatch).getAction()))
            return generateMutation((JobLogEntry)toMatch, connector, "RampInspectionStart");
        else
            return null;
    }
}
