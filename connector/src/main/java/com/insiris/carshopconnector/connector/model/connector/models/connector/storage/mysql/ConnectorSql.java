package com.insiris.carshopconnector.connector.model.connector.models.connector.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class ConnectorSql {
    private static Logger logger = LoggerFactory.getLogger(ConnectorSql.class);

    public static ConnectorSql instance;

    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = ConnectorSql.class.getClassLoader().getResourceAsStream("connector_sql.yml");
            instance = mapper.readValue(stream, ConnectorSql.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String previous;
    private String save;
    private String aliasId;
    private String columnId;
    private String aliasStart;
    private String columnStart;
    private String aliasEnd;
    private String columnEnd;
    private String aliasLastKassetTimestamp;
    private String columnLastKassetTimestamp;
    private String aliasLastKassetStatusTimestamp;
    private String columnLastKassetStatusTimestamp;
    private String aliasLastKassetFieldTimestamp;
    private String columnLastKassetFieldTimestamp;
    private String aliasLastJobLogEntryTimestamp;
    private String columnLastJobLogEntryTimestamp;
    private String aliasLastJobTimestamp;
    private String columnLastJobTimestamp;
    private String columnUuid;
    private String aliasUuid;

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public String getSave() {
        return save;
    }

    public void setSave(String save) {
        this.save = save;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public String getColumnId() {
        return columnId;
    }

    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }

    public String getAliasStart() {
        return aliasStart;
    }

    public void setAliasStart(String aliasStart) {
        this.aliasStart = aliasStart;
    }

    public String getColumnStart() {
        return columnStart;
    }

    public void setColumnStart(String columnStart) {
        this.columnStart = columnStart;
    }

    public String getAliasEnd() {
        return aliasEnd;
    }

    public void setAliasEnd(String aliasEnd) {
        this.aliasEnd = aliasEnd;
    }

    public String getColumnEnd() {
        return columnEnd;
    }

    public void setColumnEnd(String columEnd) {
        this.columnEnd = columEnd;
    }

    public String getAliasLastKassetTimestamp() {
        return aliasLastKassetTimestamp;
    }

    public void setAliasLastKassetTimestamp(String aliasLastKassetTimestamp) {
        this.aliasLastKassetTimestamp = aliasLastKassetTimestamp;
    }

    public String getAliasLastKassetStatusTimestamp() {
        return aliasLastKassetStatusTimestamp;
    }

    public void setAliasLastKassetStatusTimestamp(String aliasLastKassetStatusTimestamp) {
        this.aliasLastKassetStatusTimestamp = aliasLastKassetStatusTimestamp;
    }

    public String getColumnLastKassetStatusTimestamp() {
        return columnLastKassetStatusTimestamp;
    }

    public void setColumnLastKassetStatusTimestamp(String columnLastKassetStatusTimestamp) {
        this.columnLastKassetStatusTimestamp = columnLastKassetStatusTimestamp;
    }

    public String getColumnLastKassetTimestamp() {
        return columnLastKassetTimestamp;
    }

    public void setColumnLastKassetTimestamp(String columnLastKassetTimestamp) {
        this.columnLastKassetTimestamp = columnLastKassetTimestamp;
    }

    public String getAliasLastKassetFieldTimestamp() {
        return aliasLastKassetFieldTimestamp;
    }

    public void setAliasLastKassetFieldTimestamp(String aliasLastKassetFieldTimestamp) {
        this.aliasLastKassetFieldTimestamp = aliasLastKassetFieldTimestamp;
    }

    public String getColumnLastKassetFieldTimestamp() {
        return columnLastKassetFieldTimestamp;
    }

    public void setColumnLastKassetFieldTimestamp(String columnLastKassetFieldTimestamp) {
        this.columnLastKassetFieldTimestamp = columnLastKassetFieldTimestamp;
    }

    public String getAliasLastJobLogEntryTimestamp() {
        return aliasLastJobLogEntryTimestamp;
    }

    public void setAliasLastJobLogEntryTimestamp(String aliasLastJobLogEntryTimestamp) {
        this.aliasLastJobLogEntryTimestamp = aliasLastJobLogEntryTimestamp;
    }

    public String getColumnLastJobLogEntryTimestamp() {
        return columnLastJobLogEntryTimestamp;
    }

    public void setColumnLastJobLogEntryTimestamp(String columnLastJobLogEntryTimestamp) {
        this.columnLastJobLogEntryTimestamp = columnLastJobLogEntryTimestamp;
    }

    public String getAliasLastJobTimestamp() {
        return aliasLastJobTimestamp;
    }

    public void setAliasLastJobTimestamp(String aliasLastJobTimestamp) {
        this.aliasLastJobTimestamp = aliasLastJobTimestamp;
    }

    public String getColumnLastJobTimestamp() {
        return columnLastJobTimestamp;
    }

    public void setColumnLastJobTimestamp(String columnLastJobTimestamp) {
        this.columnLastJobTimestamp = columnLastJobTimestamp;
    }

    public String getColumnUuid() {
        return columnUuid;
    }

    public void setColumnUuid(String columnUuid) {
        this.columnUuid = columnUuid;
    }

    public String getAliasUuid() {
        return aliasUuid;
    }

    public void setAliasUuid(String aliasUuid) {
        this.aliasUuid = aliasUuid;
    }
}
