package com.insiris.carshopconnector.connector.model.matchers.person;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.asset.Asset;
import com.insiris.carshopconnector.connector.shared.Converters;

public class AssetUpdatedMatcher extends Matcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        // TODO: 23/02/2018 chaneg get start to lastUpdated & last createAt
        if(((Asset)toMatch).getUpdatedAt().toInstant().toEpochMilli() >= connector.getStart().toInstant().toEpochMilli() && ((Asset)toMatch).getUpdatedAt().toInstant().toEpochMilli() <= connector.getEnd().toInstant().toEpochMilli())
            return assetCreatedSQL((Asset)toMatch, connector);
        else
            return null;
    }

    private Mutation assetCreatedSQL(Asset asset, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "UPDATE "+connector.getDbName()+".dbo.people SET name = \\'"+asset.getName()+"\\', updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+asset.getId();
        return new UpdateMutation(sql, connector.getUuid());
    }

}