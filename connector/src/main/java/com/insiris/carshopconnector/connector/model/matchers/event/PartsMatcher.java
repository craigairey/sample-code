package com.insiris.carshopconnector.connector.model.matchers.event;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.CreateMutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.KassetStatus;
import com.insiris.carshopconnector.connector.shared.Converters;

public class PartsMatcher extends KassetStatusMatcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(35 == ((KassetStatus)toMatch).getKassetStatusId())
            return generateMutation((KassetStatus)toMatch, connector, "AwaitingParts");
        else
            return null;
    }
}
