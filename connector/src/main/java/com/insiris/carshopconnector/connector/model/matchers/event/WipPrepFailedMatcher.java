package com.insiris.carshopconnector.connector.model.matchers.event;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;

public class WipPrepFailedMatcher extends JobLogEntryMatcher{
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if ("Prep".equals(((JobLogEntry) toMatch).getName()) && "failed".equals(((JobLogEntry) toMatch).getAction()))
            return generateMutation((JobLogEntry) toMatch, connector, "WipPrepFailed");
        else
            return null;
    }
}
