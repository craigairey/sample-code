package com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.mysql;

import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MutationMapper {
    public static List<Mutation> mapResults(ResultSet resultSet) throws SQLException {
        List<Mutation> mutations = new ArrayList<>();
        while(resultSet.next()) {
            Mutation mutation = new Mutation();

            mutation.setId(resultSet.getInt(MutationSql.instance.getAliasUuid()));
            mutation.setStatement(resultSet.getString(MutationSql.instance.getAliasStatement()));
            mutation.setType(resultSet.getString(MutationSql.instance.getAliasType()));
            mutation.setCompleted(resultSet.getInt(MutationSql.instance.getAliasCompleted()) > 0);
            mutation.setConnectorUuid(resultSet.getString(MutationSql.instance.getAliasConnectorUuid()));
            mutation.setUuid(resultSet.getString(MutationSql.instance.getAliasUuid()));
            mutations.add(mutation);
        }
        return mutations;
    }
}
