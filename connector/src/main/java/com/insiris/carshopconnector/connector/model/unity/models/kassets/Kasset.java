package com.insiris.carshopconnector.connector.model.unity.models.kassets;

import java.sql.Timestamp;

public class Kasset {
    private int id;
    private String name;
    private String address;
    private String description;
    private String postcode_zip;
    private Double latitude;
    private Double decimal;
    private Double longitude;
    private String kasset_type;
    private int is_stock;
    private Double stock_count;
    private int kasset_template_id;
    private int kasset_group_id;
    private int customer_id;
    private String nfc_tag_identifier;
    private String identifier;
    private int show_on_mobile;
    private int show_on_mobile_updated;
    private Timestamp deleted_at;
    private String guid;
    private Timestamp created_at;
    private Timestamp updated_at;
    private int rate_plan_id;
    private int kasset_status_id;
    private int send_job_history_kasset_count;
    private int dormant;
    private int locked_by_user_id;
    private Timestamp locked_until;

    public Kasset(int id, String name, String address, String description, String postcode_zip, Double latitude, Double decimal, Double longitude, String kasset_type, int is_stock, Double stock_count, int kasset_template_id, int kasset_group_id, int customer_id, String nfc_tag_identifier, String identifier, int show_on_mobile, int show_on_mobile_updated, Timestamp deleted_at, String guid, Timestamp created_at, Timestamp updated_at, int rate_plan_id, int kasset_status_id, int send_job_history_kasset_count, int dormant, int locked_by_user_id, Timestamp locked_until) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.description = description;
        this.postcode_zip = postcode_zip;
        this.latitude = latitude;
        this.decimal = decimal;
        this.longitude = longitude;
        this.kasset_type = kasset_type;
        this.is_stock = is_stock;
        this.stock_count = stock_count;
        this.kasset_template_id = kasset_template_id;
        this.kasset_group_id = kasset_group_id;
        this.customer_id = customer_id;
        this.nfc_tag_identifier = nfc_tag_identifier;
        this.identifier = identifier;
        this.show_on_mobile = show_on_mobile;
        this.show_on_mobile_updated = show_on_mobile_updated;
        this.deleted_at = deleted_at;
        this.guid = guid;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.rate_plan_id = rate_plan_id;
        this.kasset_status_id = kasset_status_id;
        this.send_job_history_kasset_count = send_job_history_kasset_count;
        this.dormant = dormant;
        this.locked_by_user_id = locked_by_user_id;
        this.locked_until = locked_until;
    }

    public Kasset() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPostcode_zip() {
        return postcode_zip;
    }

    public void setPostcode_zip(String postcode_zip) {
        this.postcode_zip = postcode_zip;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getDecimal() {
        return decimal;
    }

    public void setDecimal(Double decimal) {
        this.decimal = decimal;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getKasset_type() {
        return kasset_type;
    }

    public void setKasset_type(String kasset_type) {
        this.kasset_type = kasset_type;
    }

    public int getIs_stock() {
        return is_stock;
    }

    public void setIs_stock(int is_stock) {
        this.is_stock = is_stock;
    }

    public Double getStock_count() {
        return stock_count;
    }

    public void setStock_count(Double stock_count) {
        this.stock_count = stock_count;
    }

    public int getKasset_template_id() {
        return kasset_template_id;
    }

    public void setKasset_template_id(int kasset_template_id) {
        this.kasset_template_id = kasset_template_id;
    }

    public int getKasset_group_id() {
        return kasset_group_id;
    }

    public void setKasset_group_id(int kasset_group_id) {
        this.kasset_group_id = kasset_group_id;
    }

    public int getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(int customer_id) {
        this.customer_id = customer_id;
    }

    public String getNfc_tag_identifier() {
        return nfc_tag_identifier;
    }

    public void setNfc_tag_identifier(String nfc_tag_identifier) {
        this.nfc_tag_identifier = nfc_tag_identifier;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public int getShow_on_mobile() {
        return show_on_mobile;
    }

    public void setShow_on_mobile(int show_on_mobile) {
        this.show_on_mobile = show_on_mobile;
    }

    public int getShow_on_mobile_updated() {
        return show_on_mobile_updated;
    }

    public void setShow_on_mobile_updated(int show_on_mobile_updated) {
        this.show_on_mobile_updated = show_on_mobile_updated;
    }

    public Timestamp getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(Timestamp deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public void setCreated_at(Timestamp created_at) {
        this.created_at = created_at;
    }

    public Timestamp getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(Timestamp updated_at) {
        this.updated_at = updated_at;
    }

    public int getRate_plan_id() {
        return rate_plan_id;
    }

    public void setRate_plan_id(int rate_plan_id) {
        this.rate_plan_id = rate_plan_id;
    }

    public int getKasset_status_id() {
        return kasset_status_id;
    }

    public void setKasset_status_id(int kasset_status_id) {
        this.kasset_status_id = kasset_status_id;
    }

    public int getSend_job_history_kasset_count() {
        return send_job_history_kasset_count;
    }

    public void setSend_job_history_kasset_count(int send_job_history_kasset_count) {
        this.send_job_history_kasset_count = send_job_history_kasset_count;
    }

    public int getDormant() {
        return dormant;
    }

    public void setDormant(int dormant) {
        this.dormant = dormant;
    }

    public int getLocked_by_user_id() {
        return locked_by_user_id;
    }

    public void setLocked_by_user_id(int locked_by_user_id) {
        this.locked_by_user_id = locked_by_user_id;
    }

    public Timestamp getLocked_until() {
        return locked_until;
    }

    public void setLocked_until(Timestamp locked_until) {
        this.locked_until = locked_until;
    }
}
