package com.insiris.carshopconnector.connector;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.service.CarshopConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class App {
    private static Logger logger = LoggerFactory.getLogger(App.class);
    private static Injector injector;


    public static void main(String[] args) {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        Injector injector = Guice.createInjector(new CarshopConnectorDependenciesModule(), new ConfigurationPropertiesModule());
        CarshopConnector connector = new CarshopConnector(injector);
        ScheduledExecutorService service = Executors
                .newSingleThreadScheduledExecutor();
        service.schedule(injector.getInstance(CarshopConnector.class), injector.getInstance(Key.get(Long.class, Names.named("timer_interval"))), TimeUnit.SECONDS);
        service.scheduleAtFixedRate(connector, 0, injector.getInstance(Key.get(Long.class, Names.named("timer_interval"))), TimeUnit.SECONDS);
    }


}
