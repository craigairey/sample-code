package com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class MutationSql {
    private static Logger logger = LoggerFactory.getLogger(MutationSql.class);

    public static MutationSql instance;

    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = MutationSql.class.getClassLoader().getResourceAsStream("mutation_sql.yml");
            instance = mapper.readValue(stream, MutationSql.class);
        } catch (Exception e) {
           logger.error("loading mutation sql has thrown {}", e);
        }
    }


    private String saveAll;
    private String markCompleted;
    private String markFailed;
    private String getAllCreateMutations;
    private String getAllUpdateMutations;
    private String columnUuid;
    private String aliasUuid;
    private String columnStatement;
    private String aliasStatement;
    private String columnType;
    private String aliasType;
    private String columnCompleted;
    private String aliasCompleted;
    private String columnConnectorUuid;
    private String aliasConnectorUuid;
    private String columnId;
    private String aliasId;

    public String getSaveAll() {
        return saveAll;
    }

    public void setSaveAll(String saveAll) {
        this.saveAll = saveAll;
    }

    public String getMarkCompleted() {
        return markCompleted;
    }

    public void setMarkCompleted(String markCompleted) {
        this.markCompleted = markCompleted;
    }

    public String getMarkFailed() {
        return markFailed;
    }

    public void setMarkFailed(String markFailed) {
        this.markFailed = markFailed;
    }

    public String getGetAllCreateMutations() {
        return getAllCreateMutations;
    }

    public void setGetAllCreateMutations(String getAllCreateMutations) {
        this.getAllCreateMutations = getAllCreateMutations;
    }

    public String getGetAllUpdateMutations() {
        return getAllUpdateMutations;
    }

    public void setGetAllUpdateMutations(String getAllUpdateMutations) {
        this.getAllUpdateMutations = getAllUpdateMutations;
    }

    public String getColumnUuid() {
        return columnUuid;
    }

    public void setColumnUuid(String columnUuid) {
        this.columnUuid = columnUuid;
    }

    public String getAliasUuid() {
        return aliasUuid;
    }

    public void setAliasUuid(String aliasUuid) {
        this.aliasUuid = aliasUuid;
    }

    public String getColumnStatement() {
        return columnStatement;
    }

    public void setColumnStatement(String columnStatement) {
        this.columnStatement = columnStatement;
    }

    public String getAliasStatement() {
        return aliasStatement;
    }

    public void setAliasStatement(String aliasStatement) {
        this.aliasStatement = aliasStatement;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public String getAliasType() {
        return aliasType;
    }

    public void setAliasType(String aliasType) {
        this.aliasType = aliasType;
    }

    public String getColumnCompleted() {
        return columnCompleted;
    }

    public void setColumnCompleted(String columnCompleted) {
        this.columnCompleted = columnCompleted;
    }

    public String getAliasCompleted() {
        return aliasCompleted;
    }

    public void setAliasCompleted(String aliasCompleted) {
        this.aliasCompleted = aliasCompleted;
    }

    public String getColumnConnectorUuid() {
        return columnConnectorUuid;
    }

    public void setColumnConnectorUuid(String columnConnectorUuid) {
        this.columnConnectorUuid = columnConnectorUuid;
    }

    public String getAliasConnectorUuid() {
        return aliasConnectorUuid;
    }

    public void setAliasConnectorUuid(String aliasConnectorUuid) {
        this.aliasConnectorUuid = aliasConnectorUuid;
    }

    public String getColumnId() {
        return columnId;
    }

    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }
}
