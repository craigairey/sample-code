package com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage;

import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.KassetField;

import java.sql.Timestamp;
import java.util.List;

public interface KassetFieldDao {
    List<KassetField> getAll(Timestamp start, Timestamp finish);

}
