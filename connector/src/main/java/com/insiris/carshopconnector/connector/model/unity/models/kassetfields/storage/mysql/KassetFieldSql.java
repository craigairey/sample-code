package com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class KassetFieldSql {

    public static KassetFieldSql instance;
    private static Logger logger = LoggerFactory.getLogger(KassetFieldSql.class);


    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = KassetFieldSql.class.getClassLoader().getResourceAsStream("kasset_fields_sql.yml");
            instance = mapper.readValue(stream, KassetFieldSql.class);
        } catch (Exception e) {
            logger.error("loading mutation sql has thrown {}", e);
        }
    }

    private String getAll;
    private String aliasId;
    private String aliasValue;
    private String aliasName;
    private String aliasCreatedAt;

    public String getGetAll() {
        return getAll;
    }

    public void setGetAll(String getAll) {
        this.getAll = getAll;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public String getAliasValue() {
        return aliasValue;
    }

    public void setAliasValue(String aliasValue) {
        this.aliasValue = aliasValue;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasCreatedAt() {
        return aliasCreatedAt;
    }

    public void setAliasCreatedAt(String aliasCreatedAt) {
        this.aliasCreatedAt = aliasCreatedAt;
    }
}
