package com.insiris.carshopconnector.connector.model.unity.models.asset.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class AssetSql {
    public static AssetSql instance;
    private static Logger logger = LoggerFactory.getLogger(AssetSql.class);


    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = AssetSql.class.getClassLoader().getResourceAsStream("asset_sql.yml");
            instance = mapper.readValue(stream, AssetSql.class);
        } catch (Exception e) {
            logger.error("loading AssetSql has thrown {}", e);

        }
    }

    private String getAll;
    private String aliasId;
    private String aliasName;
    private String aliasCreatedAt;
    private String aliasUpdatedAt;
    private String aliasGroupsList;

    public String getGetAll() {
        return getAll;
    }

    public void setGetAll(String getAll) {
        this.getAll = getAll;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getAliasCreatedAt() {
        return aliasCreatedAt;
    }

    public void setAliasCreatedAt(String aliasCreatedAt) {
        this.aliasCreatedAt = aliasCreatedAt;
    }

    public String getAliasUpdatedAt() {
        return aliasUpdatedAt;
    }

    public void setAliasUpdatedAt(String aliasUpdatedAt) {
        this.aliasUpdatedAt = aliasUpdatedAt;
    }

    public String getAliasGroupsList() {
        return aliasGroupsList;
    }

    public void setAliasGroupsList(String aliasGroupsList) {
        this.aliasGroupsList = aliasGroupsList;
    }
}
