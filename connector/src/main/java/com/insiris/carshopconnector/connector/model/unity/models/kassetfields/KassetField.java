package com.insiris.carshopconnector.connector.model.unity.models.kassetfields;

import java.sql.Timestamp;

public class KassetField {
    private int id;
    private String value;
    private String name;
    private Timestamp createdAt;

    public KassetField() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
}
