package com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.mysql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class KassetSql {

    public static KassetSql instance;
    private static Logger logger = LoggerFactory.getLogger(KassetSql.class);


    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = KassetSql.class.getClassLoader().getResourceAsStream("kasset_sql.yml");
            instance = mapper.readValue(stream, KassetSql.class);
        } catch (Exception e) {
            logger.error("loading KassetSql has thrown {}", e);
        }
    }

    private String getAll;
    private String columnId;
    private String aliasId;
    private String columnName;
    private String aliasName;
    private String columnAddress;
    private String aliasAddress;
    private String columnDescription;
    private String aliasDescription;
    private String columnPostcode_zip;
    private String aliasPostcode_zip;
    private String columnLatitude;
    private String aliasLatitude;
    private String columnDec;
    private String aliasDec;
    private String columnLongitude;
    private String aliasLongitude;
    private String columnKasset_type;
    private String aliasKasset_type;
    private String columnIs_stock;
    private String aliasIs_stock;
    private String columnStock_count;
    private String aliasStock_count;
    private String columnKasset_template_id;
    private String aliasKasset_template_id;
    private String columnKasset_group_id;
    private String aliasKasset_group_id;
    private String columnCustomer_id;
    private String aliasCustomer_id;
    private String columnNfc_tag_identifier;
    private String aliasNfc_tag_identifier;
    private String columnIdentifier;
    private String aliasIdentifier;
    private String columnShow_on_mobile;
    private String aliasShow_on_mobile;
    private String columnShow_on_mobile_updated;
    private String aliasShow_on_mobile_updated;
    private String columnDeleted_at;
    private String aliasDeleted_at;
    private String columnGuid;
    private String aliasGuid;
    private String columnCreated_at;
    private String aliasCreated_at;
    private String columnUpdated_at;
    private String aliasUpdated_at;
    private String columnRate_plan_id;
    private String aliasRate_plan_id;
    private String columnKasset_status_id;
    private String aliasKasset_status_id;
    private String columnSend_job_history_kasset_count;
    private String aliasSend_job_history_kasset_count;
    private String columnDormant;
    private String aliasDormant;
    private String columnLocked_by_user_id;
    private String aliasLocked_by_user_id;
    private String columnLocked_until;
    private String aliasLocked_until;

    public String getGetAll() {
        return getAll;
    }

    public void setGetAll(String getAll) {
        this.getAll = getAll;
    }

    public String getColumnId() {
        return columnId;
    }

    public void setColumnId(String columnId) {
        this.columnId = columnId;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getAliasName() {
        return aliasName;
    }

    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    public String getColumnAddress() {
        return columnAddress;
    }

    public void setColumnAddress(String columnAddress) {
        this.columnAddress = columnAddress;
    }

    public String getAliasAddress() {
        return aliasAddress;
    }

    public void setAliasAddress(String aliasAddress) {
        this.aliasAddress = aliasAddress;
    }

    public String getColumnDescription() {
        return columnDescription;
    }

    public void setColumnDescription(String columnDescription) {
        this.columnDescription = columnDescription;
    }

    public String getAliasDescription() {
        return aliasDescription;
    }

    public void setAliasDescription(String aliasDescription) {
        this.aliasDescription = aliasDescription;
    }

    public String getColumnPostcode_zip() {
        return columnPostcode_zip;
    }

    public void setColumnPostcode_zip(String columnPostcode_zip) {
        this.columnPostcode_zip = columnPostcode_zip;
    }

    public String getAliasPostcode_zip() {
        return aliasPostcode_zip;
    }

    public void setAliasPostcode_zip(String aliasPostcode_zip) {
        this.aliasPostcode_zip = aliasPostcode_zip;
    }

    public String getColumnLatitude() {
        return columnLatitude;
    }

    public void setColumnLatitude(String columnLatitude) {
        this.columnLatitude = columnLatitude;
    }

    public String getAliasLatitude() {
        return aliasLatitude;
    }

    public void setAliasLatitude(String aliasLatitude) {
        this.aliasLatitude = aliasLatitude;
    }

    public String getColumnDec() {
        return columnDec;
    }

    public void setColumnDec(String columnDec) {
        this.columnDec = columnDec;
    }

    public String getAliasDec() {
        return aliasDec;
    }

    public void setAliasDec(String aliasDec) {
        this.aliasDec = aliasDec;
    }

    public String getColumnLongitude() {
        return columnLongitude;
    }

    public void setColumnLongitude(String columnLongitude) {
        this.columnLongitude = columnLongitude;
    }

    public String getAliasLongitude() {
        return aliasLongitude;
    }

    public void setAliasLongitude(String aliasLongitude) {
        this.aliasLongitude = aliasLongitude;
    }

    public String getColumnKasset_type() {
        return columnKasset_type;
    }

    public void setColumnKasset_type(String columnKasset_type) {
        this.columnKasset_type = columnKasset_type;
    }

    public String getAliasKasset_type() {
        return aliasKasset_type;
    }

    public void setAliasKasset_type(String aliasKasset_type) {
        this.aliasKasset_type = aliasKasset_type;
    }

    public String getColumnIs_stock() {
        return columnIs_stock;
    }

    public void setColumnIs_stock(String columnIs_stock) {
        this.columnIs_stock = columnIs_stock;
    }

    public String getAliasIs_stock() {
        return aliasIs_stock;
    }

    public void setAliasIs_stock(String aliasIs_stock) {
        this.aliasIs_stock = aliasIs_stock;
    }

    public String getColumnStock_count() {
        return columnStock_count;
    }

    public void setColumnStock_count(String columnStock_count) {
        this.columnStock_count = columnStock_count;
    }

    public String getAliasStock_count() {
        return aliasStock_count;
    }

    public void setAliasStock_count(String aliasStock_count) {
        this.aliasStock_count = aliasStock_count;
    }

    public String getColumnKasset_template_id() {
        return columnKasset_template_id;
    }

    public void setColumnKasset_template_id(String columnKasset_template_id) {
        this.columnKasset_template_id = columnKasset_template_id;
    }

    public String getAliasKasset_template_id() {
        return aliasKasset_template_id;
    }

    public void setAliasKasset_template_id(String aliasKasset_template_id) {
        this.aliasKasset_template_id = aliasKasset_template_id;
    }

    public String getColumnKasset_group_id() {
        return columnKasset_group_id;
    }

    public void setColumnKasset_group_id(String columnKasset_group_id) {
        this.columnKasset_group_id = columnKasset_group_id;
    }

    public String getAliasKasset_group_id() {
        return aliasKasset_group_id;
    }

    public void setAliasKasset_group_id(String aliasKasset_group_id) {
        this.aliasKasset_group_id = aliasKasset_group_id;
    }

    public String getColumnCustomer_id() {
        return columnCustomer_id;
    }

    public void setColumnCustomer_id(String columnCustomer_id) {
        this.columnCustomer_id = columnCustomer_id;
    }

    public String getAliasCustomer_id() {
        return aliasCustomer_id;
    }

    public void setAliasCustomer_id(String aliasCustomer_id) {
        this.aliasCustomer_id = aliasCustomer_id;
    }

    public String getColumnNfc_tag_identifier() {
        return columnNfc_tag_identifier;
    }

    public void setColumnNfc_tag_identifier(String columnNfc_tag_identifier) {
        this.columnNfc_tag_identifier = columnNfc_tag_identifier;
    }

    public String getAliasNfc_tag_identifier() {
        return aliasNfc_tag_identifier;
    }

    public void setAliasNfc_tag_identifier(String aliasNfc_tag_identifier) {
        this.aliasNfc_tag_identifier = aliasNfc_tag_identifier;
    }

    public String getColumnIdentifier() {
        return columnIdentifier;
    }

    public void setColumnIdentifier(String columnIdentifier) {
        this.columnIdentifier = columnIdentifier;
    }

    public String getAliasIdentifier() {
        return aliasIdentifier;
    }

    public void setAliasIdentifier(String aliasIdentifier) {
        this.aliasIdentifier = aliasIdentifier;
    }

    public String getColumnShow_on_mobile() {
        return columnShow_on_mobile;
    }

    public void setColumnShow_on_mobile(String columnShow_on_mobile) {
        this.columnShow_on_mobile = columnShow_on_mobile;
    }

    public String getAliasShow_on_mobile() {
        return aliasShow_on_mobile;
    }

    public void setAliasShow_on_mobile(String aliasShow_on_mobile) {
        this.aliasShow_on_mobile = aliasShow_on_mobile;
    }

    public String getColumnShow_on_mobile_updated() {
        return columnShow_on_mobile_updated;
    }

    public void setColumnShow_on_mobile_updated(String columnShow_on_mobile_updated) {
        this.columnShow_on_mobile_updated = columnShow_on_mobile_updated;
    }

    public String getAliasShow_on_mobile_updated() {
        return aliasShow_on_mobile_updated;
    }

    public void setAliasShow_on_mobile_updated(String aliasShow_on_mobile_updated) {
        this.aliasShow_on_mobile_updated = aliasShow_on_mobile_updated;
    }

    public String getColumnDeleted_at() {
        return columnDeleted_at;
    }

    public void setColumnDeleted_at(String columnDeleted_at) {
        this.columnDeleted_at = columnDeleted_at;
    }

    public String getAliasDeleted_at() {
        return aliasDeleted_at;
    }

    public void setAliasDeleted_at(String aliasDeleted_at) {
        this.aliasDeleted_at = aliasDeleted_at;
    }

    public String getColumnGuid() {
        return columnGuid;
    }

    public void setColumnGuid(String columnGuid) {
        this.columnGuid = columnGuid;
    }

    public String getAliasGuid() {
        return aliasGuid;
    }

    public void setAliasGuid(String aliasGuid) {
        this.aliasGuid = aliasGuid;
    }

    public String getColumnCreated_at() {
        return columnCreated_at;
    }

    public void setColumnCreated_at(String columnCreated_at) {
        this.columnCreated_at = columnCreated_at;
    }

    public String getAliasCreated_at() {
        return aliasCreated_at;
    }

    public void setAliasCreated_at(String aliasCreated_at) {
        this.aliasCreated_at = aliasCreated_at;
    }

    public String getColumnUpdated_at() {
        return columnUpdated_at;
    }

    public void setColumnUpdated_at(String columnUpdated_at) {
        this.columnUpdated_at = columnUpdated_at;
    }

    public String getAliasUpdated_at() {
        return aliasUpdated_at;
    }

    public void setAliasUpdated_at(String aliasUpdated_at) {
        this.aliasUpdated_at = aliasUpdated_at;
    }

    public String getColumnRate_plan_id() {
        return columnRate_plan_id;
    }

    public void setColumnRate_plan_id(String columnRate_plan_id) {
        this.columnRate_plan_id = columnRate_plan_id;
    }

    public String getAliasRate_plan_id() {
        return aliasRate_plan_id;
    }

    public void setAliasRate_plan_id(String aliasRate_plan_id) {
        this.aliasRate_plan_id = aliasRate_plan_id;
    }

    public String getColumnKasset_status_id() {
        return columnKasset_status_id;
    }

    public void setColumnKasset_status_id(String columnKasset_status_id) {
        this.columnKasset_status_id = columnKasset_status_id;
    }

    public String getAliasKasset_status_id() {
        return aliasKasset_status_id;
    }

    public void setAliasKasset_status_id(String aliasKasset_status_id) {
        this.aliasKasset_status_id = aliasKasset_status_id;
    }

    public String getColumnSend_job_history_kasset_count() {
        return columnSend_job_history_kasset_count;
    }

    public void setColumnSend_job_history_kasset_count(String columnSend_job_history_kasset_count) {
        this.columnSend_job_history_kasset_count = columnSend_job_history_kasset_count;
    }

    public String getAliasSend_job_history_kasset_count() {
        return aliasSend_job_history_kasset_count;
    }

    public void setAliasSend_job_history_kasset_count(String aliasSend_job_history_kasset_count) {
        this.aliasSend_job_history_kasset_count = aliasSend_job_history_kasset_count;
    }

    public String getColumnDormant() {
        return columnDormant;
    }

    public void setColumnDormant(String columnDormant) {
        this.columnDormant = columnDormant;
    }

    public String getAliasDormant() {
        return aliasDormant;
    }

    public void setAliasDormant(String aliasDormant) {
        this.aliasDormant = aliasDormant;
    }

    public String getColumnLocked_by_user_id() {
        return columnLocked_by_user_id;
    }

    public void setColumnLocked_by_user_id(String columnLocked_by_user_id) {
        this.columnLocked_by_user_id = columnLocked_by_user_id;
    }

    public String getAliasLocked_by_user_id() {
        return aliasLocked_by_user_id;
    }

    public void setAliasLocked_by_user_id(String aliasLocked_by_user_id) {
        this.aliasLocked_by_user_id = aliasLocked_by_user_id;
    }

    public String getColumnLocked_until() {
        return columnLocked_until;
    }

    public void setColumnLocked_until(String columnLocked_until) {
        this.columnLocked_until = columnLocked_until;
    }

    public String getAliasLocked_until() {
        return aliasLocked_until;
    }

    public void setAliasLocked_until(String aliasLocked_until) {
        this.aliasLocked_until = aliasLocked_until;
    }
}
