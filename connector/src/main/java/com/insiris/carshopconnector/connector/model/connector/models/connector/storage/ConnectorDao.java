package com.insiris.carshopconnector.connector.model.connector.models.connector.storage;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;

public interface ConnectorDao {
    Connector get();
    void save(Connector connector);
}
