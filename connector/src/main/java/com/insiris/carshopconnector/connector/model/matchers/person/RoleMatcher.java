package com.insiris.carshopconnector.connector.model.matchers.person;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.AssetGroup;
import com.insiris.carshopconnector.connector.shared.Converters;

import java.util.ArrayList;
import java.util.List;

public class RoleMatcher extends Matcher {
    private List<String> roles = new ArrayList<String>(){{
        add("Team Leaders");
        add("Inbound");
        add("Tunnel Runners");
        add("Tunnel Runners A");
        add("Tunnel Runners B");
        add("Imaging Runners");
        add("Outbound");
        add("OOP");
        add("Gate");
        add("Body Insp");
        add("Ramp");
        add("Road Test");
        add("OOP/Hitachi");
        add("Mechanical");
        add("Strip/Fit/Polish");
        add("Prep/Panel");
        add("2 Pack Prime");
        add("Bag");
        add("Paint");
        add("SR14");
        add("QC");
        add("Parts");
    }};

    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(roles.contains(((AssetGroup)toMatch).getName()))
            return departmentChangedSQL((AssetGroup)toMatch, connector);
        else
            return null;
    }

    private Mutation departmentChangedSQL(AssetGroup assetGroup, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "UPDATE "+connector.getDbName()+".dbo.people SET role = \\'"+assetGroup.getName()+"\\', updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+assetGroup.getId();
        return new UpdateMutation(sql, connector.getUuid());
    }

}