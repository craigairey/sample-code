package com.insiris.carshopconnector.connector.model.matchers.event;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;

public class WipPolishPausedMatcher extends JobLogEntryMatcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if("Polish".equals(((JobLogEntry)toMatch).getName()) && "paused".equals(((JobLogEntry)toMatch).getAction()))
            return generateMutation((JobLogEntry)toMatch, connector, "WipPolishPaused");
        else
            return null;
    }
}
