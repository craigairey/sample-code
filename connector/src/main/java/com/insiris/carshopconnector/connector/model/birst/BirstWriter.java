package com.insiris.carshopconnector.connector.model.birst;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BirstWriter {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private DatabaseConnector databaseConnector;
    private Connection connection;
    private MutationDao mutationDao;

    @Inject
    public BirstWriter(@Named("mssql")DatabaseConnector databaseConnector, MutationDao mutationDao) {
        this.databaseConnector = databaseConnector;
        this.connection = databaseConnector.getConnection();
        this.mutationDao = mutationDao;
    }

    private MutationDao getMutationDao() {
        return this.mutationDao;
    }
    
    private void setMutationDao(MutationDao mutationDao) {
        this.mutationDao = mutationDao;
    }

    public void insertRows(List<Mutation> mutations){
        Statement statement = null;

        String sql ="";


        List<Mutation> completed = new ArrayList<>();
        List<Mutation> failed = new ArrayList<>();
        try {
            statement = connection.createStatement();

            for(Mutation mutation : mutations) {
                int response = 0;
                sql = mutation.getStatement();
                statement.addBatch(mutation.getStatement());
            }
            int[] responses = statement.executeBatch();
            if (responses.length > 0) {
                for(int i = 0; i < responses.length; i++){
                    if(responses[i] == 1){
                        mutations.get(i).setCompleted(true);
                        completed.add(mutations.get(i));
                    }else{
                        failed.add(mutations.get(i));
                    }
                }
            }
        } catch (SQLException e) {
            logger.error(sql);
            logger.error("inserting into db has thrown: {}",  new Object[] {e.getMessage(), e});

        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        if(completed.size() >0)
            mutationDao.markCompleted(completed);
        if(failed.size() > 0)
            mutationDao.markFailed(failed);
    }
}
