package com.insiris.carshopconnector.connector.model.handlers;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;

import java.util.ArrayList;
import java.util.List;

public class Handler {

    private List<Matcher> matchers;
    private MutationDao mutationDao;

    public Handler(List<Matcher> matchers, MutationDao mutationDao) {
        this.matchers = matchers;
        this.mutationDao = mutationDao;
    }

    protected List<Matcher> getMatchers() {
        if (this.matchers == null) {
            this.matchers = new ArrayList<>();
        }
        return this.matchers;
    }
    protected void setMatchers(List<Matcher> matchers) {
        this.matchers = matchers;
    }
    public MutationDao getMutationDao() {
        return this.mutationDao;
    }
    private void setMutationDao(MutationDao mutationDao) {
        this.mutationDao = mutationDao;
    }
    public boolean match(List toProcess, Connector connector){
        List<Mutation> mutations = new ArrayList<>();

        for(Matcher matcher : this.getMatchers()){
            for(Object kasset : toProcess){
                Mutation mutation = matcher.match(kasset, connector);
                if (mutation != null)
                    mutations.add(mutation);
            }
        }
        getMutationDao().saveAll(mutations);
        return false;
    };
}
