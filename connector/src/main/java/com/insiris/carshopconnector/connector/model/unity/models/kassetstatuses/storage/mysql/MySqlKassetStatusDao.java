package com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.mysql;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.KassetStatus;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.KassetStatusDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlKassetStatusDao implements KassetStatusDao {
    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlKassetStatusDao(@Named("mysql")DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }

    @Override
    public List<KassetStatus> getAll(Timestamp start, Timestamp finish) {
        logger.info("getting all kasset statuses start {} finish {}", start, finish);
        PreparedStatement statement = null;
        List<KassetStatus> results = new ArrayList<>();
        try {
            statement = connection.prepareStatement(KassetStatusSql.instance.getGetAll());
            statement.setTimestamp(1, start);
            statement.setTimestamp(2, finish);
            logger.info("kasset statuses get all {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = KassetStatusMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all kasset statuses start {} finish {} has thrown: {}", start, finish, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }
}
