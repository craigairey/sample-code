package com.insiris.carshopconnector.connector.model.watchers;


import com.google.inject.Inject;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.handlers.AssetGroupHandler;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.matchers.person.*;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.AssetGroup;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.storage.AssetGroupDao;


import java.util.ArrayList;
import java.util.List;

public class AssetGroupWatcher extends Watcher {

    private AssetGroupDao dao;
    private ConnectorDao connectorDao;
    private AssetGroupHandler handler;

    @Inject
    public AssetGroupWatcher(AssetGroupDao dao, ConnectorDao connectorDao,  MutationDao mutationDao){
        this.dao = dao;
        this.handler = new AssetGroupHandler(
                new ArrayList<Matcher>(){{
                    add(new AgencyMatcher());
                    add(new AreaMatcher());
                    add(new DepartmentMatcher());
                    add(new EmployeeStatusMatcher());
                    add(new RoleMatcher());
                    add(new ShiftMatcher());
                }},
                mutationDao
        );
        this.connectorDao = connectorDao;
    }

    @Override
    public void onPoll(Connector connector){
        List<AssetGroup> assetGroups = dao.getAll(connector.getStart(),connector.getEnd());
        boolean handlerCompleted = false;

        if(assetGroups != null && assetGroups.size() > 0) {
            handlerCompleted = handler.match(assetGroups, connector);
            connector.setLastJobTimestamp(assetGroups.get(assetGroups.size() - 1).getCreatedAt());
        }
    };
}
