package com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.mysql;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.StringJoiner;

public class MySqlMutationDao implements MutationDao {
    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlMutationDao(@Named("mysqlConMut") DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }


    @Override
    public void saveAll(List<Mutation> mutations) {
        Statement statement = null;
        String sql = "";
        try{
            statement = connection.createStatement();
            for(Mutation mutation: mutations) {
               sql = "";

                sql = String.format(MutationSql.instance.getSaveAll(),
                        mutation.getStatement(),
                        mutation.getType(),
                        mutation.isCompleted() ? 1 : 0,
                        mutation.getConnectorUuid(),
                        mutation.getUuid()
                );

                logger.info("inserting mutation {}", sql);

                statement.addBatch(sql);
            }
            statement.executeBatch();
        } catch (SQLException e) {
            logger.info("inserting mutation {}", sql);
            logger.error("inserting mutation has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
    }

    @Override
    public void markCompleted(List<Mutation> mutations) {
        StringJoiner joiner = new StringJoiner(",");
        for(Mutation mutation: mutations){
            joiner.add(String.valueOf(mutation.getId()));
        }
        logger.info("marking complete for {}", joiner.toString());
        Statement statement = null;
        try{
            statement = connection.createStatement();
            statement.executeUpdate(String.format(MutationSql.instance.getMarkCompleted(), joiner.add(joiner.toString())));
        } catch (SQLException e) {
            logger.error("marking complete for {} has thrown: {}", joiner.toString(), new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
    }

    @Override
    public void markFailed(List<Mutation> mutations) {
        StringJoiner joiner = new StringJoiner(",");
        for(Mutation mutation: mutations){
            joiner.add(String.valueOf(mutation.getId()));
        }
        logger.info("marking complete for {}", joiner.toString());
        Statement statement = null;
        try{
            statement = connection.createStatement();
            statement.executeUpdate(String.format(MutationSql.instance.getMarkFailed(), joiner.add(joiner.toString())));
        } catch (SQLException e) {
            logger.error("marking failed for {} has thrown: {}", joiner.toString(), new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
    }

    @Override
    public List<Mutation> getAllCreateMutations() {
        logger.info("getting all create mutations");
        Statement statement = null;
        List<Mutation> results = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(MutationSql.instance.getGetAllCreateMutations());
            results = MutationMapper.mapResults(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("getting all create mutations has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }

    @Override
    public List<Mutation> getAllUpdateMutations() {
        logger.info("getting all update mutations");
        Statement statement = null;
        List<Mutation> results = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(MutationSql.instance.getGetAllUpdateMutations());
            results = MutationMapper.mapResults(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("getting all update mutations has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }
}
