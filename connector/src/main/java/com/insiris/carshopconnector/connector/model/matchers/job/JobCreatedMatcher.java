package com.insiris.carshopconnector.connector.model.matchers.job;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.CreateMutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.jobs.Job;
import com.insiris.carshopconnector.connector.shared.Converters;

public class JobCreatedMatcher extends Matcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {

        return jobCreatedSQL((Job)toMatch, connector);
    }

    private Mutation jobCreatedSQL(Job job, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "INSERT INTO "+connector.getDbName()+".dbo.jobs (id, vehicle_id, role, total_estimated_hours, estimated_hours_completed, actual_hours_completed, estimate_value, description, created_at, updated_at, deleted_at, connector_log_run_uuid, mutation_uuid, workflow) " +
                "VALUES ("+job.getId()+", "+job.getVehicleId()+", \\'"+job.getGroup()+"\\', "+job.getTotalEstimatedHours()+", null, null, " + getEstimateValueFromNotes(job.getNotes()) + ", \\'"+escapeChars(job.getDescription())+"\\', \\'"+ now+"\\', \\'"+ now+"\\', null, \\'%1$s\\', \\'%2$s\\', \\'"+job.getWorkFlowName()+"\\')";
        return new CreateMutation(sql, connector.getUuid());
    }

    private String escapeChars(String toEscape){
        return toEscape.replace("'", "\\'\\'");
    }

    private String getEstimateValueFromNotes(String toCheck){
        if (toCheck == null || toCheck.isEmpty())
            return "null";

        java.util.regex.Pattern pattern = java.util.regex.Pattern.compile("\\|(\\d+)\\.(\\d+)\\|(?!.*\\|(\\d+).(\\d+)\\|)"); //match |99.99| with neg look forward (last match)
        java.util.regex.Matcher matcher = pattern.matcher(toCheck);

        if (matcher.find())
            return matcher.group(0).replace("|", "");
        else
            return "null";
    }

}
