package com.insiris.carshopconnector.connector.shared.enums;

/**
 * Created by craigairey on 23/05/2016.
 */
public class JobStatuses {
    public final static String ACTION_UNALLOCATED = "unallocated";
    public final static String ACTION_ALLOCATED = "allocated";
    public final static String ACTION_DISPATCHED = "dispatched";
    public final static String ACTION_DOWNLOADED = "downloaded";
    public final static String ACTION_TRAVELLING = "travelling";
    public final static String ACTION_IN_PROGRESS = "in_progress";
    public final static String ACTION_COMPLETED = "completed";
    public final static String ACTION_REVIEWED = "reviewed";
    public final static String ACTION_CLOSED = "closed";
    public final static String ACTION_REJECTED = "rejected";
    public final static String ACTION_FAILED = "failed";
    public final static String ACTION_MODIFIED = "modified";
    public final static String ACTION_PAUSED = "paused";
    public final static String ACTION_NEW = "";

    public enum Statuses {
        ACTION_UNALLOCATED("unallocated"),
        ACTION_ALLOCATED("allocated"),
        ACTION_DISPATCHED("dispatched"),
        ACTION_DOWNLOADED("downloaded"),
        ACTION_TRAVELLING("travelling"),
        ACTION_IN_PROGRESS("in_progress"),
        ACTION_COMPLETED("completed"),
        ACTION_REVIEWED("reviewed"),
        ACTION_CLOSED("closed"),
        ACTION_REJECTED("rejected"),
        ACTION_FAILED("failed"),
        ACTION_MODIFIED("modified"),
        ACTION_PAUSED("paused"),
        ACTION_NEW("");

        public final String text;

        Statuses(String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

}
