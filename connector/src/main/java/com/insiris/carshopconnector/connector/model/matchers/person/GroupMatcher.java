package com.insiris.carshopconnector.connector.model.matchers.person;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.asset.Asset;
import com.insiris.carshopconnector.connector.shared.Converters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class GroupMatcher extends Matcher {
    private final Map<Integer, String> department = new HashMap<Integer, String>(){{
        put(9, "COMPOUND");
        put(10, "INSPECTION");
        put(5, "PARTS");
    }};
    private final Map<Integer, String> area = new HashMap<Integer, String>(){{
        put(1, "MECH/MOT");
        put(2, "BODY");
        put(3, "SR14");
        put(4, "QC");
        put(9, "COMPOUND");
        put(10, "INSPECTION");
        put(5, "PARTS");
    }};
    private final Map<Integer, String> role = new HashMap<Integer, String>(){{
        put(11,"Inbound");
        put(12,"Out of program");
        put(14,"Tunnel Runners A");
        put(15,"Tunnel Runners B");
        put(16,"Outbound ");
        put(17,"Imaging/Runner A");
        put(18,"Imaging/Runner B");
        put(19,"Team Leaders");
        put(22,"Body Insp");
        put(23,"Ramp");
        put(24,"Road Test");
        put(25,"OOP/Hitachi");
        put(29,"Mechanical");
        put(30,"MOT");
        put(36,"Strip/Fit/Polish");
        put(37,"Prep/Panel");
        put(38,"Bag");
        put(39,"Paint");
        put(46,"Insp Admin");
        put(49,"Maintenance Man");
    }};
    private final Map<Integer, String> shift = new HashMap<Integer, String>(){{
        put(31,"Days A");
        put(32,"Days B");
        put(33,"Mon-Fri");
        put(34,"Nights A");
        put(35,"Nights B");
    }};
    private final Map<Integer, String> employeeStatus = new HashMap<Integer, String>(){{
        put(20,"EMPLOYED");
        put(21,"AGENCY");
        put(47,"Apprentice");
        put(45,"LEFT");
    }};
    private final Map<Integer, String> agency = new HashMap<Integer, String>(){{
        put(40,"AutoRefurb");
        put(41,"Skilltech");
        put(42,"Meenz");
        put(43,"Autoscot");
        put(44,"Igloo");
        put(48,"Workforce");
    }};
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        // TODO: 23/02/2018 chaneg get start to lastUpdated & last createAt
        if(((Asset)toMatch).getUpdatedAt().toInstant().toEpochMilli() >= connector.getStart().toInstant().toEpochMilli() && ((Asset)toMatch).getUpdatedAt().toInstant().toEpochMilli() <= connector.getEnd().toInstant().toEpochMilli())
            return groupMatcherSQL((Asset)toMatch, connector);
        else
            return null;
    }

    private Mutation groupMatcherSQL(Asset asset, Connector connector){
        String now = Converters.currentTimestampUTC();
        List<Integer> groups = createlistOfGroupsFromString(asset.getGroupsList());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("UPDATE "+connector.getDbName()+".dbo.people SET ");
        stringBuilder.append("department = " + createStringForGroups(groups, department) + ", ");
        stringBuilder.append("area = " + createStringForGroups(groups, area) + ", ");
        stringBuilder.append("role = " + createStringForGroups(groups, role) + ", ");
        stringBuilder.append("shift = " + createStringForGroups(groups, shift) + ", ");
        stringBuilder.append("employee_status = " + createStringForGroups(groups, employeeStatus) + ", ");
        stringBuilder.append("agency = " + createStringForGroups(groups, agency) + ", ");
        stringBuilder.append("updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+asset.getId());
        return new UpdateMutation(stringBuilder.toString(), connector.getUuid());
    }

    private String createStringForGroups(List<Integer> assignedGroups, Map<Integer, String> groups ){
        if (assignedGroups.size() == 0)
            return "null";
        else{
            List<String> groupValues = new ArrayList<>();

            assignedGroups.forEach((i ->{
                if(groups.containsKey(i))
                    groupValues.add(groups.get(i));
            }));
            String toReturn = groupValues.stream().collect(Collectors.joining(","));
            return toReturn == null || toReturn.isEmpty() ? "null" : "\\'"+toReturn+"\\'";
        }
    }

    private List<Integer> createlistOfGroupsFromString(String toCheck) {
        List<Integer> groups = new ArrayList<>();
        if(toCheck == null)
            return groups;
        Pattern pattern = java.util.regex.Pattern.compile("\\[(\\d+)\\]"); //match [9999]
        java.util.regex.Matcher matcher = pattern.matcher(toCheck);

        while (matcher.find()) {
            groups.add(Integer.parseInt(matcher.group().replace("[", "").replace("]", ""))); // convert [9999] from string to int
        }

        return groups;
    }
}