package com.insiris.carshopconnector.connector.database;

import java.sql.Connection;

public interface DatabaseConnector {
    Connection getConnection();
}
