package com.insiris.carshopconnector.connector.di;


import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.database.mssql.MsSqlDatabaseConnector;
import com.insiris.carshopconnector.connector.database.mysql.MySqlConMutDatabaseConnector;
import com.insiris.carshopconnector.connector.database.mysql.MySqlDatabaseConnector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.asset.storage.AssetDao;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.storage.AssetGroupDao;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.storage.mysql.MySqlAssetGroupDao;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.JobLogEntryDao;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.mysql.MySqlJobLogEntryDao;
import com.insiris.carshopconnector.connector.model.unity.models.jobs.storage.JobDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage.KassetFieldDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage.mysql.MySqlKassetFieldDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.KassetDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.KassetStatusDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.mysql.MySqlKassetStatusDao;
import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.storage.WiqEventDao;
import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.storage.mysql.MySqlWiqEventDao;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.mysql.MySqlConnectorDao;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.mysql.MySqlMutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.asset.storage.mysql.MySqlAssetDao;
import com.insiris.carshopconnector.connector.model.unity.models.jobs.storage.mysql.MySqlJobDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.mysql.MySqlKassetDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarshopConnectorDependenciesModule extends AbstractModule {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    protected void configure() {
        bind(KassetDao.class).to(MySqlKassetDao.class);
        bind(KassetStatusDao.class).to(MySqlKassetStatusDao.class);
        bind(KassetFieldDao.class).to(MySqlKassetFieldDao.class);
        bind(JobLogEntryDao.class).to(MySqlJobLogEntryDao.class);
        bind(JobDao.class).to(MySqlJobDao.class);
        bind(AssetDao.class).to(MySqlAssetDao.class);
        bind(AssetGroupDao.class).to(MySqlAssetGroupDao.class);
        bind(WiqEventDao.class).to(MySqlWiqEventDao.class);
        bind(MutationDao.class).to(MySqlMutationDao.class);
        bind(ConnectorDao.class).to(MySqlConnectorDao.class);
        bind(DatabaseConnector.class).annotatedWith(
                Names.named("mysql")).to(MySqlDatabaseConnector.class);
        bind(DatabaseConnector.class).annotatedWith(
                Names.named("mysqlConMut")).to(MySqlConMutDatabaseConnector.class);
        bind(DatabaseConnector.class).annotatedWith(
                Names.named("mssql")).to(MsSqlDatabaseConnector.class);
    }
}