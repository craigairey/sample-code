package com.insiris.carshopconnector.connector.model.matchers.event;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.CreateMutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;
import com.insiris.carshopconnector.connector.shared.Converters;

public abstract class JobLogEntryMatcher extends Matcher {
    protected Mutation generateMutation(JobLogEntry jobLogEntry, Connector connector, String eventType){
        String now = Converters.currentTimestampUTC();
        String sql = "INSERT INTO "+connector.getDbName()+".dbo.events(event_category ,event_type,event_description ,timestamp , duration ,vehicle_id ,person_id , job_id, created_at ,updated_at ,connector_log_run_uuid , mutation_uuid) VALUES (\\'vehicle_status\\', \\'"+eventType+"\\', \\'"+escapeChars(jobLogEntry.getJobDescription())+"\\',  \\'"+Converters.timestampFormatter().format(jobLogEntry.getGeneratedAt())+"\\',  null, "+jobLogEntry.getKassetId()+", "+jobLogEntry.getPersonId()+", "+jobLogEntry.getJobId()+", \\'"+now+"\\', \\'"+now+"\\', \\'%1$s\\', \\'%2$s\\')";
        return new CreateMutation(sql, connector.getUuid());
    }

    private String escapeChars(String toEscape){
        return toEscape.replace("'", "\\'\\'");
    }

}
