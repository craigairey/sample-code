package com.insiris.carshopconnector.connector.model.matchers.person;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.AssetGroup;
import com.insiris.carshopconnector.connector.shared.Converters;

import java.util.ArrayList;
import java.util.List;

public class AreaMatcher extends Matcher {
    private List<String> areas = new ArrayList<String>(){{
        add("COMPOUND");
        add("INSPECTION");
        add("MECH/MOT");
        add("BODY");
        add("SMART");
        add("QC");
        add("PARTS");
        add("SR14");
    }};

    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(areas.contains(((AssetGroup)toMatch).getName()))
            return areaChangedSQL((AssetGroup)toMatch, connector);
        else
            return null;
    }

    private Mutation areaChangedSQL(AssetGroup assetGroup, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "UPDATE "+connector.getDbName()+".dbo.people SET area = \\'"+assetGroup.getName()+"\\', updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+assetGroup.getId();
        return new UpdateMutation(sql, connector.getUuid());
    }

}