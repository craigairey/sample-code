package com.insiris.carshopconnector.connector.model.unity.models.kassets.storage;


import com.insiris.carshopconnector.connector.model.unity.models.kassets.Kasset;

import java.sql.Timestamp;
import java.util.List;

public interface KassetDao {
    List<Kasset> getAll(Timestamp start, Timestamp finish);
}
