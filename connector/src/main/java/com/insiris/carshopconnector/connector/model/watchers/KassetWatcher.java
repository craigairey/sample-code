package com.insiris.carshopconnector.connector.model.watchers;

import com.google.inject.Inject;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.handlers.KassetHandler;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.matchers.vehicle.VehicleCreatedMatcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.Kasset;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.KassetDao;

import java.util.ArrayList;
import java.util.List;

public class KassetWatcher extends Watcher {

    private KassetDao dao;
    private ConnectorDao connectorDao;
    private KassetHandler handler;

    @Inject
    public KassetWatcher(KassetDao dao, ConnectorDao connectorDao,  MutationDao mutationDao){
        this.dao = dao;
        this.handler = new KassetHandler(
                new ArrayList<Matcher>(){{
                    add(new VehicleCreatedMatcher());
                }},
                mutationDao
        );
        this.connectorDao = connectorDao;
    }

    @Override
    public void onPoll(Connector connector){
        List<Kasset> kassets = dao.getAll(connector.getStart(),connector.getEnd());
        boolean handlerCompleted = false;

        if(kassets != null && kassets.size() > 0) {
            handlerCompleted = handler.match(kassets, connector);
            connector.setLastKassetTimestamp(kassets.get(kassets.size() - 1).getCreated_at());
        }
    };
}
