package com.insiris.carshopconnector.connector.model.watchers;


import com.google.inject.Inject;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.handlers.JobLogEntryHandler;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.matchers.event.*;
import com.insiris.carshopconnector.connector.model.matchers.job.ActualHoursCompletedMatcher;
import com.insiris.carshopconnector.connector.model.matchers.job.EstimatedHoursCompletedMatcher;
import com.insiris.carshopconnector.connector.model.matchers.person.InProgressCompletedMatcher;
import com.insiris.carshopconnector.connector.model.matchers.vehicle.CurrentWorkflowStatusMatcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.JobLogEntryDao;

import java.util.ArrayList;
import java.util.List;

public class JobLogEntryWatcher extends Watcher {

    private JobLogEntryDao dao;
    private ConnectorDao connectorDao;
    private JobLogEntryHandler handler;
    protected JobLogEntryDao getDao() {
        return this.dao;
    }

    protected void setDao(JobLogEntryDao dao) {
        this.dao = dao;
    }

    @Inject
    public JobLogEntryWatcher(JobLogEntryDao dao, ConnectorDao connectorDao, MutationDao mutationDao){
        this.dao = dao;
        this.handler = new JobLogEntryHandler(
                new ArrayList<Matcher>(){{
                    add(new BodyInspectionFinishMatcher());
                    add(new BodyInspectionStartMatcher());
                    add(new FollowUpRampInspectionFinishMatcher());
                    add(new FollowUpRampInspectionStartMatcher());
                    add(new RampInspectionFinishMatcher());
                    add(new RampInspectionStartMatcher());
                    add(new RoadTestFinishMatcher());
                    add(new RoadTestStartMatcher());
                    add(new TunnelReceiptFinishMatcher());
                    add(new TunnelReceiptStartMatcher());
                    add(new WipBagFailedMatcher());
                    add(new WipBagFinishMatcher());
                    add(new WipBagPausedMatcher());
                    add(new WipBagStartMatcher());
                    add(new WipFitFailedMatcher());
                    add(new WipFitFinishMatcher());
                    add(new WipFitPausedMatcher());
                    add(new WipFitStartMatcher());
                    add(new WipMechFailedMatcher());
                    add(new WipMechFinishMatcher());
                    add(new WipMechPausedMatcher());
                    add(new WipMechStartMatcher());
                    add(new WipMotFailedMatcher());
                    add(new WipMotFinishMatcher());
                    add(new WipMotPausedMatcher());
                    add(new WipMotStartMatcher());
                    add(new WipPaintFailedMatcher());
                    add(new WipPaintFinishMatcher());
                    add(new WipPaintPausedMatcher());
                    add(new WipPaintStartMatcher());
                    add(new WipPanelFailedMatcher());
                    add(new WipPanelFinishMatcher());
                    add(new WipPanelPausedMatcher());
                    add(new WipPanelStartMatcher());
                    add(new WipPolishFailedMatcher());
                    add(new WipPolishFinishMatcher());
                    add(new WipPolishPausedMatcher());
                    add(new WipPolishStartMatcher());
                    add(new WipPrepFailedMatcher());
                    add(new WipPrepFinishMatcher());
                    add(new WipPrepPausedMatcher());
                    add(new WipPrepStartMatcher());
                    add(new WipSmartFailedMatcher());
                    add(new WipSmartFinishMatcher());
                    add(new WipSmartPausedMatcher());
                    add(new WipSmartStartMatcher());
                    add(new WipStripFailedMatcher());
                    add(new WipStripFinishMatcher());
                    add(new WipStripPausedMatcher());
                    add(new WipStripStartMatcher());
                    add(new InProgressCompletedMatcher());
                    add(new ActualHoursCompletedMatcher(dao));
                    add(new EstimatedHoursCompletedMatcher());
                    add(new CurrentWorkflowStatusMatcher());

                }},
                mutationDao
        );
        this.connectorDao = connectorDao;
    }

    @Override
    public void onPoll(Connector connector){
        List<JobLogEntry> jobLogEntries = dao.getAll(connector.getStart(),connector.getEnd());
        boolean handlerCompleted = false;

        if(jobLogEntries != null && jobLogEntries.size() > 0) {
            handlerCompleted = handler.match(jobLogEntries, connector);
            connector.setLastKassetFieldTimestamp(jobLogEntries.get(jobLogEntries.size() - 1).getGeneratedAt());
        }
    };

}
