package com.insiris.carshopconnector.connector.model.connector.models.connector.storage.mysql;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConnectorMapper {
    public static List<Connector> mapResults(ResultSet resultSet) throws SQLException {
        List<Connector> connectors = new ArrayList<>();
        while(resultSet.next()) {
            Connector connector = new Connector();

            connector.setId(resultSet.getInt(ConnectorSql.instance.getAliasId()));
            connector.setStart(resultSet.getTimestamp(ConnectorSql.instance.getAliasStart()));
            connector.setEnd(resultSet.getTimestamp(ConnectorSql.instance.getAliasEnd()));
            connector.setLastKassetTimestamp(resultSet.getTimestamp(ConnectorSql.instance.getAliasLastKassetTimestamp()));
            connector.setLastKassetStatusTimestamp(resultSet.getTimestamp(ConnectorSql.instance.getAliasLastKassetStatusTimestamp()));
            connector.setLastKassetFieldTimestamp(resultSet.getTimestamp(ConnectorSql.instance.getAliasLastKassetFieldTimestamp()));
            connector.setLastJobLogEntryTimestamp(resultSet.getTimestamp(ConnectorSql.instance.getAliasLastJobLogEntryTimestamp()));
            connector.setLastJobTimestamp(resultSet.getTimestamp(ConnectorSql.instance.getAliasLastJobTimestamp()));
            connectors.add(connector);
        }
        return connectors;
    }
}
