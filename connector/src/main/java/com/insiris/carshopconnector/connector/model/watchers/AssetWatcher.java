package com.insiris.carshopconnector.connector.model.watchers;

import com.google.inject.Inject;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.handlers.AssetHandler;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.matchers.person.GroupMatcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.asset.Asset;
import com.insiris.carshopconnector.connector.model.unity.models.asset.storage.AssetDao;

import java.util.ArrayList;
import java.util.List;

public class AssetWatcher extends Watcher {

    private AssetDao dao;
    private ConnectorDao connectorDao;
    private AssetHandler handler;

    @Inject
    public AssetWatcher(AssetDao dao, ConnectorDao connectorDao,  MutationDao mutationDao){
        this.dao = dao;
        this.handler = new AssetHandler(
                new ArrayList<Matcher>(){{
//                    add(new AssetCreatedMatcher());
//                    add(new AssetUpdatedMatcher());
                    add(new GroupMatcher());
                }},
                mutationDao
        );
        this.connectorDao = connectorDao;
    }

    @Override
    public void onPoll(Connector connector){
        List<Asset> assets = dao.getAll(connector.getStart(),connector.getEnd());
        boolean handlerCompleted = false;

        if(assets != null && assets.size() > 0) {
            handlerCompleted = handler.match(assets, connector);
            connector.setLastJobTimestamp(assets.get(assets.size() - 1).getUpdatedAt());
        }
    };
}
