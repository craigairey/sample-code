package com.insiris.carshopconnector.connector.database.mssql;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MsSqlDatabaseConnector implements DatabaseConnector {
    private final String jdbcDriver;
    private final String dbUrl;
    private final String dbUser;
    private final String dbPass;
    private final String dbName;
    private final Connection connection;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    public MsSqlDatabaseConnector(
               @Named("jdbc_mssql_driver") String jdbcDriver,
               @Named("db_url_birst") String dbUrl,
               @Named("db_user_birst") String dbUser,
               @Named("db_password_birst") String dbPassword,
               @Named("db_name_birst") String dbName){
        this.jdbcDriver = jdbcDriver;
        this.dbUrl = dbUrl;
        this.dbUser = dbUser;
        this.dbPass = dbPassword;
        this.dbName = dbName;
        this.connection = getConnection();
    }

    public Connection getConnection(){
        if(connection != null){
            return connection;
        }else{
            try{
                Class.forName(this.jdbcDriver);
                return DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPass);
            } catch (ClassNotFoundException | SQLException e) {
                logger.error("getting connection to db has thrown: {}", new Object[] {e.getMessage(), e});
            }
            return null;
        }

    }
}
