package com.insiris.carshopconnector.connector.model.unity.models.asset.storage.mysql;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.unity.models.asset.Asset;
import com.insiris.carshopconnector.connector.model.unity.models.asset.storage.AssetDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlAssetDao implements AssetDao {


    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlAssetDao(@Named("mysql")DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }

    @Override
    public List<Asset> getAll(Timestamp start, Timestamp finish) {
        logger.info("getting all Assets start {} finish {}", start, finish);
        PreparedStatement statement = null;
        List<Asset> results = new ArrayList<>();
        try {
            statement = connection.prepareStatement(AssetSql.instance.getGetAll());
            statement.setTimestamp(1, start);
            statement.setTimestamp(2, finish);
            statement.setTimestamp(3, start);
            statement.setTimestamp(4, finish);
            logger.info("Asset get all {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = AssetMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all Asset start {} finish {} has thrown: {}", start, finish, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }
}
