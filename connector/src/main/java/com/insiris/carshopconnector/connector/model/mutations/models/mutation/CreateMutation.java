package com.insiris.carshopconnector.connector.model.mutations.models.mutation;


public class CreateMutation extends Mutation {
    public CreateMutation() {
        super();
    }
    public CreateMutation(String statement, String connectorUuid) {
        super(statement, "create", connectorUuid);
    }
}
