package com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage.mysql;


import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.KassetField;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class KassetFieldMapper {
    public static List<KassetField> mapResults(ResultSet resultSet) throws SQLException {
        List<KassetField> kassetFields = new ArrayList<>();
        while(resultSet.next()) {
            KassetField kassetField = new KassetField();

            kassetField.setId(resultSet.getInt(KassetFieldSql.instance.getAliasId()));
            kassetField.setValue(resultSet.getString(KassetFieldSql.instance.getAliasValue()));
            kassetField.setName(resultSet.getString(KassetFieldSql.instance.getAliasName()));
            kassetField.setCreatedAt(resultSet.getTimestamp(KassetFieldSql.instance.getAliasCreatedAt()));
            kassetFields.add(kassetField);
        }
        return kassetFields;
    }
}
