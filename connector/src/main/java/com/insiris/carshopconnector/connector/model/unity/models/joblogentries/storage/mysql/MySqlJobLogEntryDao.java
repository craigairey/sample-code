package com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.mysql;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.JobLogEntryDao;
import com.insiris.carshopconnector.connector.shared.Converters;
import com.insiris.carshopconnector.connector.shared.enums.JobStatuses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlJobLogEntryDao implements JobLogEntryDao {


    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlJobLogEntryDao(@Named("mysql")DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }

    @Override
    public List<JobLogEntry> getAll(Timestamp start, Timestamp finish) {
        logger.info("getting all JobLogEntry start {} finish {}", start, finish);
        PreparedStatement statement = null;
        List<JobLogEntry> results = new ArrayList<>();
        try {
            statement = connection.prepareStatement(JobLogEntrySql.instance.getGetAll());
            statement.setTimestamp(1, start);
            statement.setTimestamp(2, finish);
            logger.info("JobLogEntry get all {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = JobLogEntryMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all JobLogEntry start {} finish {} has thrown: {}", start, finish, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }

    @Override
    public Double getDurationForJob(int jobId) {
        String sql = String.format(
                JobLogEntrySql.instance.getGetDurationForJob(),
                /*jobId > 0 ? "" :*/ "and jobs.id = "+jobId+" ",
                String.format("'%s','%s','%s','%s','%s'",
                        JobStatuses.ACTION_IN_PROGRESS,
                        JobStatuses.ACTION_COMPLETED,
                        JobStatuses.ACTION_FAILED,
                        JobStatuses.ACTION_REJECTED,
                        JobStatuses.ACTION_PAUSED));

        logger.info("get duration for job: " + sql);
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        List<JobLogEntry> queryResults = new ArrayList<>();
        Double toReturn = 0d;

        try {
            preparedStatement = connection.prepareStatement(sql);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                JobLogEntry jobLogEntry = new JobLogEntry();
                jobLogEntry.setId( resultSet.getInt("jle"));
                jobLogEntry.setKassetId(resultSet.getInt("id"));
                jobLogEntry.setAction(resultSet.getString("action"));
                jobLogEntry.setGeneratedAt(resultSet.getTimestamp("date"));
                jobLogEntry.setHours(resultSet.getInt("hours"));
                jobLogEntry.setMinutes(resultSet.getInt("mins"));
                queryResults.add(jobLogEntry);
            }

            if(!queryResults.isEmpty()){
                Long currentJobTime = 0L;
                Double totalTime = 0D;
                Long start = null;
                Long finish = null;

                JobLogEntry previous = null;

                for(JobLogEntry queryRow:queryResults){
                    if(JobStatuses.ACTION_ALLOCATED.equals(queryRow.getAction())){
                        start = null;
                        finish = null;
                        currentJobTime = 0L;
                    }
                    if(start == null && JobStatuses.ACTION_IN_PROGRESS.equals(queryRow.getAction())) {
                        start = queryRow.getGeneratedAt().getTime();
                        finish = null;
                        currentJobTime = 0L;
                    } else {
                        finish = queryRow.getGeneratedAt().getTime();
                    }

                    if (start != null && finish != null) {

                        Integer timeTemp = Converters.convertMilliSecondsToMinutes(finish - start);

                        currentJobTime += timeTemp;
                        totalTime += timeTemp;

                        logger.debug("***\t Found duration [" + currentJobTime +"] - total is [" +totalTime +"] at " + queryRow.getId() + " from " + start + " to " + finish);

                        if(JobStatuses.ACTION_FAILED.equals(queryRow.getAction()) ||JobStatuses.ACTION_REJECTED.equals(queryRow.getAction()) || JobStatuses.ACTION_COMPLETED.equals(queryRow.getAction()))
                            start = null;
                        else
                            start = finish.longValue();

                        finish = null;
                    }

                    previous = queryRow;
                }
                toReturn = totalTime / 60d;
            }

        } catch (SQLException e) {
            logger.error("get duration for job has thrown: " + e);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (Exception e) {
                    logger.error("Closing the prepared statement has thrown: " +e );
                }
            }
        }
        return toReturn;
    }


}
