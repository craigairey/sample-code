package com.insiris.carshopconnector.connector.model.mutations.models.mutation;

import java.util.UUID;

public class Mutation {
    private int id;
    private String statement;
    private String type;
    private boolean completed;
    private String connectorUuid;
    private String uuid;
    private boolean idleFlag;


    public Mutation(){

    }

    public Mutation(String statement, String type, String connectorUuid) {
        this.type = type;
        this.completed = false;
        this.connectorUuid = connectorUuid;
        this.uuid = UUID.randomUUID().toString();
        this.statement = String.format(statement, connectorUuid, uuid);;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatement() {
        return statement;
    }

    public void setStatement(String statement) {
        this.statement = statement;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getConnectorUuid() {
        return connectorUuid;
    }

    public void setConnectorUuid(String connectorUuid) {
        this.connectorUuid = connectorUuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public boolean isIdleFlag() {
        return idleFlag;
    }

    public void setIdleFlag(boolean idleFlag) {
        this.idleFlag = idleFlag;
    }
}
