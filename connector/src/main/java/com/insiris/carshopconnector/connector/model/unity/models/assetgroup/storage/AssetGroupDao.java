package com.insiris.carshopconnector.connector.model.unity.models.assetgroup.storage;

import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.AssetGroup;

import java.sql.Timestamp;
import java.util.List;

public interface AssetGroupDao {
    List<AssetGroup> getAll(Timestamp start, Timestamp finish);
}
