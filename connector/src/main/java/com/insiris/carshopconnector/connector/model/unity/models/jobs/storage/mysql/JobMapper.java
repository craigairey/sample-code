package com.insiris.carshopconnector.connector.model.unity.models.jobs.storage.mysql;


import com.insiris.carshopconnector.connector.model.unity.models.jobs.Job;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JobMapper {
    public static List<Job> mapResults(ResultSet resultSet) throws SQLException {
        List<Job> jobs = new ArrayList<>();
        while(resultSet.next()) {
            Job job = new Job();

            job.setId(resultSet.getInt(JobSql.instance.getAliasId()));
            job.setGroup(resultSet.getString((JobSql.instance.getAliasGroup())));
            job.setVehicleId(resultSet.getInt(JobSql.instance.getAliasVehicleId()));
            job.setDescription(resultSet.getString(JobSql.instance.getAliasDescription()));
            job.setCreatedAt(resultSet.getTimestamp(JobSql.instance.getAliasCreatedAt()));
            job.setWorkFlowName(resultSet.getString(JobSql.instance.getAliasWorkflowName()));
            job.setNotes(resultSet.getString(JobSql.instance.getAliasNotes()));
            int hours = resultSet.getInt(JobSql.instance.getAliasHour());
            int minutes = resultSet.getInt(JobSql.instance.getAliasMinute());

            double estimateHours = hours + ((double) minutes / 60d);
            job.setTotalEstimatedHours(estimateHours);
            jobs.add(job);
        }
        return jobs;
    }
}
