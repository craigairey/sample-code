package com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage;

import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;

import java.sql.Timestamp;
import java.util.List;

public interface JobLogEntryDao {
    List<JobLogEntry> getAll(Timestamp start, Timestamp finish);
    Double getDurationForJob(int jobId);
}
