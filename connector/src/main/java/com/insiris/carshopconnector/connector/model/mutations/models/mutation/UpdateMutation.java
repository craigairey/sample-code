package com.insiris.carshopconnector.connector.model.mutations.models.mutation;

public class UpdateMutation extends Mutation {
    public UpdateMutation() {}
    public UpdateMutation(String statement, String connectorUuid) {
        super(statement, "update", connectorUuid);
    }
}
