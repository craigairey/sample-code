package com.insiris.carshopconnector.connector.model.watchers;


import com.google.inject.Inject;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.handlers.KassetStatusHandler;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.matchers.event.*;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.KassetStatus;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.KassetStatusDao;

import java.util.ArrayList;
import java.util.List;

public class KassetStatusWatcher extends Watcher {

    private KassetStatusDao dao;

    protected KassetStatusDao getDao() {
        return this.dao;
    }

    protected void setDao(KassetStatusDao dao) {
        this.dao = dao;
    }

    private ConnectorDao connectorDao;
    private KassetStatusHandler handler;

    @Inject
    public KassetStatusWatcher(KassetStatusDao dao, ConnectorDao connectorDao,  MutationDao mutationDao){
        this.dao = dao;
        this.handler = new KassetStatusHandler(
                new ArrayList<Matcher>(){{
                    add(new OnHoldMatcher());
                    add(new DispatchedMatcher());
                    add(new InDecisionMatcher());
                    add(new PartsMatcher());
                    add(new QcMatcher());
                    add(new ReadyForProductionMatcher());
                    add(new RejectedMatcher());
                }},
                mutationDao
        );
        this.connectorDao = connectorDao;
    }

    @Override
    public void onPoll(Connector connector){
        List<KassetStatus> kassetStatues = dao.getAll(connector.getStart(),connector.getEnd());
        boolean handlerCompleted = false;

        if(kassetStatues != null && kassetStatues.size() > 0) {
            handlerCompleted = handler.match(kassetStatues, connector);
            connector.setLastKassetStatusTimestamp(kassetStatues.get(kassetStatues.size() - 1).getUpdatedAt());
        }
    };

}
