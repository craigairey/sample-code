package com.insiris.carshopconnector.connector.model.matchers.vehicle;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.CreateMutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.Kasset;
import com.insiris.carshopconnector.connector.shared.Converters;

public class VehicleCreatedMatcher extends Matcher {
    @Override
    public Mutation match(Object toMatch, Connector connector) {
        return vehicleCreatedSQL((Kasset)toMatch, connector);
    }

    private Mutation vehicleCreatedSQL(Kasset kasset, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "INSERT INTO "+connector.getDbName()+".dbo.vehicles (id, vehicle_id, partner_code, current_workflow_status, created_at, updated_at, deleted_at, connector_log_run_uuid, mutation_uuid) VALUES ("+kasset.getId()+", null, null, null, \\'"+ now+"\\', \\'"+ now+"\\', null, \\'%1$s\\', \\'%2$s\\')";
        return new CreateMutation(sql, connector.getUuid());
    }

}
