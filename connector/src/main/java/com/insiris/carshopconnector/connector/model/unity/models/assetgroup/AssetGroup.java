package com.insiris.carshopconnector.connector.model.unity.models.assetgroup;

import java.sql.Timestamp;

public class AssetGroup {
    private int id;
    private String name;
    private Timestamp createdAt;

    public AssetGroup(int id, String name, Timestamp createdAt) {
        this.id = id;
        this.name = name;
        this.createdAt = createdAt;
    }

    public AssetGroup() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Timestamp createdAt) {
        this.createdAt = createdAt;
    }
}
