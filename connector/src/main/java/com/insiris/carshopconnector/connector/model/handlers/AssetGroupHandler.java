package com.insiris.carshopconnector.connector.model.handlers;

import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;

import java.util.List;

public class AssetGroupHandler extends Handler {
    public AssetGroupHandler(List<Matcher> matchers, MutationDao mutationDao) {
        super(matchers, mutationDao);
    }
}
