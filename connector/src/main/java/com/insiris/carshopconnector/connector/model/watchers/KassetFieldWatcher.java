package com.insiris.carshopconnector.connector.model.watchers;


import com.google.inject.Inject;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.handlers.KassetFieldHandler;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.matchers.vehicle.AqIdChangeMatcher;
import com.insiris.carshopconnector.connector.model.matchers.vehicle.PartnerCodeMatcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.KassetField;
import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage.KassetFieldDao;


import java.util.ArrayList;
import java.util.List;

public class KassetFieldWatcher extends Watcher {


    private KassetFieldDao dao;
    private ConnectorDao connectorDao;
    private KassetFieldHandler handler;

    @Inject
    public KassetFieldWatcher(KassetFieldDao dao, ConnectorDao connectorDao,  MutationDao mutationDao){
        this.dao = dao;
        this.handler = new KassetFieldHandler(
                new ArrayList<Matcher>(){{
                    add(new AqIdChangeMatcher());
                    add(new PartnerCodeMatcher());
                }},
                mutationDao
        );
        this.connectorDao = connectorDao;
    }

    @Override
    public void onPoll(Connector connector){
        List<KassetField> kassetFields = dao.getAll(connector.getStart(),connector.getEnd());
        boolean handlerCompleted = false;

        if(kassetFields != null && kassetFields.size() > 0) {
            handlerCompleted = handler.match(kassetFields, connector);
            connector.setLastKassetFieldTimestamp(kassetFields.get(kassetFields.size() - 1).getCreatedAt());
        }
    };

}
