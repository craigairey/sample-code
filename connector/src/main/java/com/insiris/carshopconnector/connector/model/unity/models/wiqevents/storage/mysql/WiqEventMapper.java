package com.insiris.carshopconnector.connector.model.unity.models.wiqevents.storage.mysql;

import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.WiqEvent;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class WiqEventMapper {
    public static List<WiqEvent> mapResults(ResultSet resultSet) throws SQLException {
        List<WiqEvent> wiqEvents = new ArrayList<>();
        while(resultSet.next()) {
            WiqEvent wiqEvent = new WiqEvent();

            wiqEvent.setAssetId(resultSet.getInt(WiqEventSql.instance.getAliasAssetId()));
            wiqEvent.setGeneratedAt(resultSet.getTimestamp(WiqEventSql.instance.getAliasGeneratedAt()));
            wiqEvent.setType(resultSet.getString(WiqEventSql.instance.getAliasType()));
            wiqEvents.add(wiqEvent);
        }
        return wiqEvents;
    }
}
