package com.insiris.carshopconnector.connector.service;


import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.model.birst.BirstWriter;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.watchers.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class CarshopConnector implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private List<Watcher> watchers = new ArrayList<>();
    private ConnectorDao connectorDao;
    private MutationDao mutationDao;
    private BirstWriter birstWriter;
    private static String birstName;

    public CarshopConnector(Injector injector){
        watchers.add(injector.getInstance(KassetWatcher.class));
        watchers.add(injector.getInstance(KassetStatusWatcher.class));
        watchers.add(injector.getInstance(KassetFieldWatcher.class));
        watchers.add(injector.getInstance(JobLogEntryWatcher.class));
        watchers.add(injector.getInstance(JobWatcher.class));
        watchers.add(injector.getInstance(AssetWatcher.class));
        watchers.add(injector.getInstance(AssetGroupWatcher.class));
        watchers.add(injector.getInstance(WiqEventWatcher.class));
        connectorDao = injector.getInstance(ConnectorDao.class);
        mutationDao = injector.getInstance(MutationDao.class);
        birstWriter = injector.getInstance(BirstWriter.class);
        birstName = injector.getInstance(Key.get(String.class, Names.named("db_name_birst")));
    }

    @Override
    public void run() {
        Connector lastConnector = connectorDao.get();
        Connector connector = lastConnector != null ? new Connector(lastConnector) : new Connector();

        connector.setDbName(birstName);
        watchers.forEach(watcher -> watcher.onPoll(connector));

        connectorDao.save(connector);

        List<Mutation> createMutations = mutationDao.getAllCreateMutations();
        while (createMutations != null && createMutations.size() > 0) {
            birstWriter.insertRows(createMutations);
            createMutations = mutationDao.getAllCreateMutations();
        }
        List<Mutation> updateMutations = mutationDao.getAllUpdateMutations();
        while (updateMutations != null && updateMutations.size() > 0) {
            birstWriter.insertRows(updateMutations);
            updateMutations = mutationDao.getAllUpdateMutations();
        }
    }
}
