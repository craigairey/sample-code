package com.insiris.carshopconnector.connector.model.unity.models.assetgroup.storage.mysql;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.AssetGroup;
import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.storage.AssetGroupDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlAssetGroupDao implements AssetGroupDao {
    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlAssetGroupDao(@Named("mysql")DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }

    @Override
    public List<AssetGroup> getAll(Timestamp start, Timestamp finish) {
        logger.info("getting all asset groups start {} finish {}", start, finish);
        PreparedStatement statement = null;
        List<AssetGroup> results = new ArrayList<>();
        try {
            statement = connection.prepareStatement(AssetGroupSql.instance.getGetAll());
            statement.setTimestamp(1, start);
            statement.setTimestamp(2, finish);
            logger.info("asset groups get all {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = AssetGroupMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all asset groups start {} finish {} has thrown: {}", start, finish, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }
}
