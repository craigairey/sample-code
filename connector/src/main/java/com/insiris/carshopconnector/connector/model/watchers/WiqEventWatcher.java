package com.insiris.carshopconnector.connector.model.watchers;


import com.google.inject.Inject;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.model.handlers.WiqEventHandler;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.matchers.person.CheckInOutEventMatcher;
import com.insiris.carshopconnector.connector.model.matchers.person.CheckInOutMatcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.WiqEvent;
import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.storage.WiqEventDao;

import java.util.ArrayList;
import java.util.List;

public class WiqEventWatcher extends Watcher {
    private WiqEventDao dao;
    private ConnectorDao connectorDao;
    private WiqEventHandler handler;

    @Inject
    public WiqEventWatcher(WiqEventDao dao, ConnectorDao connectorDao,  MutationDao mutationDao){
        this.dao = dao;
        this.handler = new WiqEventHandler(
                new ArrayList<Matcher>(){{
                    add(new CheckInOutMatcher());
                    add(new CheckInOutEventMatcher());
                }},
                mutationDao
        );
        this.connectorDao = connectorDao;
    }

    @Override
    public void onPoll(Connector connector){
        List<WiqEvent> wiqEvents = dao.getAll(connector.getStart(),connector.getEnd());
        boolean handlerCompleted = false;

        if(wiqEvents != null && wiqEvents.size() > 0) {
            handlerCompleted = handler.match(wiqEvents, connector);
            connector.setLastKassetTimestamp(wiqEvents.get(wiqEvents.size() - 1).getGeneratedAt());
        }
    };
}
