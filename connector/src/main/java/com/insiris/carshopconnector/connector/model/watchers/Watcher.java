package com.insiris.carshopconnector.connector.model.watchers;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.handlers.Handler;


public abstract class Watcher {

    private Handler handler;

    private Handler getHandler() {
        return this.handler;
    }

    private void setHandler(Handler handler) {
        this.handler = handler;
    }

    public void onPoll(Connector connector){};
}
