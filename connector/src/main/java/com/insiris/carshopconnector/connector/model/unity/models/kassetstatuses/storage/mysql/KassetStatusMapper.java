package com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.mysql;

import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.KassetStatus;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class KassetStatusMapper {
    public static List<KassetStatus> mapResults(ResultSet resultSet) throws SQLException {
        List<KassetStatus> kassetStatuses = new ArrayList<>();
        while(resultSet.next()) {
            KassetStatus kassetStatus = new KassetStatus();

            kassetStatus.setKassetId(resultSet.getInt(KassetStatusSql.instance.getAliasKassetId()));
            kassetStatus.setUpdatedAt(resultSet.getTimestamp(KassetStatusSql.instance.getAliasUpdatedAt()));
            kassetStatus.setKassetStatus(resultSet.getString(KassetStatusSql.instance.getAliasKassetStatus()));
            kassetStatus.setKassetStatusId(resultSet.getInt(KassetStatusSql.instance.getAliasKassetStatusId()));
            kassetStatus.setUserId(resultSet.getInt(KassetStatusSql.instance.getAliasUserId()));
            kassetStatuses.add(kassetStatus);
        }
        return kassetStatuses;
    }
}
