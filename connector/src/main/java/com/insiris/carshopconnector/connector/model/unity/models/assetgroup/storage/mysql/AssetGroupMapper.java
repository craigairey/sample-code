package com.insiris.carshopconnector.connector.model.unity.models.assetgroup.storage.mysql;

import com.insiris.carshopconnector.connector.model.unity.models.assetgroup.AssetGroup;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AssetGroupMapper {
    public static List<AssetGroup> mapResults(ResultSet resultSet) throws SQLException {
        List<AssetGroup> assetGroups = new ArrayList<>();
        while(resultSet.next()) {
            AssetGroup assetGroup = new AssetGroup();

            assetGroup.setId(resultSet.getInt(AssetGroupSql.instance.getAliasId()));
            assetGroup.setCreatedAt(resultSet.getTimestamp(AssetGroupSql.instance.getAliasCreatedAt()));
            assetGroup.setName(resultSet.getString(AssetGroupSql.instance.getAliasName()));
            assetGroups.add(assetGroup);
        }
        return assetGroups;
    }
}
