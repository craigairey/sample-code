package com.insiris.carshopconnector.connector.model.matchers.person;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;
import com.insiris.carshopconnector.connector.shared.Converters;

import java.util.HashMap;
import java.util.Map;

public class InProgressCompletedMatcher extends Matcher {
    private Map<String, String> action = new HashMap<String, String>(){{
        put("in_progress", "on_job");
        put("completed", "job_finished");
        put("paused", "job_paused");
    }};

    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(action.containsKey(((JobLogEntry)toMatch).getAction()))
            return checkInOutSQL((JobLogEntry)toMatch, connector);
        else
            return null;
    }

    private Mutation checkInOutSQL(JobLogEntry jobLogEntry, Connector connector){
        String now = Converters.currentTimestampUTC();
        String sql = "UPDATE "+connector.getDbName()+".dbo.people SET current_attendence_status =\\'"+action.get(jobLogEntry.getAction())+"\\', updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+jobLogEntry.getPersonId();
        return new UpdateMutation(sql, connector.getUuid());
    }

}