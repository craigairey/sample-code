package com.insiris.carshopconnector.connector.model.matchers.person;


import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.UpdateMutation;
import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.WiqEvent;
import com.insiris.carshopconnector.connector.shared.Converters;

import java.util.ArrayList;
import java.util.List;

public class CheckInOutMatcher extends Matcher {
    private List<String> types = new ArrayList<String>(){{
        add("CheckIn");
        add("CheckOut");
    }};

    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(types.contains(((WiqEvent)toMatch).getType()))
            return checkInOutSQL((WiqEvent)toMatch, connector);
        else
            return null;
    }

    private Mutation checkInOutSQL(WiqEvent wiqEvent, Connector connector){
        String now = Converters.currentTimestampUTC();
        String columnName = "CheckIn".equals(wiqEvent.getType()) ? "latest_check_in": "latest_check_out";
        String status = "CheckIn".equals(wiqEvent.getType()) ? "checked_in": "checked_out";
        String sql = "UPDATE "+connector.getDbName()+".dbo.people SET "+ columnName + " = \\'"+Converters.timestampFormatter().format(wiqEvent.getGeneratedAt())+"\\', current_attendence_status =\\'"+status+"\\', updated_at = \\'"+ now+"\\', connector_log_run_uuid = \\'%1$s\\', mutation_uuid = \\'%2$s\\' WHERE id = "+wiqEvent.getAssetId();
        return new UpdateMutation(sql, connector.getUuid());
    }

}