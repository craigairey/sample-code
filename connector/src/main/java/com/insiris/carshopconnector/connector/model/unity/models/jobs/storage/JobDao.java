package com.insiris.carshopconnector.connector.model.unity.models.jobs.storage;

import com.insiris.carshopconnector.connector.model.unity.models.jobs.Job;

import java.sql.Timestamp;
import java.util.List;

public interface JobDao {
    List<Job> getAll(Timestamp start, Timestamp finish);
}
