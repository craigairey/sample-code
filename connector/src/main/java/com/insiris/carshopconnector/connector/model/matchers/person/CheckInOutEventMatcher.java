package com.insiris.carshopconnector.connector.model.matchers.person;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.matchers.Matcher;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.CreateMutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.unity.models.wiqevents.WiqEvent;
import com.insiris.carshopconnector.connector.shared.Converters;

import java.util.ArrayList;
import java.util.List;

public class CheckInOutEventMatcher extends Matcher{
    private List<String> types = new ArrayList<String>(){{
        add("CheckIn");
        add("CheckOut");
    }};

    @Override
    public Mutation match(Object toMatch, Connector connector) {
        if(types.contains(((WiqEvent)toMatch).getType()))
            return checkInOutEventSQL((WiqEvent)toMatch, connector);
        else
            return null;
    }

    private Mutation checkInOutEventSQL(WiqEvent wiqEvent, Connector connector){
        String now = Converters.currentTimestampUTC();
        String timestamp = "\\'"+Converters.timestampFormatter().format(wiqEvent.getGeneratedAt())+"\\'";
        String sql = "INSERT INTO "+connector.getDbName()+".dbo.events(event_category ,event_type,event_description ,timestamp , duration ,vehicle_id ,person_id ,created_at ,updated_at ,connector_log_run_uuid , mutation_uuid) VALUES (\\'person_attendance\\', \\'"+wiqEvent.getType()+"\\', null, "+timestamp+", null, null, "+wiqEvent.getAssetId()+", \\'"+ now+"\\', \\'"+ now+"\\', \\'%1$s\\', \\'%2$s\\')";
        return new CreateMutation(sql, connector.getUuid());
    }
}
