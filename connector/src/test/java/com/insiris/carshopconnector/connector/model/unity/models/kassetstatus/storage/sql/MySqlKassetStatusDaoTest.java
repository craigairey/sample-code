package com.insiris.carshopconnector.connector.model.unity.models.kassetstatus.storage.sql;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.KassetStatus;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.KassetStatusDao;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.mysql.MySqlKassetStatusDao;
import com.insiris.carshopconnector.connector.shared.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MySqlKassetStatusDaoTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_unity5")));
    private KassetStatusDao dao = injector.getInstance(MySqlKassetStatusDao.class);
    private static final String now = Converters.currentTimestampUTC();
    private static final ZonedDateTime nowZdt = ZonedDateTime.now(ZoneOffset.UTC);

    @BeforeClass
    public static void setup(){
        List<String> dbSchema = new ArrayList<String>(){{
            add(DbSchema.kassetKassetStatusDDL);
        }};
        dbHelperMysql.createDatabaseAndTables(dbName, dbSchema);
        dbHelperMysql.insertRows(dbName, DbInserts.kassetStatues(dbName, now)); //todo broken ... maybe
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }

    @Test
    public void getAllTest(){
        List<KassetStatus> kassetStatuses = dao.getAll(DateUtil.getYesterday(nowZdt), DateUtil.getTomorrow(nowZdt));
        assertEquals(1, kassetStatuses.size());
    }
}
