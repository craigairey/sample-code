package com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.mysql;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.CreateMutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.MutationDao;
import com.insiris.carshopconnector.connector.shared.DbHelperMysql;
import com.insiris.carshopconnector.connector.shared.DbInserts;
import com.insiris.carshopconnector.connector.shared.DbSchema;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.*;

public class MySqlMutationDaoTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_unity5")));
    private MutationDao dao = injector.getInstance(MySqlMutationDao.class);
    private static final ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);

    @BeforeClass
    public static void setup(){
        List<String> dbSchema = new ArrayList<String>(){{
            add(DbSchema.mutationsDDL);
        }};
        dbHelperMysql.createDatabaseAndTables(dbName, dbSchema);
        dbHelperMysql.insertRows(dbName, DbInserts.mutations(dbName));
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }

    @Test
    public void getAllCreateTest(){
        List<Mutation> mutations = dao.getAllCreateMutations();
        assertTrue(mutations.size() >= 2);
    }

    @Test
    public void getAllUpdateTest(){
        List<Mutation> mutations = dao.getAllUpdateMutations();
        assertEquals(1, mutations.size());
    }

    @Test
    public void markCompletedTest(){
        boolean errorThrown = false;

        try{
            List<Mutation> mutations = new ArrayList<Mutation>(){{
                Mutation mutation = new Mutation();
                mutation.setId(1);
                add(mutation);
            }};
            dao.markCompleted(mutations);
        } catch (Exception e){
            errorThrown = true;
        }
        assertFalse(errorThrown);
    }

    @Test
    public void saveAllTest(){
        CreateMutation createMutation = new CreateMutation("new sql", "c1");
        List<Mutation> mutations = new ArrayList<>();
        mutations.add(createMutation);
        boolean errorThrown = false;
        try {
            dao.saveAll(mutations);
        } catch(Exception e){
            errorThrown = true;
        }
        assertFalse(errorThrown);
        mutations = dao.getAllCreateMutations();
        assertEquals(3, mutations.size());
    }
}
