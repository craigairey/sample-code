package com.insiris.carshopconnector.connector.model.unity.models.joblogentires.storage.mysql;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.JobLogEntryDao;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.mysql.MySqlJobLogEntryDao;
import com.insiris.carshopconnector.connector.shared.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MySqlJobLogEntriesDaoTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_unity5")));
    private JobLogEntryDao dao = injector.getInstance(MySqlJobLogEntryDao.class);
    private static final String now = Converters.currentTimestampUTC();
    private static final ZonedDateTime nowZdt = ZonedDateTime.now(ZoneOffset.UTC);

    @BeforeClass
    public static void setup(){
        List<String> dbSchema = new ArrayList<String>(){{
            add(DbSchema.jobLogEntryDDL);
            add(DbSchema.jobsDDL);
            add(DbSchema.workflowsDDL);
            add(DbSchema.kassetDDL);
        }};
        dbHelperMysql.createDatabaseAndTables(dbName, dbSchema);
        dbHelperMysql.insertRows(dbName, DbInserts.jobLogEntries(dbName, now));
        dbHelperMysql.insertRows(dbName, DbInserts.jobs(dbName));
        dbHelperMysql.insertRows(dbName, DbInserts.workflows(dbName));
        dbHelperMysql.insertRows(dbName, DbInserts.kassets(dbName, now));
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }

    @Test
    public void getAllTest(){
        List<JobLogEntry> jobLogEntries = dao.getAll(DateUtil.getYesterday(nowZdt), DateUtil.getTomorrow(nowZdt));
        assertEquals(3, jobLogEntries.size());
    }
}
