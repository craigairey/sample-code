package com.insiris.carshopconnector.connector;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.shared.DbHelperMysql;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DbTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = "d" + new Date().toInstant().getEpochSecond();

    @BeforeClass
    public static void setup(){
        String genMyModelDump = String.format("CREATE TABLE IF NOT EXISTS %1$s.vehicles\n(\n    id INT NOT NULL,\n    vehicle_id INT,\n    partner_code VARCHAR(255),\n    current_workflow_status INT,\n    created_at DATETIME,\n    updated_at DATETIME,\n    deleted_at DATETIME,\n    connector_log_run_uuid VARCHAR(255),\n    mutation_uuid VARCHAR(255),\n    PRIMARY KEY(id)\n);\n\nCREATE TABLE IF NOT EXISTS %1$s.people\n(\n    id INT NOT NULL,\n    name VARCHAR(255),\n    department VARCHAR(255),\n    area VARCHAR(255),\n    role VARCHAR(255),\n    shift VARCHAR(255),\n    employee_status VARCHAR(255),\n    agency VARCHAR(255),\n    latest_check_in DATETIME,\n    latest_check_out DATETIME,\n    current_attendance_status VARCHAR(255),\n    created_at DATETIME,\n    updated_at DATETIME,\n    deleted_at DATETIME,\n    connector_log_run_uuid VARCHAR(255),\n    mutation_uuid VARCHAR(255),\n    PRIMARY KEY(id)\n);\n\nCREATE TABLE IF NOT EXISTS %1$s.jobs\n(\n    id INT NOT NULL,\n    vehicle_id INT,\n    role VARCHAR(255),\n    total_estimated_hours DECIMAL(65, 0),\n    estimated_hours_completed DECIMAL(65, 0),\n    actual_hours_completed DECIMAL(65, 0),\n    estimate_value DECIMAL(65, 0),\n    description VARCHAR(1000),\n    created_at DATETIME,\n    updated_at DATETIME,\n    deleted_at DATETIME,\n    connector_log_run_uuid VARCHAR(255),\n    mutation_uuid VARCHAR(255),\n    PRIMARY KEY(id),\n    FOREIGN KEY (vehicle_id) REFERENCES %1$s.vehicles(id)\n);\n\nCREATE TABLE IF NOT EXISTS %1$s.events\n(\n    id INT NOT NULL,\n    event_category VARCHAR(255),\n    event_type VARCHAR(255),\n    event_description VARCHAR(1000),\n    start DATETIME,\n    end DATETIME,\n    duration_seconds INT,\n    vehicle_id INT,\n    person_id INT,\n    job_id INT,\n    created_at DATETIME,\n    updated_at DATETIME,\n    deleted_at DATETIME,\n    connector_log_run_uuid VARCHAR(255),\n    mutation_uuid VARCHAR(255),\n    PRIMARY KEY(id),\n    FOREIGN KEY (vehicle_id) REFERENCES %1$s.vehicles(id),\n    FOREIGN KEY (person_id) REFERENCES %1$s.people(id),\n    FOREIGN KEY (job_id) REFERENCES %1$s.jobs(id)\n);", dbName);
        List<String> dbSchema = new ArrayList<>(Arrays.asList(genMyModelDump.split("[;]")));
        dbHelperMysql.createDatabaseAndTables(dbName, dbSchema);
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }

    @Test
    public void justAQuickie(){
        System.out.println("might do something at some point");
    }
}