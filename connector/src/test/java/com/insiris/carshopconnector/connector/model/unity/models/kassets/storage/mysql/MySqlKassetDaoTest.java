package com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.mysql;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.Kasset;
import com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.KassetDao;
import com.insiris.carshopconnector.connector.shared.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MySqlKassetDaoTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_unity5")));
    private KassetDao dao = injector.getInstance(MySqlKassetDao.class);
    private static final String now = Converters.currentTimestampUTC();
    private static final ZonedDateTime nowZdt = ZonedDateTime.now(ZoneOffset.UTC);

    @BeforeClass
    public static void setup(){
        List<String> dbSchema = new ArrayList<String>(){{
            add(DbSchema.kassetDDL);
            add(DbSchema.kassetKassetStatusDDL);
            add(DbSchema.kassetFieldsDDL);
            add(DbSchema.kassetFieldTemplatesDDL);
            add(DbSchema.workflowsDDL);
            add(DbSchema.jobsDDL);
            add(DbSchema.jobLogEntryDDL);
            add(DbSchema.connectorsDDL);
            add(DbSchema.mutationsDDL);
        }};
        dbHelperMysql.createDatabaseAndTables(dbName, dbSchema);

        dbHelperMysql.insertRows(dbName, DbInserts.kassets(dbName, now));
        dbHelperMysql.insertRows(dbName, DbInserts.kassetFields(dbName, now));
        dbHelperMysql.insertRows(dbName, DbInserts.kassetFieldTemplates(dbName));
        dbHelperMysql.insertRows(dbName, DbInserts.kassetStatues(dbName, now));
        dbHelperMysql.insertRows(dbName, DbInserts.jobLogEntries(dbName, now));
        dbHelperMysql.insertRows(dbName, DbInserts.jobs(dbName));
        dbHelperMysql.insertRows(dbName, DbInserts.workflows(dbName));
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }

    @Test
    public void getAllTest(){
        List<Kasset> kassets = dao.getAll(DateUtil.getYesterday(nowZdt), DateUtil.getTomorrow(nowZdt));
        assertEquals(2, kassets.size());
    }
}
