package com.insiris.carshopconnector.connector.shared;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class DateUtil {
    public static Timestamp getYesterday(ZonedDateTime now){
        LocalDateTime localDateTime = now.toInstant().atZone(ZoneOffset.UTC).toLocalDateTime();
        return Timestamp.from(localDateTime.minusDays(1l).atZone(ZoneOffset.UTC).toInstant());
    }
    public static ZonedDateTime getYesterdayZdt(ZonedDateTime now){
        return now.minusDays(1l);
    }
    public static Timestamp getTomorrow(ZonedDateTime now){
        LocalDateTime localDateTime = now.toInstant().atZone(ZoneOffset.UTC).toLocalDateTime();
        return Timestamp.from(localDateTime.plusDays(1l).atZone(ZoneOffset.UTC).toInstant());
    }
}
