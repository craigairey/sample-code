package com.insiris.carshopconnector.connector.database.mysql;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.database.DatabaseConnector;
import com.insiris.carshopconnector.connector.database.mssql.MsSqlDatabaseConnector;
import com.insiris.carshopconnector.connector.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.shared.DbHelperMysql;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import static org.junit.Assert.assertFalse;

public class MySqlDatabaseConnectorTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_unity5")));

    private static final Calendar UTCCALENDAR = Calendar.getInstance (TimeZone.getTimeZone (ZoneOffset.UTC));


    @BeforeClass
    public static void setup()
    {
        dbHelperMysql.createDatabaseAndTables(dbName, new ArrayList<>());
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }

    @Test
    public void getConnectionToTestDb(){
        DatabaseConnector databaseConnector = injector.getInstance(MySqlDatabaseConnector.class);

        Connection connection = databaseConnector.getConnection();
        boolean errored = false;
        try{
            System.out.println(connection.getSchema());
            System.out.println(connection.getClientInfo());
        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);
    }
}
