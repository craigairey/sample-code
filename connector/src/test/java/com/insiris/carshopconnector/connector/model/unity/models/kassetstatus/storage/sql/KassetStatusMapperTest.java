package com.insiris.carshopconnector.connector.model.unity.models.kassetstatus.storage.sql;

import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.KassetStatus;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.mysql.KassetStatusMapper;
import com.insiris.carshopconnector.connector.model.unity.models.kassetstatuses.storage.mysql.KassetStatusSql;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class KassetStatusMapperTest {
    @Test
    public void mappingTest(){
        ResultSet resultSet = mock(ResultSet.class);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Date datetime = new Date();
        boolean errored = false;

        try {
            when(resultSet.getInt(KassetStatusSql.instance.getAliasKassetId())).thenReturn(1);
            when(resultSet.getInt(KassetStatusSql.instance.getAliasKassetStatusId())).thenReturn(2);
            when(resultSet.getTimestamp(KassetStatusSql.instance.getAliasUpdatedAt())).thenReturn(timestamp);
            when(resultSet.getInt(KassetStatusSql.instance.getAliasUserId())).thenReturn(3);
            when(resultSet.next()).thenReturn(true).thenReturn(false);
        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);

        List<KassetStatus> kassetStatuses = null;

        try {
            kassetStatuses = KassetStatusMapper.mapResults(resultSet);
        } catch (SQLException e) {
            errored = true;
        }

        assertFalse(errored);
        assertEquals(1, kassetStatuses.size());
        assertEquals(1, kassetStatuses.get(0).getKassetId());
        assertEquals(2, kassetStatuses.get(0).getKassetStatusId());
        assertEquals(3, kassetStatuses.get(0).getUserId());
        assertEquals(timestamp.toInstant(), kassetStatuses.get(0).getUpdatedAt().toInstant());
    }
}











































