package com.insiris.carshopconnector.connector.model.unity.models.joblogentires.storage.mysql;

import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.JobLogEntry;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.mysql.JobLogEntryMapper;
import com.insiris.carshopconnector.connector.model.unity.models.joblogentries.storage.mysql.JobLogEntrySql;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class JobLogEntriesMapperTest {
    @Test
    public void mappingTest(){
        ResultSet resultSet = mock(ResultSet.class);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        boolean errored = false;

        try {
            when(resultSet.getInt(JobLogEntrySql.instance.getAliasId())).thenReturn(1);
            when(resultSet.getString(JobLogEntrySql.instance.getAliasAction())).thenReturn("action");
            when(resultSet.getString(JobLogEntrySql.instance.getAliasName())).thenReturn("name");
            when(resultSet.getTimestamp(JobLogEntrySql.instance.getAliasGen())).thenReturn(timestamp);
            when(resultSet.getInt(JobLogEntrySql.instance.getAliasPerson())).thenReturn(1);
            when(resultSet.next()).thenReturn(true).thenReturn(false);

        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);

        List<JobLogEntry> jobLogEntries = null;

        try {
            jobLogEntries = JobLogEntryMapper.mapResults(resultSet);
        } catch (SQLException e) {
            errored = true;
        }

        assertFalse(errored);
        assertEquals(1, jobLogEntries.size());
        assertEquals(1, jobLogEntries.get(0).getId());
        assertEquals("action", jobLogEntries.get(0).getAction());
        assertEquals("name", jobLogEntries.get(0).getName());
        assertEquals(1, jobLogEntries.get(0).getPersonId());

        assertEquals(timestamp.toInstant(), jobLogEntries.get(0).getGeneratedAt().toInstant());
    }
}











































