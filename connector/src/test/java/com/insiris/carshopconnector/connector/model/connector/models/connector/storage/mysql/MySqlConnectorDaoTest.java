package com.insiris.carshopconnector.connector.model.connector.models.connector.storage.mysql;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import com.insiris.carshopconnector.connector.model.connector.models.connector.storage.ConnectorDao;
import com.insiris.carshopconnector.connector.shared.Converters;
import com.insiris.carshopconnector.connector.shared.DbHelperMysql;
import com.insiris.carshopconnector.connector.shared.DbInserts;
import com.insiris.carshopconnector.connector.shared.DbSchema;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class MySqlConnectorDaoTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_unity5")));
    private ConnectorDao dao = injector.getInstance(MySqlConnectorDao.class);
    private static final Timestamp now = Timestamp.from(new Date().toInstant());

    @BeforeClass
    public static void setup(){
        List<String> dbSchema = new ArrayList<String>(){{
            add(DbSchema.connectorsDDL);
            add(DbSchema.kassetDDL);
            add(DbSchema.kassetKassetStatusDDL);
            add(DbSchema.mutationsDDL);
        }};
        dbHelperMysql.createDatabaseAndTables(dbName, dbSchema);
        dbHelperMysql.insertRows(dbName, DbInserts.connectors(dbName));
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }


    @Test
    public void getTest(){
        Connector connector = dao.get();
        assertTrue(connector.getLastKassetTimestamp().toInstant().getEpochSecond() > 1);
    }

    @Test
    public void saveAllTest(){
        Connector newConnector = new Connector();
        newConnector.setStart(now);
        newConnector.setEnd(now);
        Timestamp later = Timestamp.from(new Date().toInstant());
        newConnector.setLastKassetTimestamp(later);
        newConnector.setLastKassetFieldTimestamp(later);
        boolean errorThrown = false;
        try {
            dao.save(newConnector);
        } catch(Exception e){
            errorThrown = true;
        }
        assertFalse(errorThrown);
        newConnector = dao.get();
                String now = Converters.currentTimestampUTC(); ;

        assertEquals(Converters.timestampFormatter().format(later.toInstant()), Converters.timestampFormatter().format(newConnector.getLastKassetTimestamp().toInstant()));
        assertEquals(Converters.timestampFormatter().format(later.toInstant()), Converters.timestampFormatter().format(newConnector.getLastKassetFieldTimestamp().toInstant()));
    }
}
