package com.insiris.carshopconnector.connector.model.connector.models.connector.storage.mysql;

import com.insiris.carshopconnector.connector.model.connector.models.connector.Connector;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ConnectorMapperTest {
    @Test
    public void mappingTest(){
        ResultSet resultSet = mock(ResultSet.class);
        boolean errored = false;
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        try {
            when(resultSet.getInt(ConnectorSql.instance.getAliasId())).thenReturn(1);
            when(resultSet.getTimestamp(ConnectorSql.instance.getAliasStart())).thenReturn(timestamp);
            when(resultSet.getTimestamp(ConnectorSql.instance.getAliasEnd())).thenReturn(timestamp);
            when(resultSet.getTimestamp(ConnectorSql.instance.getAliasLastKassetTimestamp())).thenReturn(timestamp);
            when(resultSet.getTimestamp(ConnectorSql.instance.getAliasLastKassetStatusTimestamp())).thenReturn(timestamp);
            when(resultSet.next()).thenReturn(true).thenReturn(false);

        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);

        List<Connector> connectors = null;

        try {
            connectors = ConnectorMapper.mapResults(resultSet);
        } catch (SQLException e) {
            errored = true;
        }

        assertFalse(errored);
        assertEquals(1, connectors.size());
        assertEquals(1, connectors.get(0).getId());
        assertEquals(timestamp, connectors.get(0).getStart());
        assertEquals(timestamp, connectors.get(0).getEnd());
        assertEquals(timestamp, connectors.get(0).getLastKassetTimestamp());
        assertEquals(timestamp, connectors.get(0).getLastKassetStatusTimestamp());
    }
}
