package com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage.mysql;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.connector.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.connector.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.KassetField;
import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage.KassetFieldDao;
import com.insiris.carshopconnector.connector.shared.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MySqlKassetFieldsDaoTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_unity5")));
    private KassetFieldDao dao = injector.getInstance(MySqlKassetFieldDao.class);
    private static final String now = Converters.currentTimestampUTC();
    private static final ZonedDateTime nowZdt = ZonedDateTime.now(ZoneOffset.UTC);

    @BeforeClass
    public static void setup(){
        List<String> dbSchema = new ArrayList<String>(){{
            add(DbSchema.kassetDDL);
            add(DbSchema.kassetFieldsDDL);
            add(DbSchema.kassetFieldTemplatesDDL);
        }};
        dbHelperMysql.createDatabaseAndTables(dbName, dbSchema);
        dbHelperMysql.insertRows(dbName, DbInserts.kassets(dbName, now));
        dbHelperMysql.insertRows(dbName, DbInserts.kassetFields(dbName, now));
        dbHelperMysql.insertRows(dbName, DbInserts.kassetFieldTemplates(dbName));
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }

    @Test
    public void getAllTest(){
        List<KassetField> kassetFields = dao.getAll(DateUtil.getYesterday(nowZdt), DateUtil.getTomorrow(nowZdt));
        assertEquals(2, kassetFields.size());
    }
}
