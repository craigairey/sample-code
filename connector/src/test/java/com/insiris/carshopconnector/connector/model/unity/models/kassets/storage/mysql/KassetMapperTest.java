package com.insiris.carshopconnector.connector.model.unity.models.kassets.storage.mysql;

import com.insiris.carshopconnector.connector.model.unity.models.kassets.Kasset;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class KassetMapperTest {
    @Test
    public void mappingTest(){
        ResultSet resultSet = mock(ResultSet.class);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        Date datetime = new Date();
        boolean errored = false;

        try {
            when(resultSet.getInt(KassetSql.instance.getAliasId())).thenReturn(1);
            when(resultSet.getString(KassetSql.instance.getAliasName())).thenReturn("name");
            when(resultSet.getString(KassetSql.instance.getAliasAddress())).thenReturn("address");
            when(resultSet.getString(KassetSql.instance.getAliasDescription())).thenReturn("desc");
            when(resultSet.getString(KassetSql.instance.getAliasPostcode_zip())).thenReturn("postcode");
            when(resultSet.getDouble(KassetSql.instance.getAliasLatitude())).thenReturn(1.01);
            when(resultSet.getDouble(KassetSql.instance.getAliasDec())).thenReturn(1.02);
            when(resultSet.getDouble(KassetSql.instance.getAliasLongitude())).thenReturn(1.03);
            when(resultSet.getString(KassetSql.instance.getAliasKasset_type())).thenReturn("type");
            when(resultSet.getInt(KassetSql.instance.getAliasIs_stock())).thenReturn(2);
            when(resultSet.getDouble(KassetSql.instance.getAliasStock_count())).thenReturn(1.04);
            when(resultSet.getInt(KassetSql.instance.getAliasKasset_template_id())).thenReturn(3);
            when(resultSet.getInt(KassetSql.instance.getAliasKasset_group_id())).thenReturn(4);
            when(resultSet.getInt(KassetSql.instance.getAliasCustomer_id())).thenReturn(5);
            when(resultSet.getString(KassetSql.instance.getAliasNfc_tag_identifier())).thenReturn("tag");
            when(resultSet.getString(KassetSql.instance.getAliasIdentifier())).thenReturn("id");
            when(resultSet.getInt(KassetSql.instance.getAliasShow_on_mobile())).thenReturn(6);
            when(resultSet.getInt(KassetSql.instance.getAliasShow_on_mobile_updated())).thenReturn(7);
            when(resultSet.getTimestamp(KassetSql.instance.getAliasDeleted_at())).thenReturn(timestamp);
            when(resultSet.getString(KassetSql.instance.getAliasGuid())).thenReturn("guid");
            when(resultSet.getTimestamp(KassetSql.instance.getAliasCreated_at())).thenReturn(timestamp);
            when(resultSet.getTimestamp(KassetSql.instance.getAliasUpdated_at())).thenReturn(timestamp);
            when(resultSet.getInt(KassetSql.instance.getAliasRate_plan_id())).thenReturn(8);
            when(resultSet.getInt(KassetSql.instance.getAliasKasset_status_id())).thenReturn(9);
            when(resultSet.getInt(KassetSql.instance.getAliasSend_job_history_kasset_count())).thenReturn(10);
            when(resultSet.getInt(KassetSql.instance.getAliasDormant())).thenReturn(11);
            when(resultSet.getInt(KassetSql.instance.getAliasLocked_by_user_id())).thenReturn(12);
            when(resultSet.getTimestamp(KassetSql.instance.getAliasLocked_until())).thenReturn(timestamp);
            when(resultSet.next()).thenReturn(true).thenReturn(false);

        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);

        List<Kasset> kassets = null;

        try {
            kassets = KassetMapper.mapResults(resultSet);
        } catch (SQLException e) {
            errored = true;
        }

        assertFalse(errored);
        assertEquals(1, kassets.size());
        assertEquals(1, kassets.get(0).getId());
        assertEquals("name", kassets.get(0).getName());
        assertEquals("address", kassets.get(0).getAddress());
        assertEquals("desc", kassets.get(0).getDescription());
        assertEquals("postcode", kassets.get(0).getPostcode_zip());
        assertEquals(new Double(1.01), kassets.get(0).getLatitude());
        assertEquals(new Double(1.02), kassets.get(0).getDecimal());
        assertEquals(new Double(1.03), kassets.get(0).getLongitude());
        assertEquals("type", kassets.get(0).getKasset_type());
        assertEquals(2, kassets.get(0).getIs_stock());
        assertEquals(new Double(1.04), kassets.get(0).getStock_count());
        assertEquals(3, kassets.get(0).getKasset_template_id());
        assertEquals(4, kassets.get(0).getKasset_group_id());
        assertEquals(5, kassets.get(0).getCustomer_id());
        assertEquals("tag", kassets.get(0).getNfc_tag_identifier());
        assertEquals("id", kassets.get(0).getIdentifier());
        assertEquals(6, kassets.get(0).getShow_on_mobile());
        assertEquals(7, kassets.get(0).getShow_on_mobile_updated());
        assertEquals(timestamp.toInstant(), kassets.get(0).getDeleted_at().toInstant());
        assertEquals("guid", kassets.get(0).getGuid());
        assertEquals(timestamp.toInstant(), kassets.get(0).getCreated_at().toInstant());
        assertEquals(timestamp.toInstant(), kassets.get(0).getUpdated_at().toInstant());
        assertEquals(8, kassets.get(0).getRate_plan_id());
        assertEquals(9, kassets.get(0).getKasset_status_id());
        assertEquals(10, kassets.get(0).getSend_job_history_kasset_count());
        assertEquals(11, kassets.get(0).getDormant());
        assertEquals(12, kassets.get(0).getLocked_by_user_id());
        assertEquals(timestamp.toInstant(), kassets.get(0).getLocked_until().toInstant());
    }
}











































