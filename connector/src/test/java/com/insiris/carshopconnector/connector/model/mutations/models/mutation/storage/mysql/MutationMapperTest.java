package com.insiris.carshopconnector.connector.model.mutations.models.mutation.storage.mysql;

import com.insiris.carshopconnector.connector.model.mutations.models.mutation.Mutation;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MutationMapperTest {
    @Test
    public void mappingTest(){
        ResultSet resultSet = mock(ResultSet.class);
        boolean errored = false;

        try {
            when(resultSet.getInt(MutationSql.instance.getAliasUuid())).thenReturn(1);
            when(resultSet.getString(MutationSql.instance.getAliasStatement())).thenReturn("statement");
            when(resultSet.getString(MutationSql.instance.getAliasType())).thenReturn("create");
            when(resultSet.getInt(MutationSql.instance.getAliasCompleted())).thenReturn(1);
            when(resultSet.getString(MutationSql.instance.getAliasConnectorUuid())).thenReturn("c1");
            when(resultSet.next()).thenReturn(true).thenReturn(false);

        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);

        List<Mutation> mutations = null;

        try {
            mutations = MutationMapper.mapResults(resultSet);
        } catch (SQLException e) {
            errored = true;
        }

        assertFalse(errored);
        assertEquals(1, mutations.size());
        assertEquals(1, mutations.get(0).getId());
        assertEquals("statement", mutations.get(0).getStatement());
        assertEquals("create", mutations.get(0).getType());
        assertTrue(mutations.get(0).isCompleted());
        assertEquals("c1", mutations.get(0).getConnectorUuid());
    }
}











































