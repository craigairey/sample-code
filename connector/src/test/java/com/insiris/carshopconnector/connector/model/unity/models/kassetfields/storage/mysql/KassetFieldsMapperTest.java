package com.insiris.carshopconnector.connector.model.unity.models.kassetfields.storage.mysql;

import com.insiris.carshopconnector.connector.model.unity.models.kassetfields.KassetField;
import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class KassetFieldsMapperTest {
    @Test
    public void mappingTest(){
        ResultSet resultSet = mock(ResultSet.class);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        boolean errored = false;

        try {
            when(resultSet.getInt(KassetFieldSql.instance.getAliasId())).thenReturn(1);
            when(resultSet.getString(KassetFieldSql.instance.getAliasValue())).thenReturn("value");
            when(resultSet.getString(KassetFieldSql.instance.getAliasName())).thenReturn("name");
            when(resultSet.getTimestamp(KassetFieldSql.instance.getAliasCreatedAt())).thenReturn(timestamp);
            when(resultSet.next()).thenReturn(true).thenReturn(false);

        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);

        List<KassetField> kassetFields = null;

        try {
            kassetFields = KassetFieldMapper.mapResults(resultSet);
        } catch (SQLException e) {
            errored = true;
        }

        assertFalse(errored);
        assertEquals(1, kassetFields.size());
        assertEquals(1, kassetFields.get(0).getId());
        assertEquals("value", kassetFields.get(0).getValue());
        assertEquals("name", kassetFields.get(0).getName());

        assertEquals(timestamp.toInstant(), kassetFields.get(0).getCreatedAt().toInstant());
    }
}











































