package com.insiris.carshopconnector.idling.shared;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class DbHelperMysql {
    private final String jdbcDriver;
    private final String dbUrl;
    private final String dbUser;
    private final String dbPass;
    private final Connection connection;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    public DbHelperMysql(
            @Named("jdbc_mysql_driver") String jdbcDriver,
            @Named("db_url_unity5") String dbUrl,
            @Named("db_user_unity5") String dbUser,
            @Named("db_password_unity5") String dbPassword){
        this.jdbcDriver = jdbcDriver;
        this.dbUrl = dbUrl;
        this.dbUser = dbUser;
        this.dbPass = dbPassword;
        this.connection = getConnection();
    }

    private Connection getConnection(){
        try{
            Class.forName(this.jdbcDriver);
            return DriverManager.getConnection(this.dbUrl, this.dbUser, this.dbPass);
        } catch (ClassNotFoundException | SQLException e) {
            logger.error("getting connection to db has thrown: {}", new Object[] {e.getMessage(), e});
        }
        return null;
    }

    public void createDatabaseAndTables(String dbName, List<String> tableSchemas){
        Statement statement = null;
        try {
            statement = connection.createStatement();
            String sql = String.format("CREATE DATABASE %s ", dbName);
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            logger.error("creating db from {} has thrown: {}", dbName, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }

        createTables(dbName, tableSchemas);
    }

    public void insertRows(String dbName, List<String> insertStatements){
        Statement statement = null;
        try {
            connection.setCatalog(dbName);
            statement = connection.createStatement();
            for(String sql : insertStatements) {
                logger.info("inserting:" + sql);
                statement.executeUpdate(sql);
            }
        } catch (SQLException e) {
            logger.error("inserting into db has thrown: {}",  new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
    }

    public void dropDatabase(String dbName){
        Statement statement = null;
        try {
            statement = connection.createStatement();
            String sql = String.format("DROP DATABASE %s ", dbName);
            statement.executeUpdate(sql);
        } catch (SQLException e) {
            logger.error("dropping db: {} has thrown: {}", dbName, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
            try{
                if(connection!=null)
                    connection.close();
            }catch(SQLException se){
                logger.error("dropping db: {} has thrown: {}", dbName, new Object[] {se.getMessage(), se});

            }
        }
    }

    private void createTables(String dbName, List<String> tableSchemas) {
        tableSchemas.forEach( tableSchema ->{
            Statement statement = null;
            try {
                connection.setCatalog(dbName);
                statement = connection.createStatement();
                statement.executeUpdate(tableSchema);
            } catch (SQLException e) {
                logger.error("creating db from {} has thrown: {}", dbName, new Object[] {e.getMessage(), e});
            } finally {
                try{
                    if(statement!=null)
                        statement.close();
                }catch(SQLException se2){}
            }
        });
    }
}
