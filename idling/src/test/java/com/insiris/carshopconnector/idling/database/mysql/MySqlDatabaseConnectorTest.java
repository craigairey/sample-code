package com.insiris.carshopconnector.idling.database.mysql;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.idling.database.DatabaseConnector;
import com.insiris.carshopconnector.idling.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.idling.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.idling.shared.DbHelperMysql;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.assertFalse;

public class MySqlDatabaseConnectorTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMysql dbHelperMysql = injector.getInstance(DbHelperMysql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_unity5")));

    @BeforeClass
    public static void setup()
    {
        dbHelperMysql.createDatabaseAndTables(dbName, new ArrayList<>());
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMysql.dropDatabase(dbName);
    }

    @Test
    public void getConnectionToTestDb(){
        DatabaseConnector databaseConnector = injector.getInstance(MySqlDatabaseConnector.class);

        Connection connection = databaseConnector.getConnection();
        boolean errored = false;
        try{
            System.out.println(connection.getClientInfo());
        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);
    }
}
