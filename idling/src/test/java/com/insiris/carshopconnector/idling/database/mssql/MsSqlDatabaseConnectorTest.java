package com.insiris.carshopconnector.idling.database.mssql;


import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.idling.database.DatabaseConnector;
import com.insiris.carshopconnector.idling.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.idling.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.idling.shared.DbHelperMssql;
import com.insiris.carshopconnector.idling.shared.DbSchema;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

import static org.junit.Assert.assertFalse;

public class MsSqlDatabaseConnectorTest {
    private static Injector injector = Guice.createInjector(new ConfigurationPropertiesModule(), new CarshopConnectorDependenciesModule());
    private static DbHelperMssql dbHelperMssql = injector.getInstance(DbHelperMssql.class);
    private static final String dbName = injector.getInstance(Key.get(String.class, Names.named("db_name_birst")));


    @BeforeClass
    public static void setup(){
        dbHelperMssql.createDatabaseAndTables(dbName, new ArrayList<String>(){{
            add(DbSchema.mssqlVehicles(dbName));
            add(DbSchema.mssqlEvents(dbName));
            add(DbSchema.mssqlPeople(dbName));
            add(DbSchema.mssqljobs(dbName));
            add(DbSchema.mssqlEventsIndices);}});
    }

    @AfterClass
    public static void tearDown(){
        dbHelperMssql.dropDatabase(dbName);
    }

    @Test
    public void getConnectionToTestDb(){
        DatabaseConnector databaseConnector = injector.getInstance(MsSqlDatabaseConnector.class);

        Connection connection = databaseConnector.getConnection();
        boolean errored = false;
        try{
            System.out.println(connection.getSchema());
            System.out.println(connection.getClientInfo());
        } catch (SQLException e) {
            errored = true;
        }
        assertFalse(errored);
    }
}
