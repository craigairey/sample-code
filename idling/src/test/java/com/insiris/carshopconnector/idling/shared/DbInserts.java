package com.insiris.carshopconnector.idling.shared;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

public class DbInserts {
    private static         String now = Converters.currentTimestampUTC(); ;

    public static List<String> kassets(String dbName, ZonedDateTime now){
        return new ArrayList<String>(){{
            add("INSERT INTO "+dbName+".kassets (id, name, address, description, postcode_zip, latitude, `decimal`, longitude, kasset_type, is_stock, stock_count, kasset_template_id, kasset_group_id, customer_id, nfc_tag_identifier, identifier, show_on_mobile, show_on_mobile_updated, deleted_at, guid, created_at, updated_at, rate_plan_id, kasset_status_id, send_job_history_kasset_count, dormant, locked_by_user_id, locked_until) VALUES (1, 'car1', 'add1', 'desc1', 'post', 1.01, 1.02, 1.03, 'VEHICLE', 1, 10, 5, 89, null, 'id1', 'id2', 0, 1, null, '3b331eed3a4a2d9288ef415a7f5be1a7af4d43c2', '"+Converters.timestampFormatter().format(now)+"', '2016-07-18 15:19:49', null, 31, 0, 0, null, null);");
            add("INSERT INTO "+dbName+".kassets (id, name, address, description, postcode_zip, latitude, `decimal`, longitude, kasset_type, is_stock, stock_count, kasset_template_id, kasset_group_id, customer_id, nfc_tag_identifier, identifier, show_on_mobile, show_on_mobile_updated, deleted_at, guid, created_at, updated_at, rate_plan_id, kasset_status_id, send_job_history_kasset_count, dormant, locked_by_user_id, locked_until) VALUES (2, 'car1', 'add1', 'desc1', 'post', 1.01, 1.02, 1.03, 'VEHICLE', 1, 10, 5, 89, null, 'id1', 'id2', 0, 1, null, '3b331eed3a4a2d9288ef415a7f5be1a7af4d43xx', '"+Converters.timestampFormatter().format(now)+"', '2016-07-18 15:19:49', null, 31, 0, 0, null, null);");
            add("INSERT INTO "+dbName+".kassets (id, name, address, description, postcode_zip, latitude, `decimal`, longitude, kasset_type, is_stock, stock_count, kasset_template_id, kasset_group_id, customer_id, nfc_tag_identifier, identifier, show_on_mobile, show_on_mobile_updated, deleted_at, guid, created_at, updated_at, rate_plan_id, kasset_status_id, send_job_history_kasset_count, dormant, locked_by_user_id, locked_until) VALUES (3, 'car1', 'add1', 'desc1', 'post', 1.01, 1.02, 1.03, 'VEHICLE', 1, 10, 5, 89, null, 'id1', 'id2', 0, 1, null, '3b331eed3a4a2d9288ef415a7f5be1a7af4d43c3', '"+ Converters.timestampFormatter().format(DateUtil.getYesterday(now).toInstant())+"', '2016-07-18 15:19:49', null, 31, 0, 0, null, null);");
        }};
    }

    public static List<String> mutations(String dbName){
        return new ArrayList<String>(){{
            add("INSERT INTO "+dbName+".mutations (id, statement, type, completed, connector_uuid, uuid) VALUES(NULL, 'some sql', 'create', 0, 1, 'uuid1')");
            add("INSERT INTO "+dbName+".mutations (id, statement, type, completed, connector_uuid, uuid) VALUES(NULL, 'some sql2', 'create', 0, 1, 'uuid2')");
            add("INSERT INTO "+dbName+".mutations (id, statement, type, completed, connector_uuid, uuid) VALUES(NULL, 'some sql3', 'update', 0, 1, 'uuid3')");
        }};
    }

    public static List<String> connectors(String dbName){
        String now = Converters.currentTimestampUTC();
        return new ArrayList<String>(){{
            add("INSERT INTO "+dbName+".connectors (id, start, end, last_kasset_timestamp, last_kasset_field_timestamp, uuid) VALUES(NULL, '"+ now + "', '"+ now + "', '"+ now + "', '"+ now + "', 'uuid1')");
            add("INSERT INTO "+dbName+".connectors (id, start, end, last_kasset_timestamp, last_kasset_field_timestamp, uuid) VALUES(NULL, '"+ now + "', '"+ now + "', '"+ now + "', '"+ now + "', 'uuid2')");
        }};
    }

    public static List<String> kassetFieldTemplates(String dbName){
        return new ArrayList<String>(){{
            add("INSERT INTO "+dbName+".kasset_field_templates (id, name, description, default_value, field_type, options, tag, workflow_value_tag, `group`, kasset_template_id, deleted_at, created_at, updated_at) VALUES (null, 'Stock Number', '', '', 'text', '', 'my_field', '', '', 2, null, '2016-05-10 11:15:06', '2016-05-10 11:15:06')");
            add("INSERT INTO "+dbName+".kasset_field_templates (id, name, description, default_value, field_type, options, tag, workflow_value_tag, `group`, kasset_template_id, deleted_at, created_at, updated_at) VALUES (null, 'Partner Code', '', '', 'text', '', 'my_field', '', '', 2, null, '2016-05-10 11:15:06', '2016-05-10 11:15:06')");
        }};
    }

    public static List<String> kassetFields(String dbName, ZonedDateTime now){
        return new ArrayList<String>(){{
            add("INSERT INTO "+dbName+".kasset_fields (id, value, kasset_field_template_id, kasset_id, guid, kasset_guid, created_at, updated_at, user_id) VALUES (null, 'some value', 1, null, null, '3b331eed3a4a2d9288ef415a7f5be1a7af4d43c2', '"+Converters.timestampFormatter().format(now)+"', '"+Converters.timestampFormatter().format(now)+"', null);");
            add("INSERT INTO "+dbName+".kasset_fields (id, value, kasset_field_template_id, kasset_id, guid, kasset_guid, created_at, updated_at, user_id) VALUES (null, 'some value2', 2, null, null, '3b331eed3a4a2d9288ef415a7f5be1a7af4d43c2', '"+Converters.timestampFormatter().format(now)+"', '"+Converters.timestampFormatter().format(now)+"', null);");
        }};
    }

    public static List<String> jobLogEntries(String dbName, ZonedDateTime now){
        return new ArrayList<String>(){{
            add("INSERT INTO "+dbName+".job_log_entries (id, action, generated_at, notes, latitude, longitude, job_id, created_at, updated_at, wiq_event_guid, sequence_number) VALUES (null, 'in_progress', '"+Converters.timestampFormatter().format(now)+"', 'Job created by operations@insiris.com', null, null, 1, '2016-05-05 16:14:25', '2016-05-05 16:14:25', null, 1);");
            add("INSERT INTO "+dbName+".job_log_entries (id, action, generated_at, notes, latitude, longitude, job_id, created_at, updated_at, wiq_event_guid, sequence_number) VALUES (null, 'completed', '"+Converters.timestampFormatter().format(now)+"', 'Job created by operations@insiris.com', null, null, 1, '2016-05-05 16:14:25', '2016-05-05 16:14:25', null, 1);");
            add("INSERT INTO "+dbName+".job_log_entries (id, action, generated_at, notes, latitude, longitude, job_id, created_at, updated_at, wiq_event_guid, sequence_number) VALUES (null, 'in_progress', '"+Converters.timestampFormatter().format(now)+"', 'Job created by operations@insiris.com', null, null, 2, '2016-05-05 16:14:25', '2016-05-05 16:14:25', null, 1);");
        }};
    }
    public static List<String> jobs(String dbName){
        return new ArrayList<String>(){{
            add("INSERT INTO "+dbName+".jobs (id, customer_name, contact_name, contact_number_1, contact_number_2, email_1, email_2, address, postcode_zip, current_status, notes, latitude, longitude, scheduled_start, scheduled_finish, asset_id, guid, workflow_id, user_id, customer_id, deleted_at, planned_duration_hours, planned_duration_minutes, group_id, created_at, updated_at, account_id, gps_location_id, monetary_value, job_identifier, access_notes, due_by, rate_plan_id, monetary_cost, monetary_value_including_children, monetary_cost_including_children, parent_id, rollup_monetary_from_children, priority, current_sequence_number, send_history_count, prefetch_history_count, due_from, possible_days, time_window_from, time_window_to, pdf_attachment_id, send_job_history_account_count, send_job_history_place_count, generate_lat_long, job_pdf_template_id, sub_workflows_xml, committed_scheduling_plan_job_id, primary_kasset_guid, send_job_history_kasset_count, allocated_by_user_id) VALUES (null, 'afgsfdgs', '', '', '', '', '', '', '', 'dispatched', '', null, null, '2016-05-05 16:14:00', '2016-05-05 17:14:00', null, null, 1, 1, 3, null, 1, 0, null, '2016-05-05 16:14:25', '2016-05-05 16:14:37', null, null, 0.000, '1', '', null, null, 0.000, 0.000, 0.000, null, 1, '', 102, null, null, null, '', '00:00', '23:59', null, 0, 0, 1, null, '', null, '3b331eed3a4a2d9288ef415a7f5be1a7af4d43c2', 0, null)");
            add("INSERT INTO "+dbName+".jobs (id, customer_name, contact_name, contact_number_1, contact_number_2, email_1, email_2, address, postcode_zip, current_status, notes, latitude, longitude, scheduled_start, scheduled_finish, asset_id, guid, workflow_id, user_id, customer_id, deleted_at, planned_duration_hours, planned_duration_minutes, group_id, created_at, updated_at, account_id, gps_location_id, monetary_value, job_identifier, access_notes, due_by, rate_plan_id, monetary_cost, monetary_value_including_children, monetary_cost_including_children, parent_id, rollup_monetary_from_children, priority, current_sequence_number, send_history_count, prefetch_history_count, due_from, possible_days, time_window_from, time_window_to, pdf_attachment_id, send_job_history_account_count, send_job_history_place_count, generate_lat_long, job_pdf_template_id, sub_workflows_xml, committed_scheduling_plan_job_id, primary_kasset_guid, send_job_history_kasset_count, allocated_by_user_id) VALUES (null, 'wipmotstart', '', '', '', '', '', '', '', 'dispatched', '', null, null, '2016-05-05 16:14:00', '2016-05-05 17:14:00', null, null, 2, 1, 3, null, 1, 0, null, '2016-05-05 16:14:25', '2016-05-05 16:14:37', null, null, 0.000, '1', '', null, null, 0.000, 0.000, 0.000, null, 1, '', 102, null, null, null, '', '00:00', '23:59', null, 0, 0, 1, null, '', null, '3b331eed3a4a2d9288ef415a7f5be1a7af4d43xx', 0, null)");
        }};
    }
    public static List<String> workflows(String dbName){
        return new ArrayList<String>(){{
            add("INSERT INTO "+dbName+".workflows (id, name, description, customer_id, deleted_at, created_at, updated_at, internal, publish_to_devices, send_history_count, prefetch_history_count, job_pdf_template_id, last_built_at) VALUES (null, '2. Tunnel Receipt', '', 3, '2016-05-26 12:58:32', '2016-05-05 15:12:18', '2016-05-26 12:58:32', 0, 1, null, null, null, null);");
            add("INSERT INTO "+dbName+".workflows (id, name, description, customer_id, deleted_at, created_at, updated_at, internal, publish_to_devices, send_history_count, prefetch_history_count, job_pdf_template_id, last_built_at) VALUES (null, 'MOT', '', 3, '2016-05-26 12:58:32', '2016-05-05 15:12:18', '2016-05-26 12:58:32', 0, 1, null, null, null, null);");
        }};
    }

    public static List<String> kassetStatues(String dbName, ZonedDateTime now){
        return new ArrayList<String>(){{
           add("INSERT INTO "+dbName+".kasset_kasset_statuses (id, kasset_id, kasset_status_id, created_at, updated_at, user_id) VALUES (1326231, 1, 24, '"+Converters.timestampFormatter().format(now)+"', '"+Converters.timestampFormatter().format(now)+"', 45)") ;
        }};
    }
}
