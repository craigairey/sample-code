package com.insiris.carshopconnector.idling.matcher.model;

import com.insiris.carshopconnector.idling.matcher.enums.EventIdleCategory;

import java.util.HashMap;
import java.util.Map;

public class EventTypes {
    public final static Map<String, IdleType> idleTypeMap = new HashMap<String, IdleType>(){{
        put("TunnelReceiptStart",               new IdleType("TunnelReceiptIdle", EventIdleCategory.Stop));
        put("TunnelReceiptFinish",              new IdleType("TunnelReceiptIdle", EventIdleCategory.Start));
        put("BodyInspectionStart",              new IdleType("BodyInspectionIdle", EventIdleCategory.Stop));
        put("BodyInspectionFinish",             new IdleType("BodyInspectionIdle", EventIdleCategory.Start));
        put("RampInspectionStart",              new IdleType("RampInspectionIdle", EventIdleCategory.Stop));
        put("RampInspectionFinish",             new IdleType("RampInspectionIdle", EventIdleCategory.Start));
        put("RoadTestStart",                    new IdleType("RoadTestIdle", EventIdleCategory.Stop));
        put("RoadTestFinish",                   new IdleType("RoadTestIdle", EventIdleCategory.Start));
        put("FollowupRampInspectionStart",      new IdleType("FollowupRampInspection", EventIdleCategory.Stop));
        put("FollowupRampInspectionFinish",     new IdleType("FollowupRampInspection", EventIdleCategory.Start));
        put("OnHold",                           new IdleType("OnHoldIdle", EventIdleCategory.Start));
        put("InDecision",                       new IdleType("InDecisionIdle", EventIdleCategory.Start));
        put("Rejected",                         new IdleType("RejectedIdle", EventIdleCategory.Start));
        put("AwaitingParts",                    new IdleType("AwaitingPartsIdle", EventIdleCategory.Start));
        put("ReadyForProduction",               new IdleType("ReadyForProductionIdle", EventIdleCategory.Start));
        put("QC",                               new IdleType("QCIdle", EventIdleCategory.Start));
        put("Dispatched",                       new IdleType("DispatchedIdle", EventIdleCategory.Start));
        put("WipMechStart",                      new IdleType("WipMechIdle", EventIdleCategory.Stop));
        put("WipMechPaused",                      new IdleType("WipMechIdle", EventIdleCategory.Start));
        put("WipMechFailed",                       new IdleType("WipMechIdle", EventIdleCategory.Start));
        put("WipMechFinish",                     new IdleType("WipMechIdle", EventIdleCategory.Start));
        put("WipMotStart",                      new IdleType("WipMotIdle", EventIdleCategory.Stop));
        put("WipMotPaused",                      new IdleType("WipMotIdle", EventIdleCategory.Start));
        put("WipMotFailed",                       new IdleType("WipMotIdle", EventIdleCategory.Start));
        put("WipMotFinish",                     new IdleType("WipMotIdle", EventIdleCategory.Start));
        put("WipSmartStart",                    new IdleType("WipSmartIdle", EventIdleCategory.Stop));
        put("WipSmartPaused",                    new IdleType("WipSmartIdle", EventIdleCategory.Start));
        put("WipSmartFailed",                     new IdleType("WipSmartIdle", EventIdleCategory.Start));
        put("WipSmartFinish",                   new IdleType("WipSmartIdle", EventIdleCategory.Start));
        put("WipStripStart",                    new IdleType("WipStripIdle", EventIdleCategory.Stop));
        put("WipStripPaused",                    new IdleType("WipStripIdle", EventIdleCategory.Start));
        put("WipStripFailed",                     new IdleType("WipStripIdle", EventIdleCategory.Start));
        put("WipStripFinish",                   new IdleType("WipStripIdle", EventIdleCategory.Start));
        put("WipPanelStart",                    new IdleType("WipPanelIdle", EventIdleCategory.Stop));
        put("WipPanelPaused",                    new IdleType("WipPanelIdle", EventIdleCategory.Start));
        put("WipPanelFailed",                     new IdleType("WipPanelIdle", EventIdleCategory.Start));
        put("WipPanelFinish",                   new IdleType("WipPanelIdle", EventIdleCategory.Start));
        put("WipPrepStart",                     new IdleType("WipPrepIdle", EventIdleCategory.Stop));
        put("WipPrepPaused",                     new IdleType("WipPrepIdle", EventIdleCategory.Start));
        put("WipPrepFailed",                      new IdleType("WipPrepIdle", EventIdleCategory.Start));
        put("WipPrepFinish",                    new IdleType("WipPrepIdle", EventIdleCategory.Start));
        put("WipBagStart",                      new IdleType("WipBagIdle", EventIdleCategory.Stop));
        put("WipBagPaused",                      new IdleType("WipBagIdle", EventIdleCategory.Start));
        put("WipBagFailed",                       new IdleType("WipBagIdle", EventIdleCategory.Start));
        put("WipBagFinish",                     new IdleType("WipBagIdle", EventIdleCategory.Start));
        put("WipPaintStart",                    new IdleType("WipPaintIdle", EventIdleCategory.Stop));
        put("WipPaintPaused",                    new IdleType("WipPaintIdle", EventIdleCategory.Start));
        put("WipPaintFailed",                     new IdleType("WipPaintIdle", EventIdleCategory.Start));
        put("WipPaintFinish",                   new IdleType("WipPaintIdle", EventIdleCategory.Start));
        put("WipPolishStart",                   new IdleType("WipPolishIdle", EventIdleCategory.Stop));
        put("WipPolishPaused",                   new IdleType("WipPolishIdle", EventIdleCategory.Start));
        put("WipPolishFailed",                    new IdleType("WipPolishIdle", EventIdleCategory.Start));
        put("WipPolishFinish",                  new IdleType("WipPolishIdle", EventIdleCategory.Start));
        put("WipFitStart",                      new IdleType("WipFitIdle", EventIdleCategory.Stop));
        put("WipFitPaused",                      new IdleType("WipFitIdle", EventIdleCategory.Start));
        put("WipFitFailed",                       new IdleType("WipFitIdle", EventIdleCategory.Start));
        put("WipFitFinish",                     new IdleType("WipFitIdle", EventIdleCategory.Start));
        put("CheckIn",                          new IdleType("CheckInIdle", EventIdleCategory.Start));
        put("CheckOut",                         new IdleType("CheckOutIdle", EventIdleCategory.Stop));

    }};
}
