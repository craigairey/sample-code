package com.insiris.carshopconnector.idling.model.idleconnector.storage.mysql;

import com.insiris.carshopconnector.idling.model.idleconnector.IdleConnector;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class IdleConnectorMapper {
    public static List<IdleConnector> mapResults(ResultSet resultSet) throws SQLException {
        List<IdleConnector> idleConnectors = new ArrayList<>();
        while(resultSet.next()) {
            IdleConnector idleConnector = new IdleConnector();

            idleConnector.setId(resultSet.getInt(IdleConnectorSql.instance.getAliasId()));
            idleConnector.setStart(resultSet.getTimestamp(IdleConnectorSql.instance.getAliasStart()));
            idleConnector.setFinish(resultSet.getTimestamp(IdleConnectorSql.instance.getAliasFinish()));
            idleConnectors.add(idleConnector);
        }
        return idleConnectors;
    }
}
