package com.insiris.carshopconnector.idling.model.idleconnector.storage;

import com.insiris.carshopconnector.idling.model.idleconnector.IdleConnector;

public interface IdleConnectorDao {
    IdleConnector get();
    void save(IdleConnector idleConnector);
}
