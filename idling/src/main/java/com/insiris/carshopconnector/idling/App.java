package com.insiris.carshopconnector.idling;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.idling.di.CarshopConnectorDependenciesModule;
import com.insiris.carshopconnector.idling.di.ConfigurationPropertiesModule;
import com.insiris.carshopconnector.idling.service.CarshopIdle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class App {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private static Injector injector;


    public static void main(String[] args) {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
        Injector injector = Guice.createInjector(new CarshopConnectorDependenciesModule(), new ConfigurationPropertiesModule());
        CarshopIdle carshopIdle = new CarshopIdle(injector);
        ScheduledExecutorService service = Executors
                .newSingleThreadScheduledExecutor();
        service.schedule(injector.getInstance(CarshopIdle.class), injector.getInstance(Key.get(Long.class, Names.named("timer_interval"))), TimeUnit.SECONDS);
        service.scheduleAtFixedRate(carshopIdle, 0, injector.getInstance(Key.get(Long.class, Names.named("timer_interval"))), TimeUnit.SECONDS);
    }
}
