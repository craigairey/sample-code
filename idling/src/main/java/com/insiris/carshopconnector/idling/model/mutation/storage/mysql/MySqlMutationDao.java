package com.insiris.carshopconnector.idling.model.mutation.storage.mysql;


import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.idling.database.DatabaseConnector;
import com.insiris.carshopconnector.idling.model.mutation.Mutation;
import com.insiris.carshopconnector.idling.model.mutation.storage.MutationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.StringJoiner;

public class MySqlMutationDao implements MutationDao {
    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlMutationDao(@Named("mysqlConMut") DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }


    @Override
    public void saveAll(List<Mutation> mutations) {
        Statement statement = null;
        try{
            statement = connection.createStatement();
            for(Mutation mutation: mutations) {

                String sql = String.format(MutationSql.instance.getSaveAll(),
                        mutation.getStatement(),
                        mutation.getType(),
                        mutation.isCompleted() ? 1 : 0,
                        mutation.isIdleFlag() ? 1 : 0,
                        mutation.getConnectorUuid(),
                        mutation.getUuid()
                );

                logger.info("inserting mutation {}", sql);

                statement.addBatch(sql);
            }
            if (mutations != null && mutations.size() > 0)
                statement.executeBatch();

        } catch (Exception e) {
            logger.error("inserting mutation has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(Exception se2){}

        }
    }

    @Override
    public void markCompleted(List<Mutation> mutations) {
        StringJoiner joiner = new StringJoiner(",");
        for(Mutation mutation: mutations){
            joiner.add(String.valueOf(mutation.getId()));
        }
        logger.info("marking complete for {}", joiner.toString());
        Statement statement = null;
        try{
            statement = connection.createStatement();
            statement.executeUpdate(String.format(MutationSql.instance.getMarkCompleted(), joiner.add(joiner.toString())));
        } catch (SQLException e) {
            logger.error("marking complete for {} has thrown: {}", joiner.toString(), new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
    }

    @Override
    public void markFailed(List<Mutation> mutations) {
        StringJoiner joiner = new StringJoiner(",");
        for(Mutation mutation: mutations){
            joiner.add(String.valueOf(mutation.getId()));
        }
        logger.info("marking complete for {}", joiner.toString());
        Statement statement = null;
        try{
            statement = connection.createStatement();
            statement.executeUpdate(String.format(MutationSql.instance.getMarkFailed(), joiner.add(joiner.toString())));
        } catch (SQLException e) {
            logger.error("marking failed for {} has thrown: {}", joiner.toString(), new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
    }

    @Override
    public List<Mutation> getAllMutations() {
        logger.info("getting all mutations");
        Statement statement = null;
        List<Mutation> results = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(MutationSql.instance.getGetAllCreateMutations());
            results = MutationMapper.mapResults(resultSet);
            resultSet.close();
        } catch (SQLException e) {
            logger.error("getting all create mutations has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }

}
