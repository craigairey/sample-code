package com.insiris.carshopconnector.idling.model.mutation;

import java.util.ArrayList;
import java.util.List;

public class Mutations {
    private List<Mutation>completed;
    private List<Mutation>failed;

    public Mutations() {
        this.completed = new ArrayList<>();
        this.failed = new ArrayList<>();
    }

    public List<Mutation> getCompleted() {
        return completed;
    }

    public void setCompleted(List<Mutation> completed) {
        this.completed = completed;
    }

    public List<Mutation> getFailed() {
        return failed;
    }

    public void setFailed(List<Mutation> failed) {
        this.failed = failed;
    }

    public void addFailed(Mutation mutation){
        this.failed.add(mutation);
    }

    public void addCompleted(Mutation mutation){
        this.completed.add(mutation);
    }
}
