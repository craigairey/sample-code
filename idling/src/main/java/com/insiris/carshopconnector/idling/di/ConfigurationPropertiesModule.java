package com.insiris.carshopconnector.idling.di;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ConfigurationPropertiesModule extends AbstractModule{
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    protected void configure() {
        Properties defaults = new Properties();
        try {
            Properties props = new Properties(defaults);
            props.load(new FileInputStream("config.properties"));
            Names.bindProperties(binder(), props);
        } catch (IOException e) {
            logger.error("getting config has thrown: {}", new Object[] {e.getMessage(), e});
        }
    }
}
