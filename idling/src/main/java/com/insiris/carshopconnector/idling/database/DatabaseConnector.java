package com.insiris.carshopconnector.idling.database;

import java.sql.Connection;

public interface DatabaseConnector {
    Connection getConnection();
}
