package com.insiris.carshopconnector.idling.handler;

import com.insiris.carshopconnector.idling.matcher.Matcher;
import com.insiris.carshopconnector.idling.model.idleconnector.IdleConnector;
import com.insiris.carshopconnector.idling.model.mutation.Mutation;
import com.insiris.carshopconnector.idling.model.mutation.storage.MutationDao;

import java.util.ArrayList;
import java.util.List;

public class IdleHandler {
    private List<Matcher> matchers;
    private MutationDao mutationDao;


    public IdleHandler(List<Matcher> matchers, MutationDao mutationDao) {
        this.matchers = matchers;
        this.mutationDao = mutationDao;
    }

    protected List<Matcher> getMatchers() {
        if (this.matchers == null) {
            this.matchers = new ArrayList<>();
        }
        return this.matchers;
    }
    protected void setMatchers(List<Matcher> matchers) {
        this.matchers = matchers;
    }
    public MutationDao getMutationDao() {
        return this.mutationDao;
    }
    private void setMutationDao(MutationDao mutationDao) {
        this.mutationDao = mutationDao;
    }
    public boolean match(List toProcess, IdleConnector connector){
        List<Mutation> mutations = new ArrayList<>();

        for(Matcher matcher : this.getMatchers()){
            for(int i = 0; i < toProcess.size(); i++){
                Mutation mutation = matcher.match(toProcess.get(i), i > 0 ? toProcess.get(i - 1) : null, connector);
                if (mutation != null)
                    mutations.add(mutation);
            }
        }
        getMutationDao().saveAll(mutations);
        return false;
    };
}
