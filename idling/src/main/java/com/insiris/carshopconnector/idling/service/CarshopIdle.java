package com.insiris.carshopconnector.idling.service;


import com.google.inject.Injector;
import com.google.inject.Key;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.idling.handler.IdleHandler;
import com.insiris.carshopconnector.idling.matcher.Matcher;
import com.insiris.carshopconnector.idling.matcher.PersonIdleMatcher;
import com.insiris.carshopconnector.idling.matcher.VehicleIdleMatcher;
import com.insiris.carshopconnector.idling.model.event.Event;
import com.insiris.carshopconnector.idling.model.event.storage.EventDao;
import com.insiris.carshopconnector.idling.model.idleconnector.IdleConnector;
import com.insiris.carshopconnector.idling.model.mutation.Mutation;
import com.insiris.carshopconnector.idling.model.mutation.Mutations;
import com.insiris.carshopconnector.idling.model.mutation.storage.MutationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CarshopIdle implements Runnable {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private EventDao eventDao;
    private MutationDao mutationDao;
    private IdleHandler handler;
    private static String birstName;

    public CarshopIdle(Injector injector){
        eventDao = injector.getInstance(EventDao.class);
        mutationDao = injector.getInstance(Key.get(MutationDao.class, Names.named("mysql")));
        handler = new IdleHandler(new ArrayList<Matcher>(){{
            add(new VehicleIdleMatcher());
            add(new PersonIdleMatcher());
        }}, mutationDao);
        birstName = injector.getInstance(Key.get(String.class, Names.named("db_name_birst")));
    }

    @Override
    public void run() {
        Timestamp start = Timestamp.valueOf("2018-07-11 00:00:00.00");
        Timestamp finish = Timestamp.valueOf("2018-07-11 23:59:59.00");
        IdleConnector connector = new IdleConnector();
        connector.setStart(start);
        connector.setFinish(finish);

        connector.setDbName(birstName);
        List<Event> newEvents = eventDao.findNewEvents(connector.getStart(), connector.getFinish());
        List<Integer> vehicleIds = newEvents.stream()
                .map(Event::getVehicleId)
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());
        List<Integer> peopleIds = newEvents.stream()
                .map(Event::getPersonId)
                .filter(Objects::nonNull)
                .distinct()
                .collect(Collectors.toList());

        eventDao.removeAllIdleEventsForVehicles(vehicleIds);
        for (Integer id : vehicleIds) {
            logger.error("before v - {}", id);
            handler.match(eventDao.getAllForVehicle(id), connector);
            logger.error("after v - {}", id);

        }

        handler = new IdleHandler(new ArrayList<Matcher>(){{
            add(new VehicleIdleMatcher());
            add(new PersonIdleMatcher());
        }}, mutationDao);

        eventDao.removeAllIdleEventsForPeople(peopleIds);
        for (Integer id : peopleIds) {
            logger.error("before p - {}", id);
            handler.match(eventDao.getAllForPerson(id), connector);
            logger.error("after p - {}", id);
        }

        List<Mutation> mutations = mutationDao.getAllMutations();
        while (mutations != null && mutations.size() > 0 ) {
            Mutations response = eventDao.addAllMutations(mutations);
            if (response.getCompleted().size() > 0)
                mutationDao.markCompleted(response.getCompleted());
            if(response.getFailed().size() >0 )
                mutationDao.markFailed(response.getFailed());
            mutations = mutationDao.getAllMutations();
        }
        Timestamp newFinish = new Timestamp(connector.getFinish().toInstant().toEpochMilli() + 30000L);
        connector.setStart(connector.getFinish());
        connector.setFinish(newFinish);
    }
}
