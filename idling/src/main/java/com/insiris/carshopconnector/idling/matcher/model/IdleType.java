package com.insiris.carshopconnector.idling.matcher.model;

import com.insiris.carshopconnector.idling.matcher.enums.EventIdleCategory;

public class IdleType {
    private String type;
    private EventIdleCategory startOrFinished;

    public IdleType() {
    }

    public IdleType(String type, EventIdleCategory startOrFinished) {
        this.type = type;
        this.startOrFinished = startOrFinished;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EventIdleCategory getStartOrFinished() {
        return startOrFinished;
    }

    public void setStartOrFinished(EventIdleCategory startOrFinished) {
        this.startOrFinished = startOrFinished;
    }
}
