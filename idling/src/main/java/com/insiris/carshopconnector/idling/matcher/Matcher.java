package com.insiris.carshopconnector.idling.matcher;


import com.insiris.carshopconnector.idling.matcher.enums.EventIdleCategory;
import com.insiris.carshopconnector.idling.matcher.model.IdleType;
import com.insiris.carshopconnector.idling.model.event.Event;
import com.insiris.carshopconnector.idling.model.idleconnector.IdleConnector;
import com.insiris.carshopconnector.idling.model.mutation.Mutation;
import com.insiris.carshopconnector.idling.shared.Converters;

import java.sql.Timestamp;


public class Matcher {

    private         String now = Converters.currentTimestampUTC();

    public Mutation match(Object toMatch, Object previous, IdleConnector connector) {
        return null;
    }

    protected Mutation mutation(Event event, IdleConnector connector, EventIdleCategory eventIdleCategory, IdleType idleType, String eventType){
        return new Mutation(
                generateSql(
                        connector.getDbName(),
                        eventIdleCategory,
                        idleType.getType(),
                        eventType,
                        event.getEventTimestamp(),
                        event.getVehicleId(),
                        event.getPersonId()
                ),
                connector.getUuid());
    }

    private String generateSql(
            String dbName,
            EventIdleCategory eventIdleCategory,
            String eventType,
            String eventDescription,
            Timestamp timestamp,
            Integer vehicleId,
            Integer personId){

        String now =  Converters.currentTimestampUTC();
        return String.format(
                "INSERT INTO %1$s.dbo.events(event_category, event_type, event_description, timestamp, duration, vehicle_id, person_id, created_at, updated_at, connector_log_run_uuid, mutation_uuid) VALUES " +
                                            "(%2$s, %3$s, %4$s,  %5$s, null, %6$s, %7$s, %8$s, %9$s, %10$s, %11$s)",
                dbName,
                eventIdleCategory == EventIdleCategory.VEHICLE ? escapeString("vehicle_idle") : escapeString("person_idle"),
                nullOrValue(eventDescription),
                nullOrValue(eventType),
                nullOrTimestamp(timestamp),
                eventIdleCategory == EventIdleCategory.PERSON ? "null" : nullOrInteger(vehicleId),
                eventIdleCategory == EventIdleCategory.VEHICLE ? "null" : nullOrInteger(personId),
                escapeString(now),
                escapeString(now),
                "\\'%1$s\\'",
                "\\'%2$s\\'"
        );
    }

    private String escapeString(String value){
        StringBuilder builder = new StringBuilder();
        builder.append("\\'");
        builder.append(value);
        builder.append("\\'");
        return builder.toString();
    }

    private String nullOrValue(String value){
        return value == null ? "null" : escapeString(value);
    }

    private String nullOrTimestamp(Timestamp value){
        return value == null ? "null" : escapeString(Converters.timestampFormatter().format(value));
    }

    private String nullOrInteger(Integer value){
        return value == null ? "null" : value.toString();
    }

}
