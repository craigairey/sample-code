package com.insiris.carshopconnector.idling.model.idleconnector.storage.mysql;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.idling.database.DatabaseConnector;
import com.insiris.carshopconnector.idling.model.idleconnector.IdleConnector;
import com.insiris.carshopconnector.idling.model.idleconnector.storage.IdleConnectorDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class MySqlIdleConnectorDao implements IdleConnectorDao {
    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MySqlIdleConnectorDao(@Named("mysqlConMut") DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }


    @Override
    public IdleConnector get() {
        Statement statement = null;
        IdleConnector toReturn = null;
        try{
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(IdleConnectorSql.instance.getPrevious());
            List<IdleConnector> results = IdleConnectorMapper.mapResults(resultSet);
            if(results.size() == 1)
                toReturn = results.get(0);
        } catch (SQLException e){
            logger.error("getting latest idleconnector has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return toReturn;
    }

    @Override
    public void save(IdleConnector connector) {
        Statement statement = null;
        try{
            statement = connection.createStatement();
            String sql = String.format(
                    IdleConnectorSql.instance.getSave(),
                    connector.getStart(),
                    connector.getFinish(),
                    connector.getUuid()
            );
            statement.execute(sql);
        } catch (SQLException e) {
            logger.error("creating idleconnector has thrown: {}", new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
    }
}
