package com.insiris.carshopconnector.idling.di;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;
import com.insiris.carshopconnector.idling.database.DatabaseConnector;
import com.insiris.carshopconnector.idling.database.mssql.MsSqlDatabaseConnector;
import com.insiris.carshopconnector.idling.database.mysql.MySqlConMutDatabaseConnector;
import com.insiris.carshopconnector.idling.database.mysql.MySqlDatabaseConnector;
import com.insiris.carshopconnector.idling.model.event.storage.EventDao;
import com.insiris.carshopconnector.idling.model.event.storage.mssql.MsSqlEventDao;
import com.insiris.carshopconnector.idling.model.mutation.storage.MutationDao;
import com.insiris.carshopconnector.idling.model.mutation.storage.mysql.MySqlMutationDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CarshopConnectorDependenciesModule extends AbstractModule {
    private Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    protected void configure() {

        bind(DatabaseConnector.class).annotatedWith(
                Names.named("mysql")).to(MySqlDatabaseConnector.class);
        bind(DatabaseConnector.class).annotatedWith(
                Names.named("mysqlConMut")).to(MySqlConMutDatabaseConnector.class);
        bind(DatabaseConnector.class).annotatedWith(
                Names.named("mssql")).to(MsSqlDatabaseConnector.class);
        bind(EventDao.class).to(MsSqlEventDao.class);
        bind(MutationDao.class).annotatedWith(
                Names.named("mysql")).to(MySqlMutationDao.class);
    }
}