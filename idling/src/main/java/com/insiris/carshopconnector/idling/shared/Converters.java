package com.insiris.carshopconnector.idling.shared;

import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class Converters {
    public static SimpleDateFormat timestampFormatter(){
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    }
    public static String currentTimestampUTC(){
        return DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(ZonedDateTime.now(ZoneOffset.UTC));
    }
}
