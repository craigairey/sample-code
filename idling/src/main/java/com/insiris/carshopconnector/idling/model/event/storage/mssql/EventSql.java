package com.insiris.carshopconnector.idling.model.event.storage.mssql;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;

public class EventSql {

    public static EventSql instance;
    private static Logger logger = LoggerFactory.getLogger(EventSql.class);


    static {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        try {
            InputStream stream = EventSql.class.getClassLoader().getResourceAsStream("event_sql.yml");
            instance = mapper.readValue(stream, EventSql.class);
        } catch (Exception e) {
            logger.error("loading EventSql has thrown {}", e);
        }
    }

    private String allForVehicle;
    private String allForPerson;
    private String removeAllVehiclesIdle;
    private String removeAllPeopleIdle;
    private String findNewEvents;
    private String addAllMutations;
    private String aliasId;
    private String aliasEventCatagory;
    private String aliasEventType;
    private String aliasEventDescription;
    private String aliasTimestamp;
    private String aliasDurationSeconds;
    private String aliasVehicleId;
    private String aliasPersonId;

    public String getAllForVehicle() {
        return allForVehicle;
    }

    public void setAllForVehicle(String allForVehicle) {
        this.allForVehicle = allForVehicle;
    }

    public String getAllForPerson() {
        return allForPerson;
    }

    public String getRemoveAllVehiclesIdle() {
        return removeAllVehiclesIdle;
    }

    public void setRemoveAllVehiclesIdle(String removeAllVehiclesIdle) {
        this.removeAllVehiclesIdle = removeAllVehiclesIdle;
    }

    public String getRemoveAllPeopleIdle() {
        return removeAllPeopleIdle;
    }

    public void setRemoveAllPeopleIdle(String removeAllPeopleIdle) {
        this.removeAllPeopleIdle = removeAllPeopleIdle;
    }

    public void setAllForPerson(String allForPerson) {
        this.allForPerson = allForPerson;
    }

    public String getFindNewEvents() {
        return findNewEvents;
    }

    public void setFindNewEvents(String findNewEvents) {
        this.findNewEvents = findNewEvents;
    }

    public String getAddAllMutations() {
        return addAllMutations;
    }

    public void setAddAllMutations(String addAllMutations) {
        this.addAllMutations = addAllMutations;
    }

    public String getAliasId() {
        return aliasId;
    }

    public void setAliasId(String aliasId) {
        this.aliasId = aliasId;
    }

    public String getAliasEventCatagory() {
        return aliasEventCatagory;
    }

    public void setAliasEventCatagory(String aliasEventCatagory) {
        this.aliasEventCatagory = aliasEventCatagory;
    }

    public String getAliasEventType() {
        return aliasEventType;
    }

    public void setAliasEventType(String aliasEventType) {
        this.aliasEventType = aliasEventType;
    }

    public String getAliasEventDescription() {
        return aliasEventDescription;
    }

    public void setAliasEventDescription(String aliasEventDescription) {
        this.aliasEventDescription = aliasEventDescription;
    }

    public String getAliasTimestamp() {
        return aliasTimestamp;
    }

    public void setAliasTimestamp(String aliasTimestamp) {
        this.aliasTimestamp = aliasTimestamp;
    }


    public String getAliasDurationSeconds() {
        return aliasDurationSeconds;
    }

    public void setAliasDurationSeconds(String aliasDurationSeconds) {
        this.aliasDurationSeconds = aliasDurationSeconds;
    }

    public String getAliasVehicleId() {
        return aliasVehicleId;
    }

    public void setAliasVehicleId(String aliasVehicleId) {
        this.aliasVehicleId = aliasVehicleId;
    }

    public String getAliasPersonId() {
        return aliasPersonId;
    }

    public void setAliasPersonId(String aliasPersonId) {
        this.aliasPersonId = aliasPersonId;
    }
}
