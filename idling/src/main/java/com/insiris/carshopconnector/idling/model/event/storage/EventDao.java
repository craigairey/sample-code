package com.insiris.carshopconnector.idling.model.event.storage;

import com.insiris.carshopconnector.idling.model.event.Event;
import com.insiris.carshopconnector.idling.model.mutation.Mutation;
import com.insiris.carshopconnector.idling.model.mutation.Mutations;

import java.sql.Timestamp;
import java.util.List;

public interface EventDao {
    List<Event> getAllForVehicle(int vehicleId);
    List<Event> getAllForPerson(int personId);
    List<Event> findNewEvents(Timestamp start, Timestamp end);
    Mutations addAllMutations(List<Mutation> mutations);

    void removeAllIdleEventsForVehicles(List<Integer> ids);
    void removeAllIdleEventsForPeople(List<Integer> ids);
}
