package com.insiris.carshopconnector.idling.database.mssql;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.idling.database.DatabaseConnector;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class MsSqlDatabaseConnector implements DatabaseConnector {
    private final String jdbcDriver;
    private final String dbUrl;
    private final String dbUser;
    private final String dbPass;
    private final String dbName;
    private final DataSource dataSource;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Inject
    public MsSqlDatabaseConnector(
               @Named("jdbc_mssql_driver") String jdbcDriver,
               @Named("db_url_birst") String dbUrl,
               @Named("db_user_birst") String dbUser,
               @Named("db_password_birst") String dbPassword,
               @Named("db_name_birst") String dbName){
        this.jdbcDriver = jdbcDriver;
        this.dbUrl = dbUrl;
        this.dbUser = dbUser;
        this.dbPass = dbPassword;
        this.dbName = dbName;
        this.dataSource = getDataSource();
    }

    public DataSource getDataSource(){
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(jdbcDriver);
        ds.setUrl(dbUrl);
        ds.setUsername(dbUser);
        ds.setPassword(dbPass);
        ds.setInitialSize(16);
        return ds;
    }

    public Connection getConnection(){
        Connection connection = null;
        try {
            connection = this.dataSource.getConnection();
        } catch (SQLException e) {
           logger.error(e.getLocalizedMessage());
        }
        return connection;
    }
}
