package com.insiris.carshopconnector.idling.matcher;


import com.insiris.carshopconnector.idling.matcher.enums.EventIdleCategory;
import com.insiris.carshopconnector.idling.model.event.Event;
import com.insiris.carshopconnector.idling.model.idleconnector.IdleConnector;
import com.insiris.carshopconnector.idling.model.mutation.Mutation;
import com.insiris.carshopconnector.idling.matcher.model.IdleType;
import com.insiris.carshopconnector.idling.matcher.model.EventTypes;

public class VehicleIdleMatcher extends Matcher {
    @Override
    public Mutation match(Object toMatch, Object previous, IdleConnector connector) {

        if(EventTypes.idleTypeMap.containsKey(((Event)toMatch).getEventType())){
            IdleType idleType = EventTypes.idleTypeMap.get(((Event)toMatch).getEventType());

            if(EventIdleCategory.Stop.equals(idleType.getStartOrFinished()) && previous != null){
                return mutation(((Event)toMatch), connector, EventIdleCategory.VEHICLE, EventTypes.idleTypeMap.get(((Event)previous).getEventType()),  EventTypes.idleTypeMap.get(((Event)previous).getEventType()).getType() + EventIdleCategory.Stop);
            } else if (EventIdleCategory.Start.equals(idleType.getStartOrFinished())){
                return mutation(((Event)toMatch), connector, EventIdleCategory.VEHICLE, idleType,  idleType.getType() + EventIdleCategory.Start);
            }
        }
        return null;
    }
}
