package com.insiris.carshopconnector.idling.matcher.enums;

public enum EventIdleCategory {
    VEHICLE, PERSON, Start, Stop;
}
