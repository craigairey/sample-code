package com.insiris.carshopconnector.idling.model.event.storage.mssql;

import com.insiris.carshopconnector.idling.model.event.Event;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class EventMapper {

    public static List<Event> mapResults(ResultSet resultSet) throws SQLException {
        List<Event> events = new ArrayList<>();

        while(resultSet.next()) {
            Event event = new Event();

            event.setId(resultSet.getInt(EventSql.instance.getAliasId()));
            event.setEventCatagory(resultSet.getString(EventSql.instance.getAliasEventCatagory()));
            event.setEventType(resultSet.getString(EventSql.instance.getAliasEventType()));
            event.setEventDescription(resultSet.getString(EventSql.instance.getAliasEventDescription()));
            event.setEventTimestamp(resultSet.getTimestamp(EventSql.instance.getAliasTimestamp()));
            event.setVehicleId(resultSet.getInt(EventSql.instance.getAliasVehicleId()));
            event.setPersonId(resultSet.getInt(EventSql.instance.getAliasPersonId()));
            events.add(event);
        }
        return events;
    }
}
