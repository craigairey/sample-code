package com.insiris.carshopconnector.idling.model.event;

import java.sql.Timestamp;

public class Event {
    private int id;
    private String eventCatagory;
    private String eventType;
    private String eventDescription;
    private Timestamp eventTimestamp;
    private int vehicleId;
    private int personId;

    public Event() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEventCatagory() {
        return eventCatagory;
    }

    public void setEventCatagory(String eventCatagory) {
        this.eventCatagory = eventCatagory;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public Timestamp getEventTimestamp() {
        return eventTimestamp;
    }

    public void setEventTimestamp(Timestamp eventTimestamp) {
        this.eventTimestamp = eventTimestamp;
    }

    public int getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(int vehicleId) {
        this.vehicleId = vehicleId;
    }

    public int getPersonId() {
        return personId;
    }

    public void setPersonId(int personId) {
        this.personId = personId;
    }
}
