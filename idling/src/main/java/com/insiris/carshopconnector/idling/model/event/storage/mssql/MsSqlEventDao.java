package com.insiris.carshopconnector.idling.model.event.storage.mssql;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.insiris.carshopconnector.idling.database.DatabaseConnector;
import com.insiris.carshopconnector.idling.model.event.Event;
import com.insiris.carshopconnector.idling.model.event.storage.EventDao;
import com.insiris.carshopconnector.idling.model.mutation.Mutation;
import com.insiris.carshopconnector.idling.model.mutation.Mutations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MsSqlEventDao implements EventDao {

    private Connection connection;
    Logger logger = LoggerFactory.getLogger(this.getClass());


    @Inject
    public MsSqlEventDao(@Named("mssql")DatabaseConnector databaseConnector) {
        this.connection = databaseConnector.getConnection();
    }

    @Override
    public List<Event> getAllForVehicle(int vehicleId) {
        logger.info("getting all events for vehicle {}", vehicleId);
        PreparedStatement statement = null;
        List<Event> results = new ArrayList<>();
        try {
            connection.setCatalog("UnityBI");
            statement = connection.prepareStatement(EventSql.instance.getAllForVehicle());
            statement.setInt(1, vehicleId);
            logger.info("Events get all vehicles {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = EventMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all Events for vehicleId {} has thrown: {}", vehicleId, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }

    @Override
    public List<Event> getAllForPerson(int personId) {
        logger.info("getting all events for person {}", personId);
        PreparedStatement statement = null;
        List<Event> results = new ArrayList<>();
        try {
            connection.setCatalog("UnityBI");
            statement = connection.prepareStatement(EventSql.instance.getAllForPerson());
            statement.setInt(1, personId);
            logger.info("Events get all person {}", statement.toString());
            ResultSet resultSet = statement.executeQuery();
            results = EventMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all Events for personId {} has thrown: {}", personId, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }

    @Override
    public List<Event> findNewEvents(Timestamp start, Timestamp end){
        logger.info("getting all new events start {} finish {}", start, end);
        PreparedStatement statement = null;
        List<Event> results = new ArrayList<>();
        try {
            connection.setCatalog("UnityBI");
            statement = connection.prepareStatement(EventSql.instance.getFindNewEvents());
            statement.setTimestamp(1, start);
            statement.setTimestamp(2, end);
            ResultSet resultSet = statement.executeQuery();
            results = EventMapper.mapResults(resultSet);
            resultSet.close();
        } catch (Exception e) {
            logger.error("getting all Event start {} finish {} has thrown: {}", start, end, new Object[] {e.getMessage(), e});
        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }
        return results;
    }

    @Override
    public Mutations addAllMutations(List<Mutation> mutations) {
        String sql ="";
        Statement statement = null;
        Mutations toReturn = new Mutations();
        try {
            statement = connection.createStatement();
            for(Mutation mutation: mutations) {
                statement.addBatch(mutation.getStatement());
            }
            int[] responses = statement.executeBatch();
            for(int i = 0; i < responses.length; i++){
                if(responses[i] == 1){
                    mutations.get(i).setCompleted(true);
                    toReturn.addCompleted(mutations.get(i));
                }else{
                    toReturn.addFailed(mutations.get(i));
                }

            }
        } catch (SQLException e) {
            logger.error(sql);
            logger.error("inserting into db has thrown: {}",  new Object[] {e.getMessage(), e});

        } finally {
            try{
                if(statement!=null)
                    statement.close();
            }catch(SQLException se2){}
        }

        return toReturn;
    }

    @Override
    public void removeAllIdleEventsForVehicles(List<Integer> ids) {
        String vehicleIds = convertListOfIntsToCommaSeparatedString(ids);
        logger.info(" removing all vehicle {}", vehicleIds);
        PreparedStatement statement = null;
        try {
            connection.setCatalog("UnityBI");
            String sql =String.format(EventSql.instance.getRemoveAllVehiclesIdle(), vehicleIds);
            statement = connection.prepareStatement(sql);
            logger.info("Events get all person {}", statement.toString());
            Integer result = statement.executeUpdate();
            logger.info("{} deleted for: {}", result, vehicleIds);
        } catch (Exception e) {
            logger.error("removing all Vehicle Idle Events for ids {} has thrown: {}", vehicleIds, new Object[]{e.getMessage(), e});
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException se2) {
            }
        }
    }

    @Override
    public void removeAllIdleEventsForPeople(List<Integer> ids) {

        String peopleIds = convertListOfIntsToCommaSeparatedString(ids);
        logger.info(" removing all people {}", peopleIds);
        PreparedStatement statement = null;
        try {
            connection.setCatalog("UnityBI");

            String sql =String.format(EventSql.instance.getRemoveAllPeopleIdle(), peopleIds);
            statement = connection.prepareStatement(sql);
            logger.info("Events get all person {}", statement.toString());
            Integer result = statement.executeUpdate();
            logger.info("{} deleted for: {}", result, peopleIds);
        } catch (Exception e) {
            logger.error("removing all person Idle Events for id {} has thrown: {}", peopleIds, new Object[]{e.getMessage(), e});
        } finally {
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException se2) {
            }
        }
    }

    private String convertListOfIntsToCommaSeparatedString(List<Integer> ints){
        return ints.stream()
                .map(i -> i.toString())
                .collect(Collectors.joining(", "));
    }
}
