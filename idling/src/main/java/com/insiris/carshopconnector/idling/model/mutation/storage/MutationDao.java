package com.insiris.carshopconnector.idling.model.mutation.storage;

import com.insiris.carshopconnector.idling.model.mutation.Mutation;

import java.util.List;

public interface MutationDao {
    void saveAll(List<Mutation> mutations);
    void markCompleted(List<Mutation> mutations);
    void markFailed(List<Mutation> mutations);
    List<Mutation> getAllMutations();
}
